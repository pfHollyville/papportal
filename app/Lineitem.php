<?php
/**
 * Line item class file
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Lineitem class
 *
 * Create, edit and update billing line item with details of companyservice,
 * units used and total cost
 */
class Lineitem extends Model
{
    use \App\Traits\Userstamps;

    private $_rules = [
    'unitsused' => 'required',
    'bill_id' => 'required',
    'service_id' => 'required'
    ];
                    
    private    $_messages = [
    ];
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    'prorata',
    'unitsused', 
    'bill_id',
    'companyservice_id',
    'maxusage',
    'mincommitment',
    'basecost',
    'extracost',
    'basesubtotal',
    'extrasubtotal',
    'linetotal'
    ];
    

    /**
     * Get the bill for this line item.
     *
     * @return App\Bill parent bill for this lineitem
     */
    public function bill()
    {
        return $this->belongsTo('App\Bill');
    }

    /**
     * Get the company service for this line item.
     *
     * @return App\CompanyServices company service for this lineitem
     */
    public function companyservice()
    {
        return $this->belongsTo('App\CompanyServices');
    }
    
    
    
    /**
     * Show which user created this item
     *
     * @return App\User 
     */
    public function createdby() 
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    /**
     * Show which user updated this line item
     *
     * @return App\User
     */
    public function updatedby() 
    {
        return $this->belongsTo('App\User', 'updated_by');
    }
    
    /**
     * Return the default rules for validating a line item
     *
     * @return Array rules
     */
    public function getRules() 
    {
        return $this->_rules;
    }

    /**
     * Return the error messages when validating a line item
     *
     * @return Array messages
     */
    public function getMessages() 
    {
        return $this->_messages;
    }
    
    
}