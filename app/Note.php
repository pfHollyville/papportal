<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    use \App\Traits\Userstamps;
    private $rules = [
    'message' => 'required|max:500',
	'company_id' => 'required'
    ];
                    
    private    $messages = [
    ];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message','company_id','date'
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    
    public function getRules() 
    {
        return $this->rules;
    }

    public function getMessages() 
    {
        return $this->messages;
    }

    public function createdby() 
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function updatedby() 
    {
        return $this->belongsTo('App\User', 'updated_by', 'id');
    }

    //
}
