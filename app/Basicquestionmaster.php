<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Basicquestionmaster extends Model
{
        use \App\Traits\Userstamps;

    private $rules = [
    'question' => 'required|max:180'
    ];
                    
    private    $messages = [
    ];
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question','category','order'
    ];
    
    public function getRules() 
    {
        return $this->rules;
    }

    public function getMessages() 
    {
        return $this->messages;
    }

}
