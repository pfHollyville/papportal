<?php
/**
 * Line item class file
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Lineitem class
 *
 * Create, edit and update billing line item with details of companyservice,
 * units used and total cost
 */
class Exportlineitem extends Model
{
	protected $table = "vw_billing";
	
    /**
    * Get the bill for this line item.
    *
    * @return App\Bill parent bill for this lineitem
    */
    public function bill()
    {
        return $this->belongsTo('App\Bill');
    }

    
    
}