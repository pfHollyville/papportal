<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;

class CompanyServices extends Model
{
    use \App\Traits\Userstamps;

    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id','service_id','maxusage','mincommitment','units','basecost','pertransactioncost','widget_id','start_date','end_date','status','queueid','datasetid'
    ];

    /**
     * Get the company who this service is provided for.
     */
    public function company()
    {
        return $this->belongsTo('App\Company');
    }
    
    /**
     * Get the service for this company service.
     */
    public function service()
    {
        return $this->belongsTo('App\Service');
    }

    
    /**
     * Get the billing line items for this company service.
     */
    public function billlineitems()
    {
        return $this->hasMany('App\Xerolineitem');
    }
    
    /**
     * Get the web chats for this company service.
     */
    public function webchats()
    {
        return $this->hasMany('App\Webchat', 'widget_id', 'widget_id');
    }

    /**
     * Get the phonecontacts for this company service.
     */
    public function phonecontacts()
    {
        return $this->hasMany('App\Phonecontact', 'datasetid', 'datasetid');
    }


    /**
     * Get the phonecontacts for this company service.
     */
    public function phonelogs()
    {
        return $this->hasMany('App\Cdrlog', 'dataset', 'datasetid');
    }

    /**
     * Get the phonecontacts for this company service.
     */
    public function virtuallogs()
    {
        return $this->hasMany('App\Cdrlog', 'qid', 'queueid');
    }
	
	
    /**
     * Get the Queues for this company service.
     */
    public function queues()
    {
        return $this->hasMany('App\Queue');
    }
    
    
    
    public function getRules() 
    {
        return $this->rules;
    }

    public function getMessages() 
    {
        return $this->messages;
    }

}
