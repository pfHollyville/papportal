<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use \App\Traits\Userstamps;
    private $rules = [
    'question' => 'required|max:180',
    'answer' => 'required',
    'category' => 'required'
    ];
                    
    private    $messages = [
    ];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question','category','order','company_id','answer'
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    
    public function getRules() 
    {
        return $this->rules;
    }

    public function getMessages() 
    {
        return $this->messages;
    }
    //
}
