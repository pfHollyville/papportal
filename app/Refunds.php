<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Refund extends Model
{
    use \App\Traits\Userstamps;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id','amount', 'refund_date','status','reason','notes'
    ];
    

    /**
     * Get the company for this bill.
     */
    public function company()
    {
        return $this->belongsTo('App\Company');
    }


    
    
    public function createdby() 
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updatedby() 
    {
        return $this->belongsTo('App\User', 'updated_by');
    }
    

    
    
}
