<?php 

namespace App\Traits;
use ReCaptcha\ReCaptcha;

trait CaptchaTrait
{

    public function captchaCheck($data)
    {
        if (\App::runningUnitTests()) {
               return 1;
        }

        $response = $data['g-recaptcha-response'];
        $remoteip = $_SERVER['REMOTE_ADDR'];
        $secret   = config('recaptcha.secret');

        $recaptcha = new ReCaptcha($secret);
        $resp = $recaptcha->verify($response, $remoteip);
        if ($resp->isSuccess()) {
            return 1;
        } else {
            return 0;
        }

    }

}
