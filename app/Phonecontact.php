<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phonecontact extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'phonecontactid','loaddate','datasetid', 'sourcefile', 'outcomecode','callback','processtype','agentref','notes','data'
    ];
     

    /**
     * Get the company service that this contact belongs to.
     */
    public function companyService()
    {
        return $this->belongsTo('App\CompanyServices', 'datasetid','datasetid');
    }

    
}
