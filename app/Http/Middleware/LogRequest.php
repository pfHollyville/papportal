<?php

namespace App\Http\Middleware;

use Closure;  
use Illuminate\Support\Facades\Log;

class LogRequest
{

    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        Log::info('app.requests', ['headers' => $request->header(),'server' => $request->server(),'request' => $request->all(), 'response' => $response]);
    }

}