<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;


use Closure;

class Active
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if (Auth::check() && Auth::user()->isActive()) {
				return $next($request);
        }

		Auth::logout();
        return redirect('/')->with('status', ['class'=>'warning','text'=>'Sorry, that account is not recognised!']);
    }
}
