<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;


use Closure;

class Client
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->isClient()) {
			return $next($request);
	    } 

        if (Auth::user()->isAdmin() || Auth::user()->isStaff() ) {
               Auth::logout();
               return redirect('admin/login')->with('status', ['class'=>'warning','text'=>'Sorry, you need to be in the admin pages to do that!']);
        }

		
        
        return redirect('/')->with('status', ['class'=>'warning','text'=>'Sorry, you dont have Client permission to do that!']);

    }
}
