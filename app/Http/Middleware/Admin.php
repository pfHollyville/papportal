<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;


use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->isAdmin() ) {
            return $next($request);
        }

        if (Auth::check() && Auth::user()->isClient() ) {
			return redirect('/')->with('status', ['class'=>'warning','text'=>'Sorry, you dont have permission to do that!']);
        }
		
		
        return redirect('/admin')->with('status', ['class'=>'warning','text'=>'Sorry, you dont have Admin permission to do that!']);
    }
}
