<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Cdrlog;


class CallController extends Controller
{


    public function index()
    {
        $calls = Cdrlog::orderBy('datetime','asc')->paginate(10);
        return view('admin.calls.list', ['calls' => $calls]);
    }

}

