<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use App\Campaign;
use App\Company;
use Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class CampaignController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allCompanies = Company::All();
        
        return view('admin.campaigns.list', ['allCompanies' => $allCompanies]);
    }
    
    public function company($company_id)
    {
        $where[] = ['company_id','=',$company_id];
        $campaigns = Campaign::where($where)->get();
        foreach($campaigns as $key => $campagin) {
               $campaigns[$key]->edit = false;
        }
        return $campaigns->toJSON();
    }

   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        $newcampaign = new Campaign();
        
        $this->validate($request, $newcampaign->getRules(), $newcampaign->getMessages());
        
    
        $newcampaign->fill($request->all());
        if ($newcampaign->save()) {
               return json_encode($newcampaign->id);
        } else {
               return json_encode('error');
        }
        return json_encode('error');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $campaign = Campaign::find($id);
        

        $editrules = [
		'date' => 'required|date',
        'queuename' => 'required|max:180',
        'queueid' => 'required|max:64',
        'phonenumbers' => 'max:180',
        'numberpresentation' => 'max:180',
        'type' => 'required|in:Inbound,Outbound,Broadcast'
        ];
        
        $this->validate($request, $editrules, $campaign->getMessages());
    
        $fields = ['queuename','queueid','type','notes','date','phonenumbers','numberpresentation' ];

        foreach ($fields as $field) {
               $campaign->$field = $request->$field;
        }

    
        if ($campaign->save()) {
               return json_encode(true);
        } else {
               return json_encode(false);
        }
        return json_encode(false);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $campaign = Campaign::find($id);    
        if ($campaign->delete()) {
               return json_encode(true);
        } else {
               return json_encode(false);
        }
    }
    //
}