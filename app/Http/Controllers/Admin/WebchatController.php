<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use App\CompanyServices;
use App\Webchat;
use Validator;

class WebchatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $webchats = Webchat::with(['companyservice'])->orderBy('created_at_date', 'desc')->paginate(10);
        return view('admin.webchats.list', ['webchats' => $webchats]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companyServices = CompanyServices::All();
        return view('admin.webchats.add', ['companyServices'=>$companyServices]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newwebchat = new Webchat();
        
        $this->validate($request, $newwebchat->getRules(), $newwebchat->getMessages());
        
    
        $newwebchat->fill($request->all());
        if ($newwebchat->save()) {
               return redirect(route('admin.webchats.show', ['id' => $newwebchat->id]));
        } else {
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $webchat = Webchat::with(['companyservice'])->find($id);
        return view('admin.webchat.show', ['webchat' => $webchat]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
