<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Alert;
use Validator;


class alertController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alerts = Alert::paginate(10);
        return view('admin.alerts.list', ['alerts' => $alerts]);
    }

	
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.alerts.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newalert = new alert();
        
		$rules = [
			'subject' 	=> 'required',
			'body' 		=> 'required'
		];
		
		$messages = [];
		
        $this->validate($request, $rules, $messages);
        
    
        $newalert->fill($request->all());
        if ($newalert->save()) {
				if($request->ajax()) {
					return json_encode($newalert->id);
				} else {
					return redirect(route('admin.alerts.show', ['id' => $newalert->id]));
				}
        } else {
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $alert = alert::find($id);
        return view('admin.alerts.show', ['alert' => $alert]);
        
    }



}

