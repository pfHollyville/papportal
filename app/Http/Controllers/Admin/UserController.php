<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use App\User;
use App\Company;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users = User::with(['company'])->paginate(10);
        return view('admin.users.list', ['users' => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newuser = new User();
        
        $this->validate($request, $newuser->getRules(), $newuser->getMessages());
        
        $request->merge(['password' => bcrypt($request['password'])]);

        $newuser->fill($request->all());
        if ($newuser->save()) {
               return redirect(route('admin.users.show', ['id' => $newuser->id]));
        } else {
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with('company')->find($id);
        return view('admin.users.show', ['user' => $user]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.users.edit', ['user'=>$user]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        
        if(empty($request->password)) {
               $request->password = "noupdate";
               $request->confpassword = "noupdate";
               $passwordChange = false;
        } else {
               $passwordChange = true;
        }

        $editrules = [
        'first_name' => 'required|max:180',
        'last_name' => 'required|max:180',
        'email' => 'email',
        'password' =>'sometimes|same:confpassword|max:16|min:6',
        'role' =>'in:admin,staff,client',
        'account_status' =>'in:inactive,active'
        ];


        
        $this->validate($request, $editrules, $user->getMessages());
    
        $fields = ['first_name','last_name','email','password','role','account_status' ];

        foreach ($fields as $field) {
            if ($field == 'password') {
                if ($passwordChange) {
                    $user->password = bcrypt($request->password);
                }
            } else {
                $user->$field = $request->$field;
            }
        }
    
        if ($user->save()) {
               return redirect(route('admin.users.show', ['id' => $user->id]));
        } else {
               return redirect(route('admin.users.edit', ['id' => $user->id]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
