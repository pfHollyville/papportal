<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check() && Auth::user()->isClient() ) {
			return redirect('/')->with('status', ['class'=>'warning','text'=>'Sorry, you dont have permission to do that!']);
        }
        return view('admin.dashboard.basic');
    }

    
}
