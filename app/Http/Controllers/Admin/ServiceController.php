<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use App\Servicecategory;
use App\Service;
use Validator;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::with('servicecategory')->paginate(10);
        return view('admin.services.list', ['services' => $services]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Servicecategory::All();
        return view('admin.services.add', ['categories'=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newservice = new Service();
        
        $this->validate($request, $newservice->getRules(), $newservice->getMessages());
        
    
        $newservice->fill($request->all());
        if ($newservice->save()) {
               return redirect(route('admin.services.show', ['id' => $newservice->id]));
        } else {
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service = service::find($id);
        return view('admin.services.show', ['service' => $service]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Servicecategory::All();
        $service = service::find($id);
        return view('admin.services.edit', ['service' => $service,'categories'=>$categories]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Service::find($id);
		
		$editRules = [
			'name' 			=> 'required',
			'units' 		=> 'required|in:per minute,per month,per webchat,per call',
			'accountcode' 	=> 'required'
		];
		
        
        $this->validate($request, $editRules, []);
        
        $fields = ['name','description','maxusage','mincommitment','basecost','extracost','units','active','image','category_id','accountcode'];
    

        foreach ($fields as $field) {
               $service->$field = $request->$field;
        }
    
        if ($service->save()) {
               return redirect(route('admin.services.show', ['id' => $service->id]));
        } else {
               return redirect(route('admin.services.edit', ['id' => $service->id]));
        }

	
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
