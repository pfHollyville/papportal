<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use App\Company;
use App\Service;
use App\CompanyServices;
use Carbon;
use Validator;

class CompanyServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $companies = Company::All();
        $services = Service::All();
        $filter = ['fcompany'=>0,'fstatus'=>'All','fservice'=>0];
        $companyservices = CompanyServices::with(['company','service','webchats'])->paginate(10);
        return view('admin.companyservices.list', ['companyservices' => $companyservices, 'companies' => $companies,'services'=>$services,'filter'=>$filter]);
    }

    public function filter(Request $request)
    {
        $whereArray = [];
        if($request->fcompany > 0) {
               $whereArray[] = ['company_id','=',$request->fcompany];
        } 
        if($request->fservice > 0) {
               $whereArray[] = ['service_id','=',$request->fservice];
        } 
        if($request->fstatus != 'All') {
               $whereArray[] = ['status','=',$request->fstatus];
        } 
        $companyservices = CompanyServices::with(['company','service','webchats'])->where($whereArray)->paginate(10);
                
        $companies = Company::All();
        $services = Service::All();
        $filter = ['fcompany'=>$request->fcompany,'fstatus'=>$request->fstatus,'fservice'=>$request->fservice];
        return view('admin.companyservices.list', ['companyservices' => $companyservices, 'companies' => $companies,'services'=>$services,'filter'=>$filter]);
    }
    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::All();
        $services = Service::where('active',1)->get();
        return view('admin.companyservices.add', ['companies'=>$companies,'services'=>$services]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newcompanyservice = new CompanyServices();

		$end = Carbon\Carbon::now()->addMonth();
		
		$rules = [
			'company_id' => 'required',
			'service_id' => 'required',
			'start_date' => 'required|date_format:d/m/Y',
			'end_date' => 'nullable|date_format:d/m/Y|after:' . $end,
			'status' => 'required|in:pending,approved,rejected,cancelled'
		];

		$messages = [
			'end_date.after' => 'End Date must be at least 28 days in the future'
		];

        $this->validate($request, $rules, $messages);
		$request->merge(['start_date' => Carbon\Carbon::createFromFormat('d/m/Y', $request->start_date)->toDateString()]);
		if ($request->end_date != null) {
			$request->merge(['end_date' => Carbon\Carbon::createFromFormat('d/m/Y', $request->end_date)->toDateString()]);	
		}
    
        $newcompanyservice->fill($request->all());
        if ($newcompanyservice->save()) {
               return redirect(route('admin.companyservices.show', ['id' => $newcompanyservice->id]));
        } else {
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $companyservice = Companyservices::with(['company','service'])->find($id);
        return view('admin.companyservices.show', ['companyservice' => $companyservice]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $companyservice = Companyservices::find($id);
        $services = Service::All();
        $companies = Company::All();
        return view('admin.companyservices.edit', ['companies'=>$companies,'services'=>$services,'companyservice'=>$companyservice]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $companyservice = CompanyServices::find($id);
		
		$end = Carbon\Carbon::now()->addMonth();
		
		$editRules = [
			'company_id' => 'required',
			'service_id' => 'required',
			'start_date' => 'required|date_format:d/m/Y',
			'end_date' => 'nullable|date_format:d/m/Y|after:' . $end,
			'status' => 'required|in:pending,approved,rejected,cancelled'
		];
		
		$messages = [
			'end_date.after' => 'End Date must be at least 28 days in the future'
		];
        
        $this->validate($request, $editRules, $messages);
		$request->merge(['start_date' => Carbon\Carbon::createFromFormat('d/m/Y', $request->start_date)->toDateString()]);
		if ($request->end_date != null) {
			$request->merge(['end_date' => Carbon\Carbon::createFromFormat('d/m/Y', $request->end_date)->toDateString()]);	
		}
        
    
        $fields = ['company_id','service_id','widget_id','datasetid','queueid','basecost','pertransactioncost','units','maxusage','mincommitment' ,'start_date','end_date','status'];

        foreach ($fields as $field) {
               $companyservice->$field = $request->$field;
        }
    
        if ($companyservice->save()) {
               return redirect(route('admin.companyservices.show', ['id' => $companyservice->id]));
        } else {
               return redirect(route('admin.companyservices.edit', ['id' => $companyservice->id]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
