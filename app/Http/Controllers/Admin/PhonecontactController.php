<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Phonecontact;


class PhonecontactController extends Controller
{


    public function index()
    {
        $contacts = Phonecontact::paginate(10);
        return view('admin.phonecontacts.list', ['contacts' => $contacts]);
    }

}

