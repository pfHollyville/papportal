<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Note;
use Validator;
use App\Company;


class NoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allCompanies = Company::All();
        $notes = Note::with(['company'])->paginate(10);
        $filter = ['fcompany'=>0];
        return view('admin.notes.list', ['notes' => $notes, 'allCompanies' => $allCompanies, 'filter'=>$filter]);
    }

    public function filter(Request $request)
    {
		$company = 0;
        $whereArray = [];
		if ($request->has("company")) {
			$company_id =  ltrim($request->company, '0');
			if($company_id > 0) {
				   $whereArray[] = ['company_id','=',$company_id];
			} 
		} 
        $notes = Note::with(['company'])->where($whereArray)->paginate(10);
                
        $allCompanies = Company::All();
        $filter = ['fcompany'=>$request->fcompany];
        return view('admin.notes.list', ['notes' => $notes,'filter'=>$filter,'allCompanies' => $allCompanies]);
    }
	
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::All();
        return view('admin.notes.add', ['companies'=>$companies]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newnote = new Note();
        
        $this->validate($request, $newnote->getRules(), $newnote->getMessages());
        
    
        $newnote->fill($request->all());
        if ($newnote->save()) {
				if($request->ajax()) {
					return json_encode($newnote->id);
				} else {
					return redirect(route('admin.notes.show', ['id' => $newnote->id]));
				}
        } else {
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $note = note::find($id);
        return view('admin.notes.show', ['note' => $note]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     *
    public function edit($id)
    {
        $companies = Company::All();
        $note = Note::find($id);
        return view('admin.notes.edit', ['note'=>$note, 'companies'=>$companies]);
    }
	*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     *
    public function update(Request $request, $id)
    {
        $note = Note::find($id);
        

        $editrules = [
        'note' => 'required|max:180'
        ];
        
        $this->validate($request, $editrules, $note->getMessages());
    
        $fields = ['date','message','company_id' ];

        foreach ($fields as $field) {
               $note->$field = $request->$field;
        }

    
        if ($note->save()) {
               return redirect(route('admin.notes.show', ['id' => $note->id]));
        } else {
               return redirect(route('admin.notes.edit', ['id' => $note->id]));
        }
    }
	*/
	

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     *
    public function destroy($id)
    {
        //
    }
    //
	*/


}

