<?php

namespace App\Http\Controllers\Admin;

use League\Csv\Writer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use App\Company;
use App\CompanyServices;
use App\Bill;
//use App\Lineitem;
use App\Xerolineitem;
use App\Exportlineitem;
use Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;


class BillController extends Controller
{
	
	private $start_date;
	private $end_date;
	private $month;
	private $year;
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allCompanies = Company::All();
        $filter = ['fcompany'=>0];
        $bills = Bill::with(['xerolineitems','company'])->paginate(10);
        return view('admin.bills.list', ['bills' => $bills, 'filter'=>$filter,'allCompanies' => $allCompanies]);
        
    }

    public function filter(Request $request)
    {
        $whereArray = [];
        if($request->fcompany > 0) {
               $whereArray[] = ['company_id','=',$request->fcompany];
        } 
        $bills = Bill::with(['xerolineitems','company'])->where($whereArray)->paginate(10);
                
        $allCompanies = Company::All();
        $filter = ['fcompany'=>$request->fcompany];
        return view('admin.bills.list', ['bills' => $bills, 'filter'=>$filter,'allCompanies' => $allCompanies]);
    }
    
    /**
     * Get a list of valid CompanyServices for this bill
     *
     * Request will include the company and start and end dates.
     * Search for CompanyServices where the start date is after bill start and end date is blank or after bill end date.
     * The "prorata" value is calculated just in case a service started or ended part way through the bill period.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $company_id Key to company for this bill
     * @param  date                     $start_date Start date for this bill period
     * @param  date                     $end_date   end Date for this bill period
     * @return \Illuminate\Http\Response
     */
    public function getServices(Request $request)
    {
        
        $company_id = $request->company_id;
        $first_bill_date = new Carbon($request->start_date);
        //		$first_bill_date = new Carbon('first day of last month');
        $first_bill_date->setTime(0, 0, 0);
        $last_bill_date = new Carbon($request->end_date);
        //		$last_bill_date = new Carbon('last day of last month');
        $last_bill_date->setTime(23, 59, 59);

        $where = [
        ['company_id','=',$company_id],
        //['status','=','Approved'],
        ['start_date', '<=', $last_bill_date],
        ];

        $company = Company::find($company_id);
        $companyservices = CompanyServices::where($where)->where(
            function ($query) use ($first_bill_date) {
                $first_bill_date = new Carbon('first day of last month');
                $query->whereNull('end_date')->orWhere('end_date', '>=', $first_bill_date);
            }
        )
		->with(['phonelogs'=> function ($query) use ($first_bill_date,$last_bill_date) {
				$query->whereBetween('datetime',[$first_bill_date,$last_bill_date]);
		}])
		->withCount(['webchats AS onlinechats' => function ($query) use ($first_bill_date,$last_bill_date) {
				$query->where('type', 'chat')->whereBetween('created_at_date',[$first_bill_date,$last_bill_date]);
		}])->get();
		
        
        $xerolineitems = [];
        foreach($companyservices as $companyservice) {
			// check if the company service started or ended this month and pro rata the costs accordingly
            if ($companyservice->end_date == null || $companyservice->end_date >= $last_bill_date) {
                $prorata = 1;
            } else {
                $enddate = new Carbon($companyservice->end_date);
                $days = $first_bill_date->diffInDays($enddate);
                $prorata = round(($days /  $first_bill_date->daysInMonth), 2);
            }
			
			// create base costs for services that have a fixed charge element
			if($companyservice->service->basecost != null) {
               $xerolineitems[] = [
                'id' => $companyservice->id,
                'description' => $companyservice->service->name . " BASE",
                'unitamount' => $companyservice->service->basecost,
                'maxusage' => null,
                'accountcode' => $companyservice->service->accountcode,
                'discount' => 0,
                'quantity' => $prorata,
                'total' => round(($prorata * floatval($companyservice->service->basecost)), 2)
				];
			}

			// create variable cost for services that have a unit cost element
			if($companyservice->service->extracost > 0) {
				$quantity = 0;
				// check the service has a webchat id
				if ($companyservice->widget_id) {
					// now get the count of webchats
					$quantity = $companyservice->onlinechats_count;
				}

				// check if the service has a dataset (linked to phone charges)
				if ($companyservice->datasetid) {
					// now get the count of webchats
						$quantity = $companyservice->phonelogs->sum('duration');
						if ($quantity > 0) {
							$quantity = round(($quantity / 60),2);
						}
				}

				
				$total = ($quantity - $companyservice->service->maxusage) * $companyservice->service->extracost;
				if ($total < 0) {
					$total = 0;
				}
				
               $xerolineitems[] = [
                'id' => $companyservice->id,
                'description' => $companyservice->service->name . " USAGE",
                'unitamount' => $companyservice->service->extracost,
                'maxusage' => $companyservice->service->maxusage,
                'accountcode' => $companyservice->service->accountcode,
                'discount' => 0,
                'quantity' => $quantity,
                'total' => $total
               ];
			}
        }

        
        return json_encode(['xerolineitems' => $xerolineitems,'billdetails'=>['company_id'=>$company->id,'company'=>$company,'start_date'=>$first_bill_date->toDateString(),'end_date'=>$last_bill_date->toDateString()]]);
    }

    /**
     * Create a new bill
     *
     * Show the "add" bill view where the user can choose a company and set the start and end date for this bill
     *
     * @return \Illuminate\Http\Response
     */
    function create()
    {
        $companies = Company::select(['id','companyname'])->get()->toJSON();
        return view('admin.bills.add', ['companies'=>$companies]);
        
    }
    /**
     * Store a newly created bill and associated lineitems.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //		return json_encode([$request->lineitems,$request->billdetails,$request->total]);

        $rules = [
        'billdetails.company_id' => 'required',
        'billdetails.start_date' => 'required|date',
        'billdetails.end_date' => 'required|date',
        'xerolineitems' => 'required',
        ];
                    
        $messages = [
        'billdetails.start_date.required' => 'You must provide a start date for this bill',
        'billdetails.end_date.required' => 'You must provide an end date for this bill',
        ];

        $this->validate($request, $rules, $messages);

        $newBill = new Bill();
        
        $newBill->company_id = $request->billdetails['company_id'];
        $newBill->start_date = $request->billdetails['start_date'];
        $newBill->end_date = $request->billdetails['end_date'];
        $newBill->status = "draft";
        $newBill->total = $request->total;
        
        if ($newBill->save()) {
               $linetotal = 0;
            foreach($request->xerolineitems as $xerolineitem) {
                $newXerolineitem = new Xerolineitem();
                $newXerolineitem->bill_id = $newBill->id;
                $newXerolineitem->company_services_id = $xerolineitem['id'];
                $newXerolineitem->description = $xerolineitem['description'];
                $newXerolineitem->accountcode = $xerolineitem['accountcode'];
                $newXerolineitem->discount = $xerolineitem['discount'];
                $newXerolineitem->unitamount = $xerolineitem['unitamount'];
				if ($xerolineitem['quantity'] > $xerolineitem['maxusage']) {
					$newXerolineitem->quantity = $xerolineitem['quantity'] - $xerolineitem['maxusage'];
				} else {
					$newXerolineitem->quantity = 0;
				}
                $newXerolineitem->total = round((($newXerolineitem['quantity'] * $newXerolineitem['unitamount']) * ((100 - $newXerolineitem['discount'])/100)),2);
                $linetotal += $newXerolineitem->total;
                
                $newXerolineitem->save();
            }
            if (round($linetotal,2) == round($request->total,2)) {
                // something does not add up
                return json_encode(true);
            } else {
                return json_encode([false,'errors'=>[['invalid total'=>'total bill does not match input'],['total'=>$request->total,'line'=>$linetotal]]]);
            }
            
        }
        return json_encode([false,'errors'=>[['invalid new bill'=>'new bill not saved']]]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bill = Bill::with('xerolineitems', 'company')->find($id);
		$bill->invoicenumber = $bill->getInvoiceNumber();
        return view('admin.bills.show', ['bill' => $bill]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

	
    /**
     * Create all bills for a set month
     *
     * Show the "all bills" bill view where the user can choose a month and create all bills for companies for that month
     *
     */
    function createAllBills()
    {
        return view('admin.bills.allbills' );
        
    }
	
	


    /**
     * Find all companies and create a bill for each of them
     * get the start and end dates from the request
     */
	public function saveAllBills($year, $month) {
		
		// check that no bills already exist fo this period
		$bills = Bill::where([["year",$year],["month",$month]])->get();
		
		if ($bills->count()) {
			return json_encode(1);
			
		}
		
		
		$this->year = $year;
		$this->month = $month;
		
		$start_date = Carbon::now();
		$start_date->setDate($year, $month, 1)->setTime(0, 0, 0)->toDateTimeString();

		$end_date = Carbon::now();
		$end_date->setDate($year, $month + 1, 1)->setTime(0, 0, 0)->toDateTimeString();
		
		
        $this->start_date = $start_date;
        $this->end_date = $end_date;
		

		// get all companies
		$companies = Company::All();
		
		foreach ($companies as $company) {
			// create a bill for each company
			$this->saveBill($company->id);
		}
	
		return json_encode(2);
	
	}
	
	public function export($year, $month) 
	{
		// now create the extract
		$bills = Bill::where([['year', '=', $year],['month','=', $month]])->pluck('id')->toArray();
		$exportlineitems = Exportlineitem::whereIn('bill_id',$bills)->join('bills','bills.id','vw_billing.bill_id')->orderBy('bills.company_id')->orderBy("AccountCode")->get();
										
		$csv = Writer::createFromFileObject(new \SplTempFileObject());

		
		$headers = ["ContactName","EmailAddress","POAddressLine1","POAddressLine2","POAddressLine3","POAddressLine4","POCity","PORegion","POPostalCode","POCountry","InvoiceNumber","Reference","InvoiceDate","DueDate","InventoryItemCode","Description","Quantity","UnitAmount","Discount","AccountCode","TaxType","TaxAmount","TrackingName1","TrackingName2","TrackingName3","Currency","BrandingTheme"];

		$csv->insertOne($headers);
		
		foreach($exportlineitems as $exportlineitem)
		{
			$row = [];
			foreach($headers as $field)
			{
				$row[] = 	$exportlineitem[$field];			
			}
			$csv->insertOne($row);
			
		}
		
		$fileName = $year . "-" . $month . '.csv';
		
		return new Response((string) $csv, 200, [
			'Content-Encoding' => 'none',
			'Content-Type' => 'text/csv; charset=UTF-8',
			'Content-Disposition' => 'attachment; filename="' . $fileName . '"',
			'Content-Description' => 'File Transfer',
		]);
		
//		$csv->output($year . "-" . $month . '.csv');
//		exit;
										
	}
	

    /**
     * Save bill for the requested company
     * get the start and end dates from the class
     * @param  int	$company_id		id of company for the billl
     */
    public function saveBill($company_id)
    {
 
        $newBill = new Bill();
        
        $newBill->company_id = $company_id;
        $newBill->start_date = $this->start_date;
        $newBill->end_date = $this->end_date;
        $newBill->status = "draft";
        $newBill->year = $this->year;
        $newBill->month = $this->month;		
        $newBill->total = 0;
        
        if ($newBill->save()) {
				$total = $this->saveLineItems($newBill);
				$newBill->total = $total;
				$newBill->save();
        }

    }


	
    /**
     * save the line items for this bill
     *
     * Request will include the company and start and end dates.
     * Search for CompanyServices where the start date is after bill start and end date is blank or after bill end date.
     * The "prorata" value is calculated just in case a service started or ended part way through the bill period.
     *
     * @param  int                      $company_id Key to company for this bill
     * @param  date                     $start_date Start date for this bill period
     * @param  date                     $end_date   end Date for this bill period
     * @param  int  	                $bill_id	unique id for this bill
     * @return int		$total			total bill value
     */
    public function saveLineItems(Bill $bill)
    {
        

        $where = [
			['company_id','=',$bill->company_id],
			['start_date', '<=', $bill->end_date],
        ];

        $companyservices = CompanyServices::where($where)->where(
            function ($query) use ($bill) {
                $query->whereNull('end_date')->orWhere('end_date', '>=', $bill->start_date);
            }
        )
		->with(
			['phonelogs'=> function ($query) use ($bill) {
				$query->whereBetween('datetime',[$bill->start_date,$bill->end_date]);
				}
			])
		->with(
			['virtuallogs'=> function ($query) use ($bill) {
				$query->where('type','out')->whereBetween('datetime',[$bill->start_date,$bill->end_date]);
				}
			])
		->withCount(['webchats AS onlinechats' => function ($query) use ($bill) {
				$query->where('type', 'chat')->whereBetween('created_at_date',[$bill->start_date,$bill->end_date]);
		}])->get();
		
        
        $xerolineitems = [];
        foreach($companyservices as $companyservice) {
			// check if the company service started or ended this month and pro rata the costs accordingly
            if ($companyservice->end_date == null || $companyservice->end_date >= $bill->end_date) {
                $prorata = 1;
            } else {
                $enddate = new Carbon($companyservice->end_date);
                $days = $bill->start_date->diffInDays($enddate);
                $prorata = round(($days /  $bill->start_date->daysInMonth), 2);
            }
			
			// create base costs for services that have a fixed charge element
			if($companyservice->service->basecost != null) {
               $xerolineitems[] = [
                'id' => $companyservice->id,
                'description' => $companyservice->service->name . " per month",
                'unitamount' => $companyservice->service->basecost,
                'maxusage' => null,
                'accountcode' => $companyservice->service->accountcode,
                'discount' => 0,
                'quantity' => $prorata,
                'total' => round(($prorata * floatval($companyservice->service->basecost)), 2)
				];
			}

			// create variable cost for services that have a unit cost element
			if($companyservice->service->extracost > 0) {
				$quantity = 0;
				// check the service has a webchat id
				if ($companyservice->widget_id) {
					// now get the count of webchats
					$quantity = $companyservice->onlinechats_count;
				}

				// check if the service has a dataset (linked to phone charges)
				if ($companyservice->datasetid) {
					// now get the number of minutes
					
					if ($companyservices->units = "per call") {
						$quantity = $companyservice->phonelogs->count();
					}
					if ($companyservices->units = "per minute") {
						$quantity = $companyservice->phonelogs->sum('duration');
						if ($quantity > 0) {
							$quantity = round(($quantity / 60),2);
						}
					}
				}

				// check if the service has a qid (linked to phone charges) but no dataset id - ie it is a virtual number
				if ($companyservice->queueid > 0 && $companyservice->datasetid == null) {
					// now get the number of minutes
					if ($companyservices->units = "per call") {
						$quantity = $companyservice->virtuallogs->count();
					}
					if ($companyservices->units = "per minute") {
						$quantity = $companyservice->virtuallogs->sum('duration');
						
						if ($quantity > 0) {
							$quantity = round(($quantity / 60),2);
						}
					}
				}
				
				
				
				$quantity = $quantity - $companyservice->service->maxusage;
				if ($quantity < 0) {
					$quantity = 0;
				}
				$total = $quantity * $companyservice->service->extracost;
				if ($total < 0) {
					$total = 0;
				}
				
               $xerolineitems[] = [
                'id' => $companyservice->id,
                'description' => $companyservice->service->name . " " . $companyservice->service->units,
                'unitamount' => $companyservice->service->extracost,
                'maxusage' => $companyservice->service->maxusage,
                'accountcode' => $companyservice->service->accountcode,
                'discount' => 0,
                'quantity' => $quantity,
                'total' => $total
               ];
			}
        }
		
		// now save the line items
		$linetotal = 0;
		foreach($xerolineitems as $xerolineitem) {
			$newXerolineitem = new Xerolineitem();
			$newXerolineitem->bill_id = $bill->id;
			$newXerolineitem->companyservices_id = $xerolineitem['id'];
			$newXerolineitem->description = $xerolineitem['description'];
			$newXerolineitem->accountcode = $xerolineitem['accountcode'];
			$newXerolineitem->discount = $xerolineitem['discount'];
			$newXerolineitem->unitamount = $xerolineitem['unitamount'];
			$newXerolineitem->quantity = $xerolineitem['quantity'];
			$newXerolineitem->total = round((($newXerolineitem['quantity'] * $newXerolineitem['unitamount']) * ((100 - $newXerolineitem['discount'])/100)),2);
			$linetotal += $newXerolineitem->total;
			
			$newXerolineitem->save();
		}

		return $linetotal;
	

        
    }
	
	
	
}
