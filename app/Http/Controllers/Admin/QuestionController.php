<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Question;
use Validator;
use App\Company;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allCompanies = Company::All();
        $questions = Question::with(['company'])->paginate(10);
        $filter = ['fcompany'=>0,'fqtype'=>0];
        return view('admin.questions.list', ['questions' => $questions, 'allCompanies' => $allCompanies, 'filter'=>$filter]);
    }

    public function filter(Request $request)
    {
		$company = 0;
		$qtype = 2;
        $whereArray = [];
		if ($request->has("company")) {
			$company_id =  ltrim($request->company, '0');
			if($company_id > 0) {
				   $whereArray[] = ['company_id','=',$company_id];
			} 
		} 
		if ($request->has("qtype")) {
			if($request->qtype < 2) {
				$qtype = $request->qtype;
				$whereArray[] = ['basic','=',$qtype];
			}
		}
        $questions = Question::with(['company'])->where($whereArray)->paginate(10);
                
        $allCompanies = Company::All();
        $filter = ['fcompany'=>$request->fcompany,'fqtype'=>$request->fqtype];
        return view('admin.questions.list', ['questions' => $questions,'filter'=>$filter,'allCompanies' => $allCompanies]);
    }

    public function basic(Request $request)
    {
		$company = 0;
//		$qtype = 2;
		if ($request->has("company")) {
			$company_id =  ltrim($request->company, '0');
			if($company_id > 0) {
				   $faqwhereArray = [['company_id','=',$company_id], ['basic','=',0]];
				   $bwhereArray = [['company_id','=',$company_id],['basic','=',1]];
			} 
		} 
//		if ($request->has("qtype")) {
//			if($request->qtype < 2) {
//				$qtype = $request->qtype;
//				$whereArray[] = ['basic','=',$qtype];
//			}
//		}

        $basicquestions = Question::with(['company'])->where($bwhereArray)->get();

        $faqquestions = Question::with(['company'])->where($faqwhereArray)->get();

		
        return view('admin.questions.basiclist', ['faqquestions' => $faqquestions, 'basicquestions' => $basicquestions]);
    }
	
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::All();
        return view('admin.questions.add', ['companies'=>$companies]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newquestion = new Question();
        
        $this->validate($request, $newquestion->getRules(), $newquestion->getMessages());
        
    
        $newquestion->fill($request->all());
        if ($newquestion->save()) {
               return redirect(route('admin.questions.show', ['id' => $newquestion->id]));
        } else {
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question = question::find($id);
        return view('admin.questions.show', ['question' => $question]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $companies = Company::All();
        $question = Question::find($id);
        return view('admin.questions.edit', ['question'=>$question, 'companies'=>$companies]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $question = Question::find($id);
        

        $editrules = [
        'question' => 'required|max:180'
        ];
        
        $this->validate($request, $editrules, $question->getMessages());
    
        $fields = ['category','question','order','answer','company_id' ];

        foreach ($fields as $field) {
               $question->$field = $request->$field;
        }

    
        if ($question->save()) {
               return redirect(route('admin.questions.show', ['id' => $question->id]));
        } else {
               return redirect(route('admin.questions.edit', ['id' => $question->id]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    //
}
