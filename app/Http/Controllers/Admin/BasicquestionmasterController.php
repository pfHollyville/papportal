<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use App\Basicquestionmaster;
use Validator;

class BasicquestionmasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $basicquestionmasters = Basicquestionmaster::paginate(10);
        return view('admin.basicquestionmasters.list', ['basicquestionmasters' => $basicquestionmasters]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.basicquestionmasters.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newbasicquestionmaster = new Basicquestionmaster();
        
        $this->validate($request, $newbasicquestionmaster->getRules(), $newbasicquestionmaster->getMessages());
        
    
        $newbasicquestionmaster->fill($request->all());
        if ($newbasicquestionmaster->save()) {
               return redirect(route('admin.basicquestionmasters.show', ['id' => $newbasicquestionmaster->id]));
        } else {
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $basicquestionmaster = basicquestionmaster::find($id);
        return view('admin.basicquestionmasters.show', ['basicquestionmaster' => $basicquestionmaster]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $basicquestionmaster = Basicquestionmaster::find($id);
        return view('admin.basicquestionmasters.edit', ['basicquestionmaster'=>$basicquestionmaster]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $basicquestionmaster = Basicquestionmaster::find($id);
        

        $editrules = [
        'question' => 'required|max:180'
        ];
        
        $this->validate($request, $editrules, $basicquestionmaster->getMessages());
    
        $fields = ['category','question','order' ];

        foreach ($fields as $field) {
               $basicquestionmaster->$field = $request->$field;
        }
    
        if ($basicquestionmaster->save()) {
               return redirect(route('admin.basicquestionmasters.show', ['id' => $basicquestionmaster->id]));
        } else {
               return redirect(route('admin.basicquestionmasters.edit', ['id' => $basicquestionmaster->id]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
