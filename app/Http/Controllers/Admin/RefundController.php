<?php

namespace App\Http\Controllers\Admin;

use League\Csv\Writer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use App\Company;
use App\Refund;
use Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class RefundController extends Controller
{
	
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allCompanies = Company::All();
        $filter = ['fcompany'=>0];
        $refunds = Refund::paginate(10);
        return view('admin.refunds.list', ['refunds' => $refunds, 'filter'=>$filter,'allCompanies' => $allCompanies]);
        
    }

    public function filter(Request $request)
    {
        $whereArray = [];
        if($request->fcompany > 0) {
               $whereArray[] = ['company_id','=',$request->fcompany];
        } 
        $refunds = Refund::with(['company'])->where($whereArray)->paginate(10);
                
        $allCompanies = Company::All();
        $filter = ['fcompany'=>$request->fcompany];
        return view('admin.refunds.list', ['refunds' => $refunds, 'filter'=>$filter,'allCompanies' => $allCompanies]);
    }
    

    /**
     * Create a new bill
     *
     * Show the "add" bill view where the user can choose a company and set the start and end date for this bill
     *
     * @return \Illuminate\Http\Response
     */
    function create()
    {
        $companies = Company::select(['id','companyname'])->get()->toJSON();
        return view('admin.refunds.add', ['companies'=>$companies]);
        
    }
    /**
     * Store a newly created bill and associated lineitems.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
        'company_id' => 'required',
        'refund_date' => 'required|date',
        'reason' => 'required',
		'status' => 'required|in:pending,approved,rejected,cancelled'
        ];
                    
        $messages = [
        ];

        $this->validate($request, $rules, $messages);

        $newRefund = new Refund();
        
        $newRefund->company_id = $request->company_id;
        $newRefund->refund_date = $request->refund_date;
        $newRefund->status = "draft";
        $newRefund->amount = $request->amount;
        $newRefund->reason = $request->reason;
        $newRefund->notes = $request->notes;
        
        if ($newBill->save()) {
            
        }
        return json_encode([false,'errors'=>[['invalid new refund'=>'new refund not saved']]]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $refund = Refund::with('company')->find($id);
        return view('admin.refunds.show', ['refund' => $refund]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $refund = Refund::with('company')->find($id);
        return view('admin.refunds.edit', ['refund' => $refund]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $refund = Refund::->find($id);

		
        $rules = [
        'refund_date' => 'required|date',
        'reason' => 'required',
		'status' => 'required|in:pending,approved,rejected,cancelled'
        ];
                    
        $messages = [
        ];

        $this->validate($request, $rules, $messages);
		

        $refund->refund_date = $request->refund_date;
        $refund->status = $request->status;
        $refund->amount = $request->amount;
        $refund->reason = $request->reason;
        $refund->notes = $request->notes;



        
        if ($refund->save()) {
            
        }

		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

	
}
