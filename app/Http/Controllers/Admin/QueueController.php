<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use App\Queue;
use App\Company;
use App\CompanyServices;
use Validator;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class QueueController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allCompanies = Company::All();
        
        return view('admin.queues.list', ['allCompanies' => $allCompanies]);
    }
    
    public function company($company_id)
    {
		$csArray = Company::find($company_id)->companyservices()->pluck("id")->toArray();
		
        $queues = Queue::whereIn('company_services_id',$csArray)->get();
        foreach($queues as $key => $queue) {
               $queues[$key]->edit = false;
        }
		
        $companyservices = CompanyServices::with(
			['service'=> 
				function($query) 
				{
						$query->select('id','name','description');
				}
			])
			->where('company_id',$company_id)
			->get();
		
		$data = ["queues" => $queues, "companyservices" => $companyservices];
        return json_encode($data);
    }

   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        $newqueue = new queue();
        
        $this->validate($request, $newqueue->getRules(), $newqueue->getMessages());
        
    
        $newqueue->fill($request->all());
        if ($newqueue->save()) {
               return json_encode($newqueue->id);
        } else {
               return json_encode('error');
        }
        return json_encode('error');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $queue = queue::find($id);
        

        $editrules = [
		'date' => 'required|date',
        'queuename' => 'required|max:180',
        'queueid' => 'required|max:64',
        'phonenumbers' => 'max:180',
        'numberpresentation' => 'max:180',
        'type' => 'required|in:Inbound,Outbound,Broadcast'
        ];
        
        $this->validate($request, $editrules, $queue->getMessages());
    
        $fields = ['queuename','queueid','type','notes','date','phonenumbers','numberpresentation','company_services_id' ];

        foreach ($fields as $field) {
               $queue->$field = $request->$field;
        }

    
        if ($queue->save()) {
               return json_encode(true);
        } else {
               return json_encode(false);
        }
        return json_encode(false);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $queue = queue::find($id);    
        if ($queue->delete()) {
               return json_encode(true);
        } else {
               return json_encode(false);
        }
    }
    //
}