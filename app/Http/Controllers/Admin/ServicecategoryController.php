<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use App\Servicecategory;
use Validator;

class ServicecategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servicecategories = Servicecategory::paginate(10);
        return view('admin.servicecategories.list', ['servicecategories' => $servicecategories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.servicecategories.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newservicecategory = new Servicecategory();
        
        $this->validate($request, $newservicecategory->getRules(), $newservicecategory->getMessages());
        
    
        $newservicecategory->fill($request->all());
        if ($newservicecategory->save()) {
               return redirect(route('admin.servicecategories.show', ['id' => $newservicecategory->id]));
        } else {
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $servicecategory = servicecategory::find($id);
        return view('admin.servicecategories.show', ['servicecategory' => $servicecategory]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servicecategory = Servicecategory::find($id);
        return view('admin.servicecategories.edit',['servicecategory'=>$servicecategory]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $servicecategory = Servicecategory::find($id);
		
		$editRules = [
			'name' => 'required',
			'queue' => 'required|in:No,Optional,Mandatory'
		];
		
        
        $this->validate($request, $editRules);
        
    
        $fields = ['name','description','image','queue'];

        foreach ($fields as $field) {
               $servicecategory->$field = $request->$field;
        }
    
        if ($servicecategory->save()) {
               return redirect(route('admin.servicecategories.show', ['id' => $servicecategory->id]));
        } else {
               return redirect(route('admin.servicecategories.edit', ['id' => $servicecategory->id]));
        }
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
