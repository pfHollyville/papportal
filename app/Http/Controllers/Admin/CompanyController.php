<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller;
use App\User;
use App\Company;
use App\Basicquestionmaster;
use App\Question;
use App\Industry;
use App\Note;
use App\Legalstatus;
use App\Usagereason;
use Validator;
use Illuminate\Support\Facades\Auth;


class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
	public function createXeroContact($id)
	{
		$company = Company::find($id);
		$company->createXeroContact();
		
	}
	 
    public function index()
    {
        $allCompanies = Company::All();
        $industries = Industry::All();
        $filter = ['fcompany'=>0,'findustry'=>0];
        $companies = Company::with(['industry','legalstatus','usagereason','user'])->paginate(10);
        return view('admin.companies.list', ['companies' => $companies, 'industries'=>$industries,'filter'=>$filter,'allCompanies' => $allCompanies]);
        
    }

    public function filter(Request $request)
    {
        $whereArray = [];
        if($request->fcompany > 0) {
               $whereArray[] = ['id','=',$request->fcompany];
        } 
        if($request->findustry > 0) {
               $whereArray[] = ['industry_id','=',$request->findustry];
        } 
        $companies = Company::with(['industry','legalstatus','usagereason','user'])->where($whereArray)->paginate(10);
                
        $allCompanies = Company::All();
        $industries = Industry::All();
        $filter = ['fcompany'=>$request->fcompany,'findustry'=>$request->findustry];
        return view('admin.companies.list', ['companies' => $companies, 'industries'=>$industries,'filter'=>$filter,'allCompanies' => $allCompanies]);
    }
    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::whereDoesntHave('company')->where('role', '=', 'client')->get();
        $industries = Industry::orderBy('name')->get();
        $legalstatuses = Legalstatus::orderBy('name')->get();
        $usagereasons = Usagereason::orderBy('name')->get();
        return view('admin.companies.add', ['lookups'=>['users'=>$users,'industries'=>$industries,'legalstatuses' => $legalstatuses, 'usagereasons' => $usagereasons]]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newcompany = new Company();
        
        $this->validate($request, $newcompany->getRules(), $newcompany->getMessages());
        
    
        $newcompany->fill($request->all());
        if ($newcompany->save()) {
				$newcompany->createXeroContact();
			
               $basics = Basicquestionmaster::All();
				foreach($basics as $basic) {
					$newQuestion = new Question();
					$newQuestion->company_id = $newcompany->id;
					$newQuestion->basic = true;
					$newQuestion->question = $basic->question;
					$newQuestion->category = $basic->category;
					$newQuestion->order = $basic->order;
					$newQuestion->save();
				}
				return redirect(route('admin.companies.show', ['id' => $newcompany->id]));
        } else {
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::with(['notes.updatedby' => function($query) { $query->select('id','first_name','last_name');}])->find($id);
        return view('admin.companies.show', ['company' => $company]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        $industries = Industry::orderBy('name')->get();
        $legalstatuses = Legalstatus::orderBy('name')->get();
        $usagereasons = Usagereason::orderBy('name')->get();
        return view('admin.companies.edit', ['company'=>$company,'lookups'=>['industries'=>$industries,'legalstatuses' => $legalstatuses, 'usagereasons' => $usagereasons]]);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $company = Company::find($id);

		$editRules = [
//		'companyname' => 'required|max:180|unique:companies,companyname,' . $id, 
		'companyname' => 'required|max:180',
		'workemail' => 'email|max:180'
		];

        
        $this->validate($request, $editRules, $company->getMessages());
        
    
        $fields = ['companyname', 'companynumber','address1', 'address2','city', 'county','postcode','country','workemail','workphone', 'industry_id','legalstatus_id','usagereason_id', 'xero_id', 'webvisitorspermonth','callspermonth','averagesale' ];

        foreach ($fields as $field) {
               $company->$field = $request->$field;
        }
    
        if ($company->save()) {
               return redirect(route('admin.companies.show', ['id' => $company->id]));
        } else {
               return redirect(route('admin.companies.edit', ['id' => $company->id]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	

	
}
