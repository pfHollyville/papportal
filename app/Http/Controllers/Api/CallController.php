<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Call;
use App\Cdrlog;
use Carbon\Carbon;

class CallController extends Controller
{


	private $range;
	private $startDate;
	private $endDate;
		

	private function setRange(Carbon $startDate, Carbon $endDate)
	{
		$this->startDate = $startDate;
		$this->endDate = $endDate;
		$start = $startDate->startOfDay()->timestamp;
		$end = $endDate->endOfDay()->timestamp;
		$range = $start . ":" . $end;
		
		$this->range = $range;
		
		
		
	}

	public function chargeable(Request $request) {
		
		$data = json_decode($request->data);
		
		
		foreach($data as $item) {
			$cdr = Cdrlog::find($item->id);
			if ($item->checked) {
				$cdr->chargeable = 1;
			} else {
				$cdr->chargeable = 0;
			}
			$cdr->save();
		}
		
		return 1;
		
	}
	
    public function getCalls(Request $request)
    {
		
		if ($request->has('start')) {
			$start_date = Carbon::createFromFormat('d/m/Y', $request->start);
		} else {
			$start_date = Carbon::yesterday();
		}
		if ($request->has('end')) {
			$end_date = Carbon::createFromFormat('d/m/Y', $request->end);
		} else {
			$end_date = Carbon::yesterday();
		}
		
		
		$this->setRange($start_date, $end_date);
		
	
		$this->getData();
	
	}	
	
	private function destroyExisting()
	{
		$existingCalls = Cdrlog::where([
			['datetime', '>=', $this->startDate->toDateString()],
			['datetime', '<=', $this->endDate->toDateString()]
		])->pluck('id')->toArray();
		Cdrlog::Destroy($existingCalls);
		
	}
	
	public function getData()
	{
		$this->destroyExisting();
		
		$token = $this->getToken();
		if($token) {
			
			$client = new Client([
				// Base URI is used with relative requests
				'base_uri' => 'https://api-106.dxi.eu',
				// You can set any number of default request options.
				'timeout'  => 15.0,
			]);
			

			$query =	[
				"query" => [
						'token'  => $token,
						'method' => 'cdr_log',
						'action' => 'read',
						'tstart'  => $this->startDate->toDateString() . " 00:00:00",
						'tstop'   => $this->endDate->toDateString() . " 23:59:59",
						'groupby'=> 'qid',
						'format' => 'json'
						]
				];
			
			
			$response = $client->request("get", 'database.php', $query);
			
			/*
			$response = $client->request("get", 'reporting.php', [
					"query" => [
						'token'  => $token,
						'method' => 'calls',
						'fields' => 'qid,date,cid,nc_in,nc_out,sec_talk',
						'groupby'=> 'qid,date',
						'range'  => $this->range,
						'format' => 'json'
						]
					]);
			*/		
				
			$contents = json_decode($response->getBody()->getContents(),true);
			
			
			if ($contents['success'] == true) {
				
			$fields = [
					"callid","qid","dataset","urn","agent","ddi","cli","ringtime",
					"duration","result","outcome","type","datetime","answer",
					"disconnect","last_update","carrier",
					"flags","terminate","customer","customer_cost"
			];

				
				foreach($contents['list'] as $data) {
					/*
					$newcall = new Call;
					$newcall->qid = $data['qid']; 
					$newcall->cid = $data['cid']; 
					$newcall->date = $data['date']; 
					$newcall->nc_in = $data['nc_in']; 
					$newcall->nc_out = $data['nc_out']; 
					$newcall->sec_talk = $data['sec_talk']; 
					
					$newcall->save();
					*/
					
					$newcdrlog = new Cdrlog;
					foreach($fields as $field) {
						if ($data[$field] == "0000-00-00 00:00:00") {
							$data[$field] = null;
						}
						$newcdrlog->$field = $data[$field];
					}
					$newcdrlog->save();
					
					
				}
				echo true;
				
				
			} else {
				echo false;
			}
			
		}
		
	}
	
	private function getToken()
	{
		$username="PAProsAPI";
		$password="gp68SAbxuFsUlc8oit";
		
		$client = new Client([
			// Base URI is used with relative requests
			'base_uri' => 'https://api-106.dxi.eu',
			// You can set any number of default request options.
			'timeout'  => 15.0,
		]);
		
        $response = $client->request("get", 'token.php', [
				"query" => [
					'action' => 'get',
					'username'=> $username, 
					'password'=> $password, 
					]
				]);
		$contents = json_decode($response->getBody()->getContents(),true);

		if ($contents['success'] == true) {
			return $contents['token'];
		} else {
			return false;
		}
		
		
	}
	
	private function validateToken()
	{
		
	}

}
