<?php
namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use App\Webchat;
use Carbon\Carbon;
use App\Mail\ChatEmail;
use App\Mail\OfflineEmail;
use Illuminate\Support\Facades\Mail;

// Class: SechatController
// receive HTTP Post from Snap Engage containing chat details
class SechatController extends Controller
{

	// Method: psotMessage
	// HTTP post with json content of chat conversation
	//
	// creates <webchat> records
	//
	// sends email using <ChatEmail> pr <OfflineEmail> templates
    public function postMessage(Request $request)
    {
		$json = $request->json()->all();

		
		$chatFields = [
			"id",
			"widget_id",
			"url",
			"snapshot_image_url",
			"type",
			"requested_by",
			"requester_details",
			"description",
			"created_at_date",
			"created_at_seconds",
			"created_at_milliseconds",
			"proactive_chat",
			"page_url",
			"referrer_url",
			"entry_url",
			"ip_address",
			"user_agent",
			"browser",
			"os",
			"country_code",
			"country",
			"region",
			"city",
			"latitude",
			"longitude",
			"source_id",
			"chat_waittime",
			"chat_duration",
			"language_code",
			"transcripts",
			"plugins",
			"javascript_variables",
			"operator_variables"
		];

		$requesterDetails = [
			"name",
			"emails",
			"name_profile_link",
			"phones",
			"address",
			"address_2",
			"city",
			"state",
			"zip",
			"country",
			"company_name",
			"company_profile_link",
			"employees",
			"revenue",
			"title",
			"website",
			"social_profile_links",
			"gender",
			"age",
			"influencer_score",
			"notes",
			"industry",
			"avatars",
			"other_data"
		];
		
		$jsonFields = [
			"transcripts",
			"plugins",
			"javascript_variables",
			"operator_variables",
			"emails",
			"phones",
			"social_profile_links",
			"avatars",
			"other_data"
		];

        $newwebchat = new Webchat();
		
		foreach($chatFields as $field) {
			
			if (array_key_exists($field, $json)) {
				if ($field == 'requester_details') {
					$requester = $json['requester_details'];
					foreach($requesterDetails as $detail) {
						if (array_key_exists($detail, $requester)) {
							if (in_array($detail, $jsonFields)) {
								$detail_field = "requester_details_" . $detail;
								$newwebchat[$detail_field] = json_encode($requester[$detail]);
							} else {
								$detail_field = "requester_details_" . $detail;
								$newwebchat[$detail_field] = $requester[$detail];
							}
						}
					}
				} elseif (in_array($field, $jsonFields)) {
					$newwebchat[$field] = json_encode($json[$field]);				
				} elseif ($field == 'id') {
					$newwebchat['chat_id'] = $json[$field];
				} elseif ($field == 'created_at_date') {
					$newwebchat[$field] = Carbon::createFromFormat('M j, Y g:i:s A', $json[$field])->toDateTimeString();
									
				} else {
					$newwebchat[$field] = $json[$field];
				}
			}
		}
		
		if ($newwebchat->save()) {
			if ($newwebchat->type =="chat") {
				Mail::send(new ChatEmail($newwebchat));
			}
			if ($newwebchat->type =="offline") {
				Mail::send(new OfflineEmail($newwebchat));
			}
			return (['success' => true]);
		} else {
			return (['success' => false]);
			
		}
    }

	
    
}
