<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Service;
use App\Xerolineitem;


class ServiceController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $service = Service::find($id);
        return $service;
        
    }

	
}
