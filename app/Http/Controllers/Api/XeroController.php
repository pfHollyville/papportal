<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use XeroPrivate;
use App\Company;
use App\CompanyServices;
use App\Xerolineitem;
use App\Exportlineitem;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Xerobill;

class XeroController extends Controller
{

	private $start_date;
	private $end_date;
	private $month;
	private $year;
	private $company;
	private $xerobill;

	
	public function getBranding()
	{
		$branding = XeroPrivate::load('Accounting\\BrandingTheme')->execute();
		
		return json_encode($branding);
	}
	
	public function getInvoices()
	{
//		$invoice = XeroPrivate::loadByGUID($invoice_id);
		$invoices = XeroPrivate::load('Accounting\\Invoice')->where('Contact.ContactID','f5a77e82-50e3-4340-a6e0-13d6a482a08a')->execute();

		return $invoices;
		
	}
	
	public function saveXerobill() {
		$xerobill = new Xerobill;
		
		$xerobill->year = $this->year;
		$xerobill->month = $this->month;
		$xerobill->company_id = $this->company->id;
		$xerobill->status = 'in progress';
		
		$xerobill->save();
		
		$this->xerobill = $xerobill;
		
	}
	
	public function saveInvoice()
	{
		$xero = app('XeroPrivate');
		$invoice = app('XeroInvoice');
		$contact = app('XeroContact');

		// Set up the invoice contact
		$contact->setContactID($this->company->xero_id);

		// Assign the contact to the invoice
		$invoice->setContact($contact);

		// Set the type of invoice being created (ACCREC / ACCPAY)
		$invoice->setType('ACCREC');
		$invoice->setBrandingThemeID(env("XERO_BRANDING_THEME_ID"));

		$dateInstance = new \DateTime();
		$dateDueInstance = new \DateTime();
		$invoice->setDate($dateInstance);
		$invoice->setDueDate($dateDueInstance->modify('+7 days'));
		
		

		$invoice->setCurrencyCode('GBP');
		$invoice->setStatus('Draft');

		$xeroFirstMonthLines = $this->getFirstMonthLines();
		$xeroMonthlyLines = $this->getMonthlyLines();
		$xeroUsageLines = $this->getLineItems();
		
		$xeroLines = array_merge($xeroFirstMonthLines, $xeroMonthlyLines,$xeroUsageLines);
		
		if (count($xeroLines)) {
		
			foreach ($xeroLines as $line) {
				// Add the line to the order
				$invoice->addLineItem($line);
			}
			
			$this->saveXerobill();
			
			$invoice->setInvoiceNumber($this->xerobill->getInvoiceNumber());

			$output = $xero->save($invoice);

			$this->xerobill->invoice_id = $invoice->InvoiceID;
			$this->xerobill->status = "draft";
			$this->xerobill->save();
			
			
		}
		
	
	}

    /**
     * Find all companies and create a bill for each of them
     * get the start and end dates from the request
     */
	public function saveAllInvoices($year, $month) {
		
		
		$this->year = $year;
		$this->month = $month;
		
		$start_date = Carbon::now();
		$start_date->setDate($year, $month, 1)->setTime(0, 0, 0)->toDateTimeString();

		$end_date = Carbon::now();
		$end_date->setDate($year, $month + 1, 1)->setTime(0, 0, 0)->toDateTimeString();
		
		
        $this->start_date = $start_date;
        $this->end_date = $end_date;
		

		// get all companies
		$companies = Company::All();
		
		foreach ($companies as $company) {
			$this->company = $company;
			if ($company->xero_id != "0") {
				$this->saveInvoice();
			}
			
		}
	
	
	}

    private function formatMonth() 
    {
        return str_pad($this->month, 2, "0", STR_PAD_LEFT);
    }


	private function getFirstMonthLines() {
		
		$month_start_date = Carbon::now();
		$month_start_date->setDate($this->year, $this->month, 1)->setTime(0, 0, 0);

		$month_end_date = Carbon::now();
		$month_end_date->setDate($this->year, $this->month, 1)->setTime(23, 59, 59)->modify('last day of this month');


        $where = [
			['company_id','=',$this->company->id],
			['basecost', '>', 0],
			['status', '=', 'approved']
			
        ];

		
        $companyservices = CompanyServices::where($where)->whereBetween("start_date",[$month_start_date,$month_end_date])->get();


        $xeroLines = [];
        foreach($companyservices as $companyservice) {
			
			// check if the company service started or ended this month and pro rata the costs accordingly
            $startdate = new Carbon($companyservice->start_date);
			if ($startdate < $month_start_date) {
				$startdate = $month_start_date;
			}

			if ($companyservice->end_date == null) {
				$enddate = $month_end_date;
			} else {
				$enddate = new Carbon($companyservice->end_date);
				if ($enddate > $month_end_date) {
					$enddate = $month_end_date;					
				}
			}
			
            if ($startdate == $month_start_date && $enddate == $month_end_date) {
                $prorata = 1;
			} else {
				$days = $startdate->diffInDays($enddate);
                $prorata = round(($days /  $month_start_date->daysInMonth), 2);
            }
			
			
			// create base costs for services that have a fixed charge element
			$line = app('XeroInvoiceLine');
			$line->setDescription($companyservice->service->name . " first month (" . $startdate->format("d/m/Y") . " to " . $enddate->format("d/m/Y") . ")");
			$line->setQuantity($prorata);
			$line->setUnitAmount($companyservice->basecost);
			$line->setAccountCode($companyservice->service->accountcode);
			$line->setLineAmount(round(($prorata * floatval($companyservice->basecost)), 2));
			$xeroLines[] = $line;

		}
		
		return $xeroLines;
		
	}

	
	
	private function getMonthlyLines() {
		
		$nextmonth_start_date = Carbon::now();
		$nextmonth_start_date->setDate($this->year, $this->month, 1)->setTime(0, 0, 0)->addMonth();

		$nextmonth_end_date = Carbon::now();
		$nextmonth_end_date->setDate($this->year, $this->month, 1)->setTime(23, 59, 59)->modify('last day of next month');


        $where = [
			['company_id','=',$this->company->id],
			['start_date', '<', $nextmonth_start_date->toDateTimeString()],
			['basecost', '>', 0],
			['status', '=', 'approved']
        ];

		
        $companyservices = CompanyServices::where($where)->where(
            function ($query)  {
				$nextmonth_start_date = Carbon::now();
				$nextmonth_start_date->setDate($this->year, $this->month, 1)->setTime(0, 0, 0)->addMonth();
                $query->whereNull('end_date')->orWhere('end_date', '>=', $nextmonth_start_date->toDateTimeString());
            }
        )->get();


        $xeroLines = [];
        foreach($companyservices as $companyservice) {
			
			// check if the company service started or ended this month and pro rata the costs accordingly
            $startdate = new Carbon($companyservice->start_date);
			if ($startdate < $nextmonth_start_date) {
				$startdate = $nextmonth_start_date;
			}

			if ($companyservice->end_date == null) {
				$enddate = $nextmonth_end_date;
			} else {
				$enddate = new Carbon($companyservice->end_date);
				if ($enddate > $nextmonth_end_date) {
					$enddate = $nextmonth_end_date;					
				}
			}
			
            if ($startdate == $nextmonth_start_date && $enddate == $nextmonth_end_date) {
                $prorata = 1;
			} else {
				$days = $startdate->diffInDays($enddate);
                $prorata = round(($days /  $nextmonth_start_date->daysInMonth), 2);
            }
			
			
			// create base costs for services that have a fixed charge element
			$line = app('XeroInvoiceLine');
			$line->setDescription($companyservice->service->name . " per month (" . $startdate->format("d/m/Y") . " to " . $enddate->format("d/m/Y") . ")");
			$line->setQuantity($prorata);
			$line->setUnitAmount($companyservice->basecost);
			$line->setAccountCode($companyservice->service->accountcode);
			$line->setLineAmount(round(($prorata * floatval($companyservice->basecost)), 2));
			$xeroLines[] = $line;

		}
		
		return $xeroLines;
		
	}
	
    public function getLineItems()
    {
        
        $where = [
			['company_id','=',$this->company->id],
			['start_date', '<=', $this->end_date],
			['pertransactioncost', '>', 0],
			['status', '=', 'approved']
        ];

        $companyservices = CompanyServices::where($where)->where(
            function ($query)  {
                $query->whereNull('end_date')->orWhere('end_date', '>=', $this->start_date);
            }
        )
		->with(
			['phonelogs'=> function ($query) {
				$query->where('chargeable',1)->whereBetween('datetime',[$this->start_date,$this->end_date]);
				}
			])
		->with(
			['virtuallogs'=> function ($query) {
				$query->where('type','out')->whereBetween('datetime',[$this->start_date,$this->end_date]);
				}
			])
		->withCount(['webchats AS onlinechats' => function ($query)  {
				$query->where('type', 'chat')->whereBetween('created_at_date',[$this->start_date,$this->end_date]);
		}])->get();
		
        
        $xeroLines = [];
        foreach($companyservices as $companyservice) {
			
			// create variable cost for services that have a unit cost element
				$quantity = 0;
				// check the service has a webchat id
				if ($companyservice->widget_id) {
					// now get the count of webchats
					$quantity = $companyservice->onlinechats_count;
				}

				// check if the service has a dataset (linked to phone charges)
				if ($companyservice->datasetid) {
					// now get the number of minutes
					
					if ($companyservice->units = "per call") {
						$quantity = $companyservice->phonelogs->count();
					}
					if ($companyservice->units = "per minute") {
						$quantity = $companyservice->phonelogs->sum('duration');
						if ($quantity > 0) {
							$quantity = round(($quantity / 60),2);
						}
					}
				}

				// check if the service has a qid (linked to phone charges) but no dataset id - ie it is a virtual number
				if ($companyservice->queueid > 0 && $companyservice->datasetid == null) {
					// now get the number of minutes
					if ($companyservice->units = "per call") {
						$quantity = $companyservice->virtuallogs->count();
					}
					if ($companyservice->units = "per minute") {
						$quantity = $companyservice->virtuallogs->sum('duration');
						
						if ($quantity > 0) {
							$quantity = round(($quantity / 60),2);
						}
					}
				}
				
				
				
				$quantity = $quantity - $companyservice->maxusage;
				if ($quantity < 0) {
					$quantity = 0;
				}
				$total = $quantity * $companyservice->pertransactioncost;
				if ($total < 0) {
					$total = 0;
				}

				if ($quantity > 0) {
					$start = Carbon::now();
					$start = $start->setDate($this->year, $this->month, 1)->format("d/m/Y");

					$end = Carbon::now();
					$end = $end->setDate($this->year, $this->month, 1)->modify('last day of this month')->format("d/m/Y");
					
					$line = app('XeroInvoiceLine');
					$line->setDescription($companyservice->service->name . " " . $companyservice->units . " (" . $start . " to " . $end . ")" );
					$line->setQuantity($quantity);
					$line->setUnitAmount($companyservice->pertransactioncost);
					$line->setLineAmount($total);
	                $line->setAccountCode($companyservice->service->accountcode);
					$xeroLines[] = $line;
				}
        }
		
		return $xeroLines;

        
    }


	
	
}
