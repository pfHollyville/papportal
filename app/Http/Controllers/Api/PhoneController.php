<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Phonecontact;
use Carbon\Carbon;
use App\Mail\PhonecontactEmail;
use Illuminate\Support\Facades\Mail;

class PhoneController extends Controller
{


    public function postPhone(Request $request)
    {
		
		$data = $request->data;
		$data = json_decode($data,true);
		
		$data = array_change_key_case($data, CASE_LOWER);
		
		
		$newContact = new Phonecontact;

		if (array_key_exists('id', $data)) {
			$data['phonecontactid'] = $data['id'];
			unset($data['id']);
		}

		if (array_key_exists('callback', $data)) {
			if ($data['callback'] == '0000-00-00 00:00:00') {
				unset($data['callback']);
			}
		}

		
		$fields = [
        'phonecontactid','loaddate','datasetid', 'sourcefile', 'outcomecode','callback','processtype','agentref','notes'
		];

		
		
		foreach($fields as $field) {
			if (array_key_exists($field, $data)) {
				$newContact[$field] = $data[$field];
			}
			
		}
		$newContact->data = json_encode($data, JSON_FORCE_OBJECT);
					
		if ($newContact->save()) {
			Mail::send(new PhonecontactEmail($newContact));
			return json_encode($newContact->id);			
		} else {
			return json_encode(false);
		}


					
		
	}

}
