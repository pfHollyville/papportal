<?php

/*
namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Vinkla\Pusher\Facades\Pusher;
use Illuminate\Support\Facades\Log;

class WebchatController extends Controller
{
    var $pusher;
    var $user;
    var $chatChannel;

    const DEFAULT_CHAT_CHANNEL = 'chat';

    public function __construct()
    {
        $this->pusher = App::make('pusher');
        //        $this->user = Session::get('user');
        $this->chatChannel = self::DEFAULT_CHAT_CHANNEL;
    }

    public function postMessage(Request $request)
    {
        
        $message = [
            'text' => e($request->input('chat_text')),
            'username' => 'client',
            'avatar' => '/images/client.png',
            'timestamp' => (time()*1000)
        ];
        $this->pusher->trigger($this->chatChannel, 'client-message', $message);
    }


    public function postClientEvent(Request $request)
    {
        Log::info(
            'client event', [
            'request' => $request->all()
            ]
        );
    }

    public function postChannelEvent(Request $request)
    {
        Log::info(
            'channel event', [
            'request' => $request->all()
            ]
        );
        
    }
    
}
*/