<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Company;
use App\Basicquestionmaster;
use App\Question;
use App\Industry;
use App\Legalstatus;
use App\Usagereason;
use Validator;

/**
 * Company Controller class
 *
 * Client controller to manage company records on the fontend application.
 * Clients can only create or update their own company record.
 */
class CompanyController extends Controller
{
    /**
     * Display company owned by current client.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('company.show', ['id' => Auth::user()->company->id]);
    }

    /**
     * Show the form for creating a new company.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // check that the user does  not have a company already
        if (Auth::user()->hasCompany()) {
               return redirect()->route('company.show', ['id' => Auth::user()->company->id])->with('status', ['class'=>'info','text'=>'You already have a company defined']);
        } else {
               $industries = Industry::orderBy('name')->get();
               $legalstatuses = Legalstatus::orderBy('name')->get();
               $usagereasons = Usagereason::orderBy('name')->get();
               return view('company.add', ['lookups'=>['industries'=>$industries,'legalstatuses' => $legalstatuses, 'usagereasons' => $usagereasons]]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newcompany = new Company();
        
        $this->validate($request, $newcompany->getRules(), $newcompany->getMessages());
        
    
        $newcompany->fill($request->all());
        $newcompany->user_id = Auth::user()->id;
        if ($newcompany->save()) {
				$newcompany->createXeroContact();
               // now get the basic questions
               $basics = Basicquestionmaster::All();
            foreach($basics as $basic) {
                $newQuestion = new Question();
                $newQuestion->company_id = $newcompany->id;
                $newQuestion->basic = true;
                $newQuestion->question = $basic->question;
                $newQuestion->category = $basic->category;
                $newQuestion->order = $basic->order;
                $newQuestion->save();
            }
            
               return redirect(route('company.show', ['id' => $newcompany->id]));
        } else {
            
        }
    }

    /**
     * Display the company record.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);
        return view('company.show', ['company' => $company]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
