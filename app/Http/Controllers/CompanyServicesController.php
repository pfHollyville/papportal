<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Company;
use App\Service;
use App\CompanyServices;
use Validator;

class CompanyServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::All();
        $companyservices = CompanyServices::where('company_id', '=', Auth::user()->company->id)->with(['service'])->get();
        return view('companyservices.list', ['companyservices' => $companyservices, 'services'=>$services]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::All();
        $services = Service::All();
        return view('companyservices.add', ['companies'=>$companies,'services'=>$services]);
    }

    /**
     * Store the company service by pulling through the values from the Service it is based on.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newcompanyservice = new CompanyServices();
        $rules = [
        'service_id' => 'required|integer|min:1',
        'start_date' => 'required|date|after:yesterday',
        ];
        
        $messages = [
        'start_date.after' => 'Services can only start in the future',
        'service_id.min' => 'You need to choose a service from the list',
        ];
        
        $this->validate($request, $rules, $messages);
        
		$service = Service::find($request->service_id);
		
        $newcompanyservice->company_id = Auth::user()->company->id;
        $newcompanyservice->status = "Pending";
        $newcompanyservice->start_date = $request->start_date;
        $newcompanyservice->end_date = null;
        $newcompanyservice->basecost = $service->basecost;
        $newcompanyservice->pertransactioncost = $service->extracost;
        $newcompanyservice->maxusage = $service->maxusage;
        $newcompanyservice->mincommitment = $service->mincommitment;
        $newcompanyservice->units = $service->units;
        $newcompanyservice->service_id = $request->service_id;
        
    
        if ($newcompanyservice->save()) {
               return json_encode($newcompanyservice->id);
        } else {
               return json_encode('error', 'unable to save');
            
        }
        return json_encode('error', 'unable to process');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $companyservice = Companyservices::with(['company','service'])->find($id);
        return view('companyservices.show', ['companyservice' => $companyservice]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $companyservice = Companyservices::find($id);
        $services = Service::All();
        $companies = Company::All();
        return view('companyservices.edit', ['companies'=>$companies,'services'=>$services,'companyservice'=>$companyservice]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $companyservice = CompanyServices::find($id);
        
        $this->validate($request, $companyservice->getRules(), $companyservice->getMessages());
        
    
        $fields = ['company_id','service_id','widget_id','basecost','pertransactioncost' ];

        foreach ($fields as $field) {
               $companyservice->$field = $request->$field;
        }
    
        if ($companyservice->save()) {
               return redirect(route('companyservices.show', ['id' => $companyservice->id]));
        } else {
               return redirect(route('companyservices.edit', ['id' => $companyservice->id]));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
