<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->isClient()) {    
            if (Auth::user()->hasCompany()) {
                return view('home');
            } else {
                return redirect()->route('company.create')->with('status', ['class'=>'info','text' =>'Please setup your company profile.']);
            }
        }
        if (Auth::user()->isAdmin()||Auth::user()->isStaff()) {    
               Auth::logout();
               return redirect()->route('admin.login')->with('status', ['class'=>'warning','text' =>'Sorry, you are not a client user']);
        }
        
    }
}
