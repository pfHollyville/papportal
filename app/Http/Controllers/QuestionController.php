<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Question;
use Validator;
use App\Company;

class QuestionController extends Controller
{
    
    private $filter = false;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ($this->filter) {
               $where[] = $this->filter;
        } 
        $where[] = ['company_id','=',Auth::user()->company->id];

        $questions = Question::where($where)->get();
        foreach($questions as $key => $question) {
               $questions[$key]->edit = false;
        }
        $questions = $questions->toJSON();
        
        return view('questions.list', ['questions' => $questions,'company_id' =>Auth::user()->company->id]);
    }
    
    public function basic()
    {
        $this->filter = ['basic','=',true];
        return $this->index();
    }

    public function faq()
    {
        $this->filter = ['basic','=',false];
        return $this->index();
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::All();
        return view('questions.add', ['companies'=>$companies]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company_id = Auth::user()->company->id;
    
        $newquestion = new Question();
        $newquestion->company_id = $company_id;
        
        $this->validate($request, $newquestion->getRules(), $newquestion->getMessages());
        
    
        $newquestion->fill($request->all());
        if ($newquestion->save()) {
               return json_encode($newquestion->id);
            //			return redirect(route('questions.show' ,['id' => $newquestion->id]));
        } else {
               return json_encode('error');
        }
        return json_encode('error');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question = question::find($id)->toJSon();
        
        echo $question;
        exit;
        
        
        
        return view('questions.show', ['question' => $question]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $companies = Company::All();
        $question = Question::find($id);
        return view('questions.edit', ['question'=>$question, 'companies'=>$companies]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $question = Question::find($id);
        

        $editrules = [
        'question' => 'required|max:180',
        'answer' => 'required|max:180'
        ];
        
        $this->validate($request, $editrules, $question->getMessages());
    
        $fields = ['category','question','answer' ];

        foreach ($fields as $field) {
               $question->$field = $request->$field;
        }

    
        if ($question->save()) {
               return json_encode(true);
            //				return redirect(route('questions.show' ,['id' => $question->id]));
        } else {
               return json_encode(false);
            //			return redirect(route('questions.edit' ,['id' => $question->id]));
        }
        return json_encode(false);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::find($id);    
        if ($question->delete()) {
               return json_encode(true);
        } else {
               return json_encode(false);
        }
    }
    //
}
