<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Basicquestionmaster;
use XeroPrivate;

class Company extends Model
{
    use \App\Traits\Userstamps;

    private $rules = [
    'companyname' => 'required|max:180', 
    'workemail' => 'email|max:180'
    ];
                    
    private    $messages = [
    'companyname.required' => 'You must provide a company name',
    'workemail.email' => 'That does not look like a valid email address'
    ];
    
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','companyname', 'companynumber','address1', 'address2','city', 'county','postcode','country','workemail','workphone', 'industry_id','legalstatus_id','usagereason_id','xero_id','webvisitorspermonth','callspermonth','averagesale'
    ];


    
    
    /**
     * Get the client who relates to this company.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the services for this company.
     */
    public function companyServices()
    {
        return $this->hasMany('App\CompanyServices');
    }

	
    /**
     * Get the notes for this company.
     */
    public function notes()
    {
        return $this->hasMany('App\Note');
    }
	
	

    /**
     * Get the questions for this company.
     */
    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    public function basicQuestions()
    {
        return $this->hasMany('App\Question')->where('basic', '=', true);
    }

    public function faqQuestions()
    {
        return $this->hasMany('App\Question')->where('basic', '=', false);
    }
    
    
    /**
     * Get the industry for this company.
     */
    public function industry()
    {
        return $this->belongsTo('App\Industry');
    }

    /**
     * Get the legal status for this company.
     */
    public function legalstatus()
    {
        return $this->belongsTo('App\Legalstatus');
    }
    
    /**
     * Get the industry for this company.
     */
    public function usagereason()
    {
        return $this->belongsTo('App\Usagereason');
    }
    
    public function createdby() 
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function updatedby() 
    {
        return $this->belongsTo('App\User', 'updated_by', 'id');
    }
    
    public function getRules() 
    {
        return $this->rules;
    }

    public function getMessages() 
    {
        return $this->messages;
    }
    
    public function getRef() 
    {
        return str_pad($this->id, 4, "0", STR_PAD_LEFT);
    }

	public function createXeroContact()
	{

		$xero = app('XeroPrivate');
		$contact = app('XeroContact');

		// Set up the invoice contact
		$contact->setIsCustomer(true);
		$contact->setName($this->companyname);
		$contact->setFirstName($this->user->first_name);
		$contact->setLastName($this->user->last_name);
		$contact->setEmailAddress($this->user->email);

		$output = $xero->save($contact);
		
		$this->xero_id = $contact->ContactID;
		$this->save();
		

	}

    
}
