<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Webchat extends Model
{

    private $rules = [
    'widget_id' => 'required'
    ];
                    
    private    $messages = [
    ];

	
    protected $fillable = [
			"id",
			"widget_id",
			"url",
			"snapshot_image_url",
			"type",
			"requested_by",
			"requester_details",
			"description",
			"created_at_date",
			"created_at_seconds",
			"created_at_milliseconds",
			"proactive_chat",
			"page_url",
			"referrer_url",
			"entry_url",
			"ip_address",
			"user_agent",
			"browser",
			"os",
			"country_code",
			"country",
			"region",
			"city",
			"latitude",
			"longitude",
			"source_id",
			"chat_waittime",
			"chat_duration",
			"language_code",
			"transcripts",
			"plugins",
			"javascript_variables",
			"operator_variables",
			"name",
			"emails",
			"name_profile_link",
			"phones",
			"address",
			"address_2",
			"city",
			"state",
			"zip",
			"country",
			"company_name",
			"company_profile_link",
			"employees",
			"revenue",
			"title",
			"website",
			"social_profile_links",
			"gender",
			"age",
			"influencer_score",
			"notes",
			"industry",
			"avatars",
			"other_data"
    ];
	
    //
    /**
     * Get the company service for this chat.
     */
    public function companyservice()
    {
        return $this->belongsTo('App\CompanyServices', 'widget_id', 'widget_id');
    }

    public function getRules() 
    {
        return $this->rules;
    }

    public function getMessages() 
    {
        return $this->messages;
    }
    
    
}
