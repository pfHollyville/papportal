<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use \App\Traits\Userstamps;
    
    private $rules = [
		'name' 			=> 'required|max:180',
		'units' 		=> 'required|in:per minute,per month,per webchat,per call',
		'accountcode' 	=> 'required'
    ];
                    
    private    $messages = [
    ];
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','description','maxusage','mincommitment','basecost','extracost','units','active','image','category_id','accountcode'
    ];


    /**
     * Get the category for this service.
     */
    public function servicecategory()
    {
        return $this->hasOne('App\Servicecategory', 'id', 'category_id');
    }

    /**
     * Get the category for this service.
     */
    public function companyservices()
    {
        return $this->hasMany('App\CompanyServices');
    }
    
    
    
    public function getRules() 
    {
        return $this->rules;
    }

    public function getMessages() 
    {
        return $this->messages;
    }
}
