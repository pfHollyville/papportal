<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    use \App\Traits\Userstamps;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subject','body'
    ];


    public function createdby() 
    {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function updatedby() 
    {
        return $this->belongsTo('App\User', 'updated_by', 'id');
    }

    //
}
