<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Legalstatus extends Model
{
    /**
     * Get the companies for this status.
     */
    public function companies()
    {
        return $this->hasMany('App\Company');
    }
}
