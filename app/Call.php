<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Call extends Model
{
    use \App\Traits\Userstamps;

	

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date','qid', 'cid', 'nc_in', 'nc_out', 'sec_talk'
    ];
    
    
    
    public function createdby() 
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updatedby() 
    {
        return $this->belongsTo('App\User', 'updated_by');
    }
    
    
}
