<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Api\CallController;
use Illuminate\Support\Facades\Log;

class GetCalls extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'calls:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieve all calls from previous day';
	
	

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

	
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		Log::info('start calls:get');
		$req = new \Illuminate\Http\Request();

        $callControl   = new CallController;
		$callControl->getCalls($req);
		Log::info('complete calls:get');
    }
}
