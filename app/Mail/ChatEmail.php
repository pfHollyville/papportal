<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Webchat;

class ChatEmail extends Mailable
{
    use Queueable, SerializesModels;

	/**
     * The order instance.
     *
     * @var Order
     */
    public $webchat;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Webchat $chat)
    {
        $this->webchat = $chat;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
		
        return $this->view('mail.chatemail')
							->to($this->webchat->companyService->company->workemail)
							->replyTo('client@handlr.co.uk', 'Handlr Support')
							->bcc('SEtranscript@handlr.co.uk')
							->subject("Handlr: New Web Chat");
    }
}
