<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Phonecontact;
use App\CompanyServices;
use App\Company;
use App\User;
use Carbon\Carbon;

// class: PhonecontactEmail
class PhonecontactEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $phonecontact;

    public function __construct(Phonecontact $contact)
    {
        $this->phonecontact = $contact;
    }



    /**
	 * Method: build
     * Build the message by linking it to the phonecontactemail view.
	 * Includes seting the email details:
     *
	 * - replyTo
	 * - bcc
	 * - to
	 * - subject
	 *
     * Return:
	 * 	View
     */
    public function build()
    {
		$comments = false;
		$processdate = false;
		$enquirytype = false;
		$agent = false;
		
		
		$labels['firstname'] = 'First Name';
		$labels['lastname']  = 'Last Name';
		$labels['firstname'] = 'First Name';
		$labels['homephone'] = 'Main Phone';
		$labels['mobilephone'] = 'Secondary Phone';
		$labels['servicesreq'] = 'Services Required';
		
		$contactfields = ['title','firstname','lastname','address1','address2','address3','postcode','homephone','mobilephone','email'];
		
		
		$data = json_decode($this->phonecontact->data, true);

		if (array_key_exists('agentref',$data)) {
			$agent = $this->formatAgent($data['agentref']);
		}
		
		
		if (array_key_exists('processdate',$data)) {
			$date = new Carbon($data['processdate']);
			$processdate = $date->format('d/m/Y H:i');
		}

		if (array_key_exists('comments',$data)) {
			$comments = $data['comments'];
		}

		if (array_key_exists('enquirytype',$data)) {
			$enquirytype = $data['enquirytype'];
		}
		
		$data = $this->removeFields($data);
		
			
        return $this->view('mail.phonecontactemail')
							->to($this->phonecontact->companyService->company->workemail)
							->replyTo('client@handlr.co.uk', 'Handlr Support')
							->subject("Handlr: New Phone Contact")
							->with([
								'client'		=>	$this->phonecontact->companyService->company->user,
								'data'			=>	$data,
								'labels' 		=>	$labels,
								'agent' 		=>	$agent,
								'contactfields' =>	$contactfields,
								'processdate' 	=>	$processdate,
								'enquirytype' 	=>	$enquirytype,
								'comments' 		=>	$comments
								]);
    }


	private function formatAgent($agent)
	{
		$names = explode(" ", $agent);
		$output = $names[0];
		if (count($names) > 1) {
				for($i = 1; $i < count($names); $i++) {
					$initials[] = substr($names[$i],0,1);
				}
				
			$output .= " " . implode(" ", $initials);
		}

		return $output;
		
	}
	
	private function removeFields($source)
	{
		$fields = ['processtype','sourcefile','loaddate','phonecontactid','datasetid','outcomecode','notes', 'agentref','comments','processdate','enquirytype'];

		foreach ($fields as $field) {
			if (array_key_exists($field,$source)) {
					unset($source[$field]);
			}
		}
		
		return $source;
		
	}
	
	
}
