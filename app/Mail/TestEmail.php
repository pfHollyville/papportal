<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use Carbon\Carbon;
use Sichikawa\LaravelSendgridDriver\SendGrid;


class TestEmail extends Mailable
{
    use Queueable, SerializesModels, SendGrid;

	/**
     * The order instance.
     *
     * @var Order
     */
    public $phonecontact;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

		return $this
            ->view('mail.testemail')
            ->subject('subject')
            ->from('from@example.com')
            ->to(['paul.fleming@hollyville.co.uk'])
            ->template_id("3ae6fd5f-3854-4b66-bbaa-9bfe28d38b1f");
	

		
    }


	
	
}
