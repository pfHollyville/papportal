<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usagereason extends Model
{
    //
    /**
     * Get the companies for this reason.
     */
    public function companies()
    {
        return $this->hasMany('App\Company');
    }
}
