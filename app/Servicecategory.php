<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicecategory extends Model
{
    private $rules = [
    'name' => 'required|max:180|unique:servicecategories',
    'description' => 'max:180',
    'image' => 'max:180'
    ];
                    
    private    $messages = [
    ];
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','description','image','queue'
    ];


    /**
     * Get the services for this category.
     */
    public function services()
    {
        return $this->hasMany('App\Service');
    }

    
    public function getRules() 
    {
        return $this->rules;
    }

    public function getMessages() 
    {
        return $this->messages;
    }
    
}
