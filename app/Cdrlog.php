<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cdrlog extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
			"callid","qid","dataset","urn","agent","ddi","cli","ringtime",
			"duration","result","outcome","type","datetime","answer",
			"disconnect","last_update","carrier",
			"flags","terminate","customer","customer_cost","chargeable"
    ];
    
    
    
}
