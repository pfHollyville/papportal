<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Xerobill extends Model
{
    use \App\Traits\Userstamps;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'year','month','status', 'company_id','notes','invoice_id'
    ];
    

    /**
     * Get the company for this bill.
     */
    public function company()
    {
        return $this->belongsTo('App\Company');
    }



    
    public function createdby() 
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updatedby() 
    {
        return $this->belongsTo('App\User', 'updated_by');
    }
    
    public function getRules() 
    {
        return $this->rules;
    }

    public function getMessages() 
    {
        return $this->messages;
    }

	public function getInvoiceNumber()
	{
		return "H" . str_pad($this->id,6,"0",STR_PAD_LEFT);
	}
    
    
}
