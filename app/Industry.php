<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    //
    /**
     * Get the companies for this industry.
     */
    public function companies()
    {
        return $this->hasMany('App\Company');
    }
}
