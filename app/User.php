<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;

class User extends Authenticatable
{
    use \App\Traits\Userstamps;
    
    private $rules = [
    'first_name' => 'required|max:180',
    'last_name' => 'required|max:180',
    'email' => 'required|unique:users|max:180|email',
    'password' =>'required|same:confpassword|filled|min:6|max:16',
    'role' =>'in:admin,staff,client',
    'account_status' =>'in:inactive,active'
    ];

            
    private    $messages = [
    'email.email' => 'Please provide a valid email address!',
    'email.required' => 'We need to know your e-mail address!',
    'password.same' => 'Sorry but the password does not match the confirmation value!',
    'first_name' => 'Please confirm your first name',
    'company_number' => 'Please confirm your last name'
    ];

    
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password','role', 'account_status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
     * Get the copmany record associated with the user.
     */
    public function company()
    {
        return $this->hasOne('App\Company');
    }

    /**
     * Get the comapny notes by this user.
     */
    public function notes()
    {
        return $this->hasMany('App\Note');
    }

    
    public function getRules() 
    {
        return $this->rules;
    }

    public function getMessages() 
    {
        return $this->messages;
    }
    
    public function isStaff() 
    {
        if ($this->role == 'admin'|| $this->role == 'staff') {
            return true;
        }
         return false;
    }

    public function isAdmin() 
    {
        if ($this->role == 'admin') {
            return true;
        }
         return false;
    }
    
    public function isClient() 
    {
        if ($this->role == 'client') {
            return true;
        }
         return false;
    }

    public function isActive() 
    {
        if ($this->account_status == 'active') {
            return true;
        }
         return false;
    }


    
    public function hasCompany() 
    {
        if ($this->company == null) {
            return false;
        } else {
            return true;
        }
    }
	
	public function fullName()
	{
		return $this->first_name . " " . $this->last_name;
	}
    
}
