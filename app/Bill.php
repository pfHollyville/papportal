<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    use \App\Traits\Userstamps;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start_date','end_date','status', 'total', 'company_id','notes'
    ];
    

    /**
     * Get the company for this bill.
     */
    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    /**
     * Get the line items for this Bill.
     */
    public function lineitems()
    {
        return $this->hasMany('App\Lineitem');
    }


    /**
     * Get the line items for this Bill.
     */
    public function xerolineitems()
    {
        return $this->hasMany('App\Xerolineitem');
    }
    
    
    public function createdby() 
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updatedby() 
    {
        return $this->belongsTo('App\User', 'updated_by');
    }
    
    public function getRules() 
    {
        return $this->rules;
    }

    public function getMessages() 
    {
        return $this->messages;
    }

	public function getInvoiceNumber()
	{
		return "HP" . str_pad($this->id,6,"0",STR_PAD_LEFT);
	}
    
    
}
