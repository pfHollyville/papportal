<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    use \App\Traits\Userstamps;

    private $rules = [
    'date' => 'required|date',
    'queuename' => 'required|max:180',
    'queueid' => 'required|max:180',
    'phonenumbers' => 'max:180',
    'numberpresentation' => 'max:180',
    'type' => 'required|in:Inbound,Outbound,Broadcast'
    ];
                    
    private    $messages = [
    ];

    protected $fillable = [
        'queuename','queueid','company_id', 'type','notes', 'date', 'phonenumbers','numberpresentation'
    ];

    /**
     * Get the company for this bill.
     */
    public function company()
    {
        return $this->belongsTo('App\Company');
    }

	
    public function createdby() 
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updatedby() 
    {
        return $this->belongsTo('App\User', 'updated_by');
    }
    
    public function getRules() 
    {
        return $this->rules;
    }

    public function getMessages() 
    {
        return $this->messages;
    }
	
	
}
