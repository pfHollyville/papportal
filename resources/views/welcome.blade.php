@extends('layouts.app')

@section('title','Handlr Home Page')

@section('content')
	<div class="container">
  <div class="jumbotron">
    <h1>Handlr</h1> 
    <p>Welcome to Handlr.</p> 
  </div>
  <p>Go to homepage <a href="http://www.handlr.co.uk">www.handlr.co.uk</a> for now</p> 
</div>

@endsection
