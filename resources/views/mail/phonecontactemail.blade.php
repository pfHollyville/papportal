<div>
<h3 style="font:13px arial,sans-serif;font-weight:bold">Hi {{ $client->first_name }},</h3>
<h3 style="font:13px arial,sans-serif;font-weight:bold">Handlr&nbsp;took&nbsp;a&nbsp;call&nbsp;on&nbsp;behalf&nbsp;of&nbsp;your&nbsp;business</h3>
<table style="font:12px arial,sans-serif">
	<tbody>
	@if($processdate)
		<tr>
			<td valign="top"><font style="color:#666">Date Time</font></td>
			<td>{{ $processdate }}</td>
		</tr>
	@endif
	@if($enquirytype)
		<tr>
			<td valign="top"><font style="color:#666">Enquiry Type</font></td>
			<td>{{ $enquirytype }}</td>
		</tr>
	@endif
	</tbody>
</table>

<br/>

<h3 style="font:13px arial,sans-serif;font-weight:bold">CONTACT DETAILS</h3>


<table style="font:12px arial,sans-serif">
	<tbody>
		@foreach($data as $key => $value)

			@if(in_array($key, $contactfields))
				<tr>
					<td valign="top"><font style="color:#666">
					@if(array_key_exists($key, $labels))
						{{ ucfirst($labels[$key]) }}
					@else
						{{ucfirst($key)}}
					@endif
					</font></td>
					<td>{{ $value}}</td>
				</tr>
			@endif
		@endforeach
	</tbody>
</table>

<h3 style="font:13px arial,sans-serif;font-weight:bold">OTHER INFORMATION</h3>

<table style="font:12px arial,sans-serif">
	<tbody>
		@foreach($data as $key => $value)
			@if (!in_array($key, $contactfields))
				<tr>
					<td valign="top"><font style="color:#666">
					@if(array_key_exists($key, $labels))
						{{ ucfirst($labels[$key]) }}
					@else
						{{ucfirst($key)}}
					@endif
					</font></td>
					<td>{{ $value}}</td>
				</tr>
			@endif
		@endforeach
		@if ($comments)
		<tr>
			<td valign="top"><font style="color:#666">Comments</font></td>
			<td>{{ $comments}}</td>
		</tr>
		@endif
	</tbody>
</table>

<br>
@if ($agent)
<div>Your call Handlr today was {{$agent}}.</div>
@endif
<br>
<div>Email generated by <a href="https://www.handlr.co.uk">Handlr</a></div>
<div>Call Handlr Ref: {{$phonecontact->phonecontactid}}</div>
</div>