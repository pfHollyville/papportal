<html>


 @yield('content')

<body>
    <div><br><br><span style="font-family: Archer, Arial, Sans-serif;font-size: 16px;">Best Wishes,</span></div><br><br>
    
    <div style="padding-left: 10px;position: relative">
        <img src=https://handlr.co.uk/includes/assets/esig/logo.png alt="Handlr Logo">
        <p><span style="font-weight: bold;font-family: 'GT Walsheim Pro Trial', Arial, Sans-serif;font-size: 16px;">Handlr</span><br><span style="font-family: Archer, Arial, Sans-serif;font-size: 14px;">Customer Services</span></p>
        
        <img src="https://handlr.co.uk/includes/assets/esig/phone.png"><span><a href="tel:+443332122308" style="text-decoration: none;font-family: Archer, Arial, Sans-serif;margin-left: 13px;font-size: 14px;">0333 212 2308</a></span><br>
        
        <img src="https://handlr.co.uk/includes/assets/esig/email.png"><span><a href="mailto:client@handlr.co.uk" style="text-decoration: none;font-family: Archer, Arial, Sans-serif;margin-left: 10px;font-size: 14px;">client@handlr.co.uk</a></span><br>
        
        <img src="https://handlr.co.uk/includes/assets/esig/tw.png"><span><a href="https://handlr.co.uk" style="text-decoration: none;font-family: Archer, Arial, Sans-serif;margin-left: 10px;font-size: 14px;">www.handlr.co.uk</a></span>
        <br><br>
    </div>
    
    <div style="position: static; padding-left: 30px">
        <span style="font-family: Archer, Arial, Sans-serif;font-size: 10px;position: relative">Handlr Ltd.<br>Queens Court, 9-17 Eastern Road<br>Romford, RM1 3NH</span>
        <img src="https://handlr.co.uk/includes/assets/esig/incoming.png" style="position: absolute;left: 0px;top: 235px;z-index: -1" alt="Handlr incoming">
    </div>
    
    <div style="width: 80%; text-align: center">
        <span style="position: relative;padding-left: 10px;top: 100px;font-family: Archer, Arial, Sans-serif;font-size: 10px;">Handlr is a registered trademark of Handlr Ltd. registered in England 10603748 at Queens Court, 9-17 Eastern Road, Romford, RM1 3NH and registered with the ICO ZA252525. Handlr ask that you handle all information exchanged with our organisation in a way which is compliant with GDPR and respectful of the environment.</span>
    </div>
</body>
</html>