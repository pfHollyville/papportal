@extends('layouts.app')

@section('title','Company Management')

@section('content')
 <div class="col-md-12">
<div class="row">
	<div class="col-sm-8">
	 {{ $companies->links() }}
	</div>
	<div class="col-sm-4 pull-right">
		<div class="btn-group">
			  <a title="Add New Company" type="button" class="btn btn-outline-primary" href="{{route('company.create')}}">
				<span class="icon icon-add-to-list"></span> New Company
			  </a>
		</div>
	</div>
</div>
<div class="row">
	<div class="table-responsive">
	<table id="companyTable" class="table" data-sort="table">
		<thead>
			<tr>
				<th class="header">Client Name</th>
				<th class="header">Company Name</th>
				<th class="header">Industry</th>
				<th class="header">Legal Status</th>
				<th class="header">Usage Reason</th>
				<th class="header">Xero ID</th>
			</tr>
		</thead>
	<tbody>
	@foreach($companies as $company)
	<tr>
		<td><a href="{{route('admin.users.show',['id'=>$company['user']['id']])}}">{{$company['user']['first_name']}}&nbsp;{{$company['user']['last_name']}}</a></td>
		<td><a href="{{route('admin.companies.show',['id'=>$company['id']])}}">{{$company['companyname']}}</a></td>
		<td>{{$company['industry']['name']}}</td>
		<td>{{$company['legalstatus']['name']}}</td>
		<td>{{$company['usagereason']['name']}}</td>
		<td>{{$company['xero_id']}}</td>
	</tr>
	@endforeach
	</tbody>
	</table>
	</div>
</div>
<div class="row">
	<div class="offset-col-8 col-sm-4">
	{{ $companies->links() }}
	</div>
</div>
</div>
@endsection

@section('addscripts')
<script>
	$(document).ready(function() 
		{ 
			$("#companyTable").tablesorter(); 
		} 
	); 

</script>
@endsection
