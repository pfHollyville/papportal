        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }} 
							@if(env('APP_ENV') != "production")
								<small>Env: <span>{{ env('APP_ENV')}}</span></small>
							@endif
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        @if (Auth::guest())
						@else
							@if(Auth::user()->isClient())
                            <li><a href="{{route('company.index')}}">Company</a></li>
                            <li><a href="{{route('services.index')}}">Services</a></li>
                            <!--<li><a href="#">Webchat</a></li> !-->
                            <li><a href="{{route('questions.index')}}">Questions</a></li>
                            <!--<li><a href="#">Billing</a></li> !-->
							@endif
						@endif
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li><p class="navbar-text">Welcome&nbsp;{{ Auth::user()->first_name }}</p></li>
							<li>
							<a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
