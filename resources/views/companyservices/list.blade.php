@extends('layouts.app')

@section('title','Company Services Management')

@section('content')
<div id="companyservices" class="container">
	<div class="row">
		<div class="col-sm-8">
		<h3>My Services <span  v-cloak class="badge badge-default">
			@{{ companyservices.length}}</span>
		</h3>
		</div>
	</div>
	<div class="row">
		<div class="well well-faq" v-cloak>
			<div  v-if="!newcompanyservice.edit">
				<button type="button" class="btn btn-sm btn-primary" v-on:click="addCompanyservice(newcompanyservice)"><span class="glyphicon glyphicon-plus"></span> New</button>
			</div>
			
			<div v-if="newcompanyservice.edit">
				<form class="form" @submit.prevent="savenewCompanyservice(newcompanyservice)">
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.service}]">
						<label for="service">Service:</label>
						<select v-model="newcompanyservice.service_id" class="form-control">
							<option v-for="service in services" v-bind:value="service.id">
								@{{ service.name }}
							</option>
						</select>
						<span v-if="formerrors && formerrors.service" class="help-block">
							@{{ formerrors.service[0] }}
						</span>
					</div>
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.start_date}]">
						<label for="start_date">Start Date:</label>
						<datepicker v-model="newcompanyservice.start_date" ></datepicker>
						<span v-if="formerrors && formerrors.start-date" class="help-block">
							@{{ formerrors.start_date[0] }}
						</span>
					</div>
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.status}]">
						<label for="status">Status:</label>
						<p class="form-control-static">@{{newcompanyservice.status}}</p>
						<span v-if="formerrors && formerrors.start-date" class="help-block">
							@{{ formerrors.start_date[0] }}
						</span>
					</div>
					<button type="submit" class="btn btn-sm  btn-primary" ><span class="glyphicon glyphicon-save"></span> Save</button>
					<button v-on:click.stop.prevent="clearCompanyservice(newcompanyservice)" class="btn btn-sm"><span class="glyphicon glyphicon-erase"></span> Clear</button>
					<div v-if="formerrors"  class="alert alert-warning" role="alert">
						<strong>Warning!</strong> Something is not quite right here.
					</div>
				</form>
			</div>
		</div>	
	</div>
	<div class="row">
		<template v-for="companyservice in companyservices">
		<div class="well col-sm-12" >
			<div v-if="companyservice.success"  class="alert alert-success" role="alert">
				<strong>Success!</strong> that went well.
			</div>
			<div  v-if="!companyservice.edit">
				<h4 class="col-sm-2">@{{ companyservice.service.name }}</h4>
				<div class="col-sm-2"><span class="badge badge-info">@{{ companyservice.status }}</span></div>
				<div class="col-sm-2">Max Usage: @{{ companyservice.service.maxusage }}</div>
				<div class="col-sm-2">Min Commitment: @{{ companyservice.service.mincommitment }}</div>
				<div class="col-sm-2">Monthly: &pound;@{{ companyservice.service.basecost }}</div>
				<div class="col-sm-2">Units: &pound;@{{ companyservice.service.extracost }}</div>
				<div class="col-sm-8"><small>@{{ companyservice.service.description }}</small></div>
				<div class="col-sm-2">Start: @{{ companyservice.start_date }}</div>
				<div class="col-sm-2">End: @{{ companyservice.end_date }}</div>
			</div>
		</div>
		</template>
	</div>
</div>		




@endsection
@section('addscripts')
<script src="{{ asset('js/vue.js') }}"></script>



<script>

	var errors;
    var vm = new Vue({
          el: '#companyservices',
          data: {
			newcompanyservice: {
				id: 0,
				service_id: 0,
				start_date: Moment().format('YYYY-MM-DD'),
				status: 'Pending',
				edit: false,
				service: null
			},  
			companyservices: {!! $companyservices !!},
			services: {!! $services !!},
			formerrors: null,
          },
		  methods: {
			clearCompanyservice: function(cs) {
				cs.success = false;
				cs.edit = false;
			  },
			  addCompanyservice: function(newcompanyservice) {
				newcompanyservice.edit = true;
				newcompanyservice.id = 0;
				newcompanyservice.service_id = 0;
				formerrors = null;
			  },
			  saveCompanyservice: function(companyservice) {
				axios.put('{{ url("/services")}}/'+companyservice.id,companyservice)
					.then(function (response) {
						companyservice.success = true;
						companyservice.edit = false;
					})
					.catch(function (error)	{
						//this.formerrors = error.response.data;
						
					});
			  },
			  savenewCompanyservice: function(newcs) {
				this.formerrors = null;
				newcs.start_date = Moment(newcs.start_date).format('YYYY-MM-DD');
				var _this = this;
				axios.post('{{ url("/services")}}',newcs)
					.then(function (response) {
						newcs.id = response.data;
						newcs.edit = false;
						for(var i=0; i< _this.services.length; i++) {
							if (_this.services[i].id == newcs.service_id) {
								newcs.service = _this.services[i];
							}
						}
						_this.companyservices.push(Vue.util.extend({},newcs));
						toastr.success("Success","You new service has been requested");
						newcs.service = null;
						newcs.start_date = null;
						newcs.id = 0;
						service_id: 0;
						
					})
					.catch(function (error)	{
						this.formerrors = error.response.data;
						errors = error.response.data;
						for(var error in errors) { 
							for(i=0;i<errors[error].length;i++) {
								toastr.error(errors[error][i]);
							}
						}
					});
			  }
		  },
		  components: {
				'datepicker': Datepicker
			},

	});
	

 
</script>
@endsection
