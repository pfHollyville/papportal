@extends('layouts.app')

@section('content')
<div class="container">
<h1>Handlr Ltd. Terms &amp; Conditions</h1>
<ol>
	<li><h2>INTERPRETATION</h2>
		<p>The definitions and rules of interpretation in this condition apply in these terms and conditions.</p>
		<ol>
			<li><h3>Definitions:</h3>
				<div>
					<p><strong>Contract:</strong>The Contract between Handlr and the Customer to which these Conditions apply;</p>
					<p><strong>Customer:</strong>the person, firm or company who purchases Services from Handlr.</p>
					<p><strong>Intellectual Property Rights: </strong>patents, rights to inventions, copyright and related rights, trade marks, trade names, domain names, rights in get-up, rights in goodwill or to sue for passing off, unfair competition rights, rights in designs, rights in computer software, database rights, topography rights, moral rights, rights in confidential information (including without limitation know-how and trade secrets) and any other intellectual property rights, in each case whether registered or unregistered, and including without limitation all applications for, and renewals or extensions of, such rights, and all similar or equivalent rights or forms of protection in any part of the world.</p>
					<p><strong>Services:</strong>the services to be provided by Handlr under the Contract, set out at Schedule 1. </p>
					<p><strong>Performance Targets:</strong>The targets set out at Schedule 2. </p>
					<p><strong>Handlr:</strong>Handlr Ltd., company number 10603748, whose registered office is at Queens Court, 9 – 17 Eastern Road, Romford, Essex RM1 3NH</p>
					<p><strong>VAT:</strong>value added tax chargeable under English law for the time being and any similar additional tax.</p>
				</div>
			</li>
			<li>Condition, Schedule and paragraph headings shall not affect the interpretation of this agreement.</li>
			<li>A person includes a natural person, corporate or unincorporated body (whether or not having separate legal personality).</li>
			<li>A reference to a company shall include any company, corporation or other body corporate, wherever and however incorporated or established.</li>
			<li>Unless the context otherwise requires, words in the singular shall include the plural and in the plural include the singular.</li>
			<li>Unless the context otherwise requires, a reference to one gender shall include a reference to the other genders.</li>
			<li>A reference to a statute or statutory provision is a reference to it as amended, extended or re-enacted from time to time.</li>
			<li>A reference to writing or written includes email.</li>
			<li>References to conditions and Schedules are to the conditions and Schedules of this agreement and references to paragraphs are to paragraphs of the relevant Schedule.</li>
		</ol>
	<li>
		<h2>APPLICATION OF CONDITIONS</h2>
		<ol>
			<li>These conditions shall:
				<ol>
					<li>apply to and be incorporated in the Contract; and</li> 
					<li>prevail over any inconsistent terms or conditions contained in, or referred to in, the Customer’s purchase order, confirmation of order, or specification, or implied by law, trade custom, practice or course of dealing.</li>
				</ol>
			</li>
			<li>No addition to, variation of, exclusion or attempted exclusion of any term of the Contract shall be binding on Handlr unless in writing and signed by a duly authorised representative of Handlr.</li>
			<li>The commencement of the provision of Services by Handlr (including during any agreed trial period) constitutes an offer by Handlr to supply the Services in accordance with these Conditions and an agreement by the Customer to use the Services constitutes acceptance of these Conditions. The Customer’s standard terms and conditions (if any) shall not in any circumstances govern the Contract. </li>
		</ol>
	</li>

	<li><h2>HANDLR’S OBLIGATIONS</h2> 
		<ol>
			<li>Handlr shall provide such of the Services as requested by the Customer. </li>
			<li>Handlr shall use reasonable endeavours to meet the Performance Targets, but any such Targets are estimates only and shall not be of the essence of the Contract. </li>
		</ol>
	</li>
	<li><h2>CUSTOMER’S OBLIGATIONS</h2>
		<ol>
			<li>The Customer shall:
				<ol>
					<li>Provide in a timely manner all information (including scripts and FAQs) required by Handlr in order to provide the Services; </li>
					<li>provide in a timely manner such access to the Customer’s data is requested by Handlr;</li>
					<li>provide in a timely manner such information as Handlr may request, and ensure that such information is accurate in all material respects; </li>
					<li>where Live Web Chats are provided by Handlr, the Customer is obliged to add the appropriate HTML code to the Customer website. </li>
				</ol>
			</li>
			<li>If Handlr’s performance of its obligations under the Contract is prevented or delayed by any act or omission of the Customer or the Customer’s agents, sub-contractors or employees, the Customer shall in all circumstances be liable to pay to Handlr on demand all reasonable costs, charges or losses sustained or incurred by it (including, without limitation, any direct, indirect or consequential losses, loss of profit and loss of reputation, loss or damage to property, injury to or death of any person and loss of opportunity to deploy resources elsewhere), subject to Handlr confirming such costs, charges and losses to the Customer in writing.</li>
			<li>If Handlr’s performance of its obligations under the Contract is prevented or delayed by any act or omission of the Customer or the Customer’s agents, sub-contractors or employees, or of any third party, Handlr shall not be liable for any consequential inability to provide the Services and no refund shall be payable to the Customer for any period when the Services cannot be provided. </li>
			<li>The Customer shall not, without the prior written consent of Handlr, at any time from the date of the Contract to the expiry of six months after the termination of the Contact, solicit or entice away from Handlr or employ or attempt to employ any person who is, or has been, engaged as an employee or sub-contractor of Handlr. </li>
		</ol>
	</li>
	<li><h2>CHARGES AND PAYMENT</h2>
		<ol>
			<li>Unless otherwise agreed in writing by Handlr, all sums due to Handlr under the Contract shall be payable monthly in advance by Direct Debit. Payment times are strictly 7 days from the date of invoice. Handlr will not incur any costs or commence any work until the first Direct Debit payment has been received. </li>
			<li>In the event that Handlr agrees to accept payment by means other than Direct Debit a monthly administration fee of £10 will apply. </li>
			<li>At all times when Handlr is registered for VAT, all charges under the Contract shall be subject to VAT at the prevailing rate.</li>
			<li>Handlr may review and increase the charges for the Services and will give the Customer 30 days notice in writing of any increase.</li>
			<li>Without prejudice to any other right or remedy that Handlr may have, if the Customer fails make any monthly payment in advance by Direct Debit, Handlr may:
				<ol>
					<li>charge a late payment fee of £25 each calendar month that the payment remains outstanding, and</li>
					<li>suspend all Services until payment has been made in full.</li>
				</ol>
			</li>
			<li>Time for payment shall be of the essence of the Contract.</li>
			<li>All payments payable to Handlr under the Contract shall become due immediately on termination of the Contract, despite any other provision. This condition is without prejudice to any right to claim for interest under the law, or any such right under the Contract.</li>
			<li>All amounts due under this agreement shall be paid by the Customer to Handlr in full without any set-off, counterclaim, deduction or withholding (other than any deduction or withholding of tax as required by law). Handlr may, without prejudice to any other rights it may have, set off any liability of the Customer to Handlr against any liability of Handlr to the Customer.</li>
			<li>All payments made to the Company must be made in British pound sterling (GBP). The Customer is responsible for ensuring the full pound sterling amount is paid in full when making any form of international payment from any currency other than pound sterling (GBP). The Customer will pay any and all charges and deductions that the Customers financial institution imposes; or any fees or deductions charged to the Company for any international payments made to the Company by the Customer.</li>
			<li>If the Customer believes they are entitled a refund from the Company, the Customer must make a formal request to the Company in writing for the refund and a full reason why a refund is being requested. Refund requests from the Company must never exceed the total amount paid by the Customer for the period and Services the refund request is for. Refunds will be paid at the discretion of the Company.</li>
			<li>Where a refund request is agreed and granted by the Company, this will be payable by a method as decided by the Company and not by the Customer. A refund is only payable to the Customer and not an agent or third party to the Customer. The Company reserves the right to create a refund schedule and pay such refunds over a period of time not exceeding 30 days if the total value is less than £1,000, or 6 months if the total value is less than £10,000, or 12 months if the total value exceeds £10,000. If the Customer refuses to accept a refund or returns payments made by the Company, the Company will void the refund agreement and the Customer will forfeit any refund due.</li>
			<li>Any refunds made will be payable in British pound sterling (GBP) only. The Customer is responsible for ensuring they have a bank account capable of accepting pound sterling (GBP) payments into the account via cheque or BACS payment. The Company will deduct any and all fees associated with the transfer of payments from the Company account to the Customer account if the fee, or total fees for the entire refund schedule are above £5.00 (GBP) in value.</li>
		</ol>
	</li>
	<li><h2>DATA AND CALL MONITORING</h2>
		<ol>
			<li>Handlr is registered with the Information Commissioner’s Office under reference ZA252525. Without prejudice to any other rights or remedies which the parties may have, subject to clause 6.6 information about individual clients and their employees is kept strictly confidential in accordance with the Data Protection Act 1998 and (at such time as the same comes in to force) the General Data Protection Regulation.</li>
			<li>Any information disclosed by and/or relating to (i) the Customer and its directors, employees, contractors and consultants, and (ii) any person or organisation from whom Handlr receives a telephone call or engages in a web chat with or on behalf of the Customer (a “caller”), will be treated as strictly confidential and not disclosed to any person, expect to such of the Customer’s directors, employees, contractors and consultants as the Customer may notify to Handlr from time to time:</li>
			<li>“Information” includes (without limitation) (i) the name, company, firm or organisation, telephone number and other personal and contact details of the caller, (ii) the nature and content of the call, facsimile or e-mail (including any attachments to the facsimile or e-mail , and any messages or voicemails left by the caller), and (iii) the existence of the call, facsimile or e-mail; whether disclosed, recorded or stored verbally, in writing, electronically, or by any other means;</li>
			<li>Handlr will process all “personal data” (as defined in the Data Protection Act 1998) relating to the Customer, the Customer’s directors, employees, contractors and consultants, and callers strictly in accordance with the Data Protection Act 1998. For the purposes of this agreement, “process” will include (without limitation) the collection, recording, storage and disposal of personal data; </li>
			<li>The Customer is responsible for ensuring their own compliance with the Data Protection Act 1998 (or any legislation that supersedes it) and will not pass any information obtained for them by Handlr outside of the European Union, nor to any third party. The Customer will not permit any third party to access data held on Handlr’s website or database and will ensure that their own electronic devices are secure by implementing all necessary data protection measures. </li>
			<li>Handlr will not, unless the Customer can demonstrate a genuine business requirement, collect any data that is considered Sensitive Personal Data for the purposes of the Data Protection Act 1998. Customers requiring the collection of Sensitive Personal Data must make a request in writing. Handlr may in its absolute discretion accept or refuse the request and will confirm in writing any limitations upon the Data to be collected. </li>
			<li>All calls will be recorded and retained for a minimum of 6 months. Recordings remain the property of Handlr and will not be provided to the Customer in any circumstance. </li>
			
		</ol>
	</li>
	<li><h2>LIMITATION OF LIABILITY </h2>
		<ol>
			<li>The following provisions set out the entire financial liability of Handlr (including without limitation any liability for the acts or omissions of its employees, agents and sub-contractors) to the Customer in respect of:
				<ol>
					<li>any breach of the Contract howsoever arising;</li>
					<li>any use made by the Customer of the Services, or any part of them; and</li>
					<li>any representation, misrepresentation (whether innocent or negligent), statement or tortious act or omission (including without limitation negligence) arising under or in connection with the Contract.</li>
				</ol>
			</li>
			<li>All warranties, conditions and other terms implied by statute or common law are, to the fullest extent permitted by law, excluded from the Contract.</li>
			<li>Nothing in these conditions excludes the liability of Handlr:
				<ol>
					<li>for death or personal injury caused by the Handlr’s negligence; or</li>
					<li>for fraud or fraudulent misrepresentation.</li>
				</ol>
			</li>	
			<li>Subject to Condition 7.3:
				<ol>
					<li>Handlr shall not in any circumstances be liable, whether in tort (including without limitation for negligence or breach of statutory duty howsoever arising), contract, misrepresentation (whether innocent or negligent) or otherwise for:
						<ol>
							<li>loss of profits; or</li>
							<li>loss of business; or</li>
							<li>depletion of goodwill or similar losses; or</li>
							<li>loss of anticipated savings; or</li>
							<li>loss of contract; or</li>
							<li>loss of use; or</li>
							<li>loss or corruption of data or information; or</li>
							<li>any special, indirect, consequential or pure economic loss, costs, damages, charges or expenses.</li>
						</ol>
					</li>
					<li>Handlr’s total liability in contract, tort (including without limitation negligence or breach of statutory duty howsoever arising), misrepresentation (whether innocent or negligent), restitution or otherwise, arising in connection with the performance or contemplated performance of the Contract shall be limited to the price paid for the Services.</li>
				</ol>
			</li>
		</ol>
	</li>
	<li><h2>TERMINATION</h2>
		<ol>
			<li>Unless clause 8.2 applies, either party may terminate the Contract by giving no less than 30 days’ written notice to the other party, however notice given by the Customer within the first three calendar months of the Contract will be deemed to expire no sooner than the end of the third month.</li>
			<li>Where a fixed 12 month contract has been entered into, either party may terminate the Contract by giving no less than 30 days’ written notice to the other party, however notice given by the Customer within the first 12 months of the Contract will be deemed to expire no sooner than the end of the 12 month.</li>
			<li>Without prejudice to any other rights or remedies to which Handlr may be entitled, Handlr may terminate the Contract immediately without liability to the Customer if:
				<ol>
					<li>the Customer fails to pay any amount due under this agreement on the due date for payment and remains in default not less than 14 days after being notified in writing to make such payment;</li>
					<li>the Customer commits a material breach of any other term of this agreement which breach is irremediable or (if such breach is remediable) fails to remedy that breach within a period of 14 days after being notified in writing to do so;</li>
					<li>the Customer suspends, or threatens to suspend, payment of its debts or is unable to pay its debts as they fall due or admits inability to pay its debts or is deemed unable to pay its debts within the meaning of section 123 of the Insolvency Act 1986;</li>
					<li>the Customer commences negotiations with all or any class of its creditors with a view to rescheduling any of its debts, or makes a proposal for or enters into any compromise or arrangement with its creditors other than for the sole purpose of a scheme for a solvent amalgamation of the Customer with one or more other companies or the solvent reconstruction of the Customer;</li>
					<li>a petition is filed, a notice is given, a resolution is passed, or an order is made, for or in connection with the winding up of the Customer other than for the sole purpose of a scheme for a solvent amalgamation of the Customer with one or more other companies or the solvent reconstruction of the Customer;</li>
					<li>an application is made to court, or an order is made, for the appointment of an administrator, or if a notice of intention to appoint an administrator is given or if an administrator is appointed, over the Customer;</li>
					<li>the holder of a qualifying floating charge over the assets of the Customer has become entitled to appoint or has appointed an administrative receiver;</li>
					<li>a person becomes entitled to appoint a receiver over the assets of the Customer or a receiver is appointed over the assets of the Customer;</li>
					<li>a creditor or encumbrancer of the Customer attaches or takes possession of, or a distress, execution, sequestration or other such process is levied or enforced on or sued against, the whole or any part of the Customer’s assets and such attachment or process is not discharged within 14 days;</li>
					<li>any event occurs, or proceeding is taken, with respect to the Customer in any jurisdiction to which it is subject that has an effect equivalent or similar to any of the events mentioned in Condition 11.1(d) to Condition 11.1(j) (inclusive);</li>
					<li>there is a change of control of the Customer (within the meaning of section 1124 of the Corporation Tax Act 2010).</li>
				</ol>
			</li>
			<li>Any provision of this agreement that expressly or by implication is intended to come into or continue in force on or after termination or expiry of this agreement shall remain in full force and effect.</li>
			<li>Termination of this agreement shall not affect any rights, remedies, obligations or liabilities of the parties that have accrued up to the date of termination, including the right to claim damages in respect of any breach of the agreement which existed at or before the date of termination.</li>
		</ol>
	</li>
	<li><h2>FORCE MAJEURE</h2>
		<ol>
			<li>Handlr shall not in any circumstances have any liability to the Customer under the Contract if it is prevented from, or delayed in, performing its obligations under the Contract or from carrying on its business by acts, events, omissions or accidents beyond its reasonable control, including, without limitation, strikes, lock-outs or other industrial disputes (whether involving the workforce of Handlr or any other party), failure of a utility service, network, server or transport network, act of God, war, riot, civil commotion, malicious damage, compliance with any law or governmental order, rule, regulation or direction, accident, breakdown of plant or machinery, fire, flood, storm or default of suppliers or sub-contractors.</li>
		</ol>
	</li>
	<li><h2>WAIVER </h2>
		<ol>
			<li>No failure or delay by a party to exercise any right or remedy provided under this agreement or by law shall constitute a waiver of that or any other right or remedy, nor shall it prevent or restrict the further exercise of that or any other right or remedy. No single or partial exercise of such right or remedy shall prevent or restrict the further exercise of that or any other right or remedy.</li>
		</ol>
	</li>
	<li><h2>RIGHTS AND REMEDIES</h2>
		<ol>
			<li>The rights and remedies provided under this agreement are in addition to, and not exclusive of, any rights or remedies provided by law.</li>
		</ol>
	</li>
	<li><h2>SEVERANCE</h2>
		<ol>
			<li>If any provision or part-provision of this agreement is or becomes invalid, illegal or unenforceable, it shall be deemed modified to the minimum extent necessary to make it valid, legal and enforceable. If such modification is not possible, the relevant provision or part-provision shall be deemed deleted. Any modification to or deletion of a provision or part-provision under this condition shall not affect the validity and enforceability of the rest of this agreement.</li>
			<li>If any provision or part-provision of this agreement is invalid, illegal or unenforceable, the parties shall negotiate in good faith to amend such provision so that, as amended, it is legal, valid and enforceable, and, to the greatest extent possible, achieves the intended commercial result of the original provision.</li>
		</ol>
	</li>
	<li><h2>ENTIRE AGREEMENT</h2>
		<ol>
			<li>This agreement constitutes the entire agreement between the parties and supersedes and extinguishes all previous agreements, promises, assurances, warranties, representations and understandings between them, whether written or oral, relating to its subject matter.</li>
			<li>Each party acknowledges that in entering into this agreement it does not rely on , and shall have no remedies in respect of, any statement, representation, assurance or warranty (whether made innocently or negligently) that is not set out in this agreement.</li>
			<li>Each party agrees that it shall have no claim for innocent or negligent misrepresentation or negligent misstatement based on any statement in this agreement.</li>
		</ol>
	</li>
	<li><h2>ASSIGNMENT</h2>
		<ol>
			<li>The Customer shall not, without the prior written consent of Handlr, assign, transfer, charge, sub-contract or deal in any other manner with all or any of its rights or obligations under the Contract.</li>
			<li>Handlr may at any time assign, transfer, charge, sub-contract or deal in any other manner with all or any of its rights or obligations under the Contract.</li>
		</ol>
	</li>
	<li><h2>NO PARTNERSHIP OR AGENCY</h2>
		<ol>
			<li>Nothing in the Contract is intended to or shall operate to create a partnership between the parties, or to authorise either party to act as agent for the other, and neither party shall have authority to act in the name or on behalf of or otherwise to bind the other in any way (including without limitation the making of any representation or warranty, the assumption of any obligation or liability and the exercise of any right or power).</li>
		</ol>
	</li>
	<li><h2>THIRD PARTY RIGHTS</h2>
		<ol>
			<li>No one other than a party to this agreement, their successors and permitted assignees, shall have any right to enforce any of its terms.</li>
		</ol>
	</li>
	<li><h2>NOTICES</h2>
		<ol>
			<li>Any notice or other communication given to a party under or in connection with this contract shall be in writing and shall be  delivered by hand or by pre-paid first-class post or other next working day delivery service at its registered office;</li>
			<li>Any notice or communication shall be deemed to have been received:
				<ol>
					<li>if delivered by hand, on signature of a delivery receipt or at the time the notice is left at the proper address;</li>
					<li>if sent by pre-paid first-class post or other next working day delivery service, at 9.00 am on the second Business Day after posting  or at the time recorded by the delivery service;</li>
				</ol>
			</li>
			<li>This condition does not apply to the service of any proceedings or other documents in any legal action or, where applicable, any arbitration or other method of dispute resolution. For the purposes of this condition, “writing” shall not include email.</li>
		</ol>
	</li>
	<li><h2>GOVERNING LAW</h2>
		<ol>
			<li>The Contract and any disputes or claims arising out of or in connection with it or its subject matter or formation (including without limitation non-contractual disputes or claims) are governed by and construed in accordance with the law of England and Wales.</li>
		</ol>
	</li>
	<li><h2>JURISDICTION</h2>
		<ol>
			<li>Each party irrevocably agrees that the courts of England and Wales shall have exclusive jurisdiction to settle any dispute or claim arising out of or in connection with this agreement or its subject matter or formation (including non-contractual disputes or claims).</li>
		</ol>
	</li>
</ol>	
<br/>

<div>

<h2>SCHEDULE 1 – SERVICES</h2>

<p>This schedule sets out the services provided by Handlr, along with important information about those services. It is important that you read carefully the sections which apply to the Services that you have requested Handlr to provide.</p>
<p>Packages are as advertised on Handlr’s website as “Chat Handlr” (Web chats), “Call Handlr” (Phone answering services), “Call Handlr+” (Phone answering services) and “Virtual Handlr” (Virtual numbers). </p>
<h3>Hours of Business – all services</h3>
<p>Handlr’s hours of business are 08.30 – 18.00 Monday to Friday and 0900 to 1700 Saturday. Handlr reserves the right to change these hours of business.</p>
<h3>Web Chats</h3>
	<ol>
		<li>Live web chats will be staffed by Handlr employees, who will handle chats, providing answers to customer enquiries using the Frequently Asked Questions (FAQs) information provided by the Customer.</li>
		<li>It is the Customer’s responsibility to ensure that FAQ information is kept up to date and communicated to Handlr.</li>
		<li>Handlr does not provide an installation service and the Customer is responsible for installing the appropriate code to allow live chats to take place.</li>
		<li>Unless the Customer advises otherwise in writing, once installed Live Chats will proactively open every time the relevant web page is visited.</li>
		<li>A charge will be incurred whenever a comment is made by a visitor to the website in the Live Chat box, regardless of the nature of the comment. </li>
		<li>Where the Customer has paid for a specific number of chats per month, any chat in excess of that amount will be charged at the same price per chat pro-rata.</li>
		<li>Should a Customer not want to be charged for additional chats it is the Customer’s responsibility to remove the code from their web pages.</li>
		<li>No new chats will be initiated during the final 10 minutes of business hours each day. </li>
		<li>Handlr will not inform the Customer when their package limit has been reached. </li>
		<li>Unused chats remaining in a monthly package at month end will not be carried over into the following month. </li>
	</ol>
	
<h3>Phone answering Services</h3>
	<ol>
		<li>Where the package allows, Customers may provide a custom script. The Customer is responsible for the content of that script. No more than 15 custom fields (text only) will be accepted. Handlr may at its discretion place limits on the script content and additional set up charges will apply. </li>
		<li>If the Customer does not request a bespoke recorded greeting (additional charges will apply) then a generic recorded greeting may be added unless the Customer informs Handlr in writing.</li>
		<li>In some cases Handlr will be required by law to play a specific greeting. </li>
		<li>Where calls are placed in a queue, music will be played. No specific requirements as to music choice can be accommodated. </li>
		<li>The Customer is responsible for informing Handlr which data they wish to be captured when calls are answered.</li>
		<li>Where the package allows, and If provided with a suitable virtual terminal link, Handlr will take card payments and / or set up direct debits. No API integration will be permitted. No payment information will be stored. </li>
		<li>Outbound calls can be made at the request of the Customer. These will be billed at the same rate as incoming calls.</li>
		<li>If the Customer requests a dedicated voicemail to capture messages outside of business hours, this will be charged at the normal rate per minute plus £1.00 per message.</li>
		<li>Handlr may, without notifying the Customer, make changes to Customer scripts where necessary to improve call handling and efficiency.</li>
		<li>There are limits upon the information that Handlr will collect from callers. You are referred to clause 6 of the Conditions with regard to Data Protection. We cannot collect information that is not relevant to the purpose of the call. If at any point Handlr is unsure as to the purpose for which we have been requested to collect information, we may refuse to collect that information and ask you for written reasons why that information is necessary. </li>
		<li>If this service is being used to capture calls to the Customer that would otherwise be missed, the Customer is responsible for setting up the appropriate call diverts. Handlr will not be liable for any additional network charges for diverted calls.</li>
		<li>Pay As You Go mobile phones are unsuitable for diverting calls to Handlr. </li>
		<li>No new calls will be accepted into the queue during the final 10 minutes of business hours each day.</li>
		<li>Handlr will not inform the Customer when the limit of calls contained in their package has been reached. </li>
		<li>Unused calls or minutes remaining in a monthly package at month end will not be carried over into the following month. </li>
	</ol>

<h3>Virtual Numbers</h3>
	<ol>
		<li>For billing purposes, call time will be calculated from the point of call connection to Handlr’s network to the point the call is connected to the target number, including ringing time. </li>
		<li>Handlr will not be liable for any additional network charges for diverted calls.</li>
		<li>Without the prior written consent of Handlr, virtual numbers will not be transferred to another provider upon termination of the Contract. Where consent is requested, this will be at the sole discretion of Handlr and an administration fee or £35 for the first number and £20 for each additional number will be payable. </li>
		<li>Should a Customer terminate the Contract and then enter into a new contract with Handlr the same number will not be available.</li>
		<li>Should any Customer’s account be suspended due to non payment or other breach, calls to the virtual number will not be connected to the target number.</li>
		<li>The Customer is responsible for providing the correct number to re-direct calls from a virtual number. UK Landlines starting 01/02/0333 or mobile numbers starting 07 only are acceptable for these purposes.</li>
		<li>Calls cannot be directed to any number outside of the UK.</li>
		<li>Unused call transfers remaining in a monthly package at month end will not be carried over into the following month. </li>
	</ol>

<h3>General – all services</h3>
	<ol>
		<li>Handlr do not carry out market research. If it appears that the Customer is asking for data to be collected for market research purposes, Handlr will not collect that data.</li>
		<li>If the Customer has engaged Handlr to make out going calls for the purpose of sales where there is no prior permission from the recipient and / or the Customer has not done business with the recipient within the previous 12 months, Handlr will check the number against the TPS and CTPS databases and will not call any number appearing on either.</li>
		<li>Transcripts of calls or chats will be sent only to the nominate e-mail address provided by the Customer.</li>
		<li>Customers are not permitted to express a preference as to who will be answering their calls or web chats.</li>
		<li>Whilst Customers may up grade their package within the 3 month minimum term, no downgrading will be permitted during that period – downgrading is subject to the notice periods set out at clause 8 of the Conditions.</li>
		<li>Where services attract a monthly management fee, this is payable one month in advance. Itemised billing will appear on the following monthly account. Itemised bills will be rounded up to the nearest whole minute. </li>
	</ol>

</div>	

<div>

<h2>SCHEDULE 2 – TARGETS</h2>

<p>This Schedule sets out the targets that Handlr aim to match in providing the Services. Please note that these are targets and not guaranteed. Nor are they performance indicators. In the unlikely event that Handlr is unable to meet these targets you will not be entitled to any refund nor to terminate the contract immediately. Please refer to clauses 3.2, 7 and 8 of the Conditions.</p>

<h3>Phone Answering Services</h3>
	<ol>
		<li>Handlr will capture the caller’s name, number and a short message.</li>
		<li>Where the package and the Customer’s script allows, further information will be obtained.</li>
		<li>Calls will whenever possible be answered within 5 minutes of client queuing.</li>
		<li>Calls will whenever possible be directed to agents who are familiar with the Customer. Where this is not possible then another agent will take the call and will use the script for guidance.</li>
		<li>Where the Customer has requested a personalised recorded message, this can take 14 days to go live. </li>
		<li>Outbound calls will only be made at the specific request of the Customer. If the call does not connect or is terminated by the recipient no call back will be made unless specifically requested. </li>
	</ol>
	
<h3>Live Web Chat</h3>
	<ol>
		<li>Web chats will where possible be answered within 5 minutes.</li>
		<li>If all agents are busy, chat boxes will not appear to avoid queuing.</li>
		<li>Agents will be handling multiple chats and whilst they will try and avoid delays, some delay may be unavoidable.</li>
		<li>The agent will answer any FAQs and obtain contact details.</li>
	</ol>
	
<h3>Changes</h3>

<p>Where a Customer requests a change to their Services (Including but not limited to changing a re-direct target number or pausing a service) Handlr will endeavour to process the change within 2 working days. </p>

</div>

@endsection
