@extends('layouts.app')

@section('content')
<div class="container">
<h2>Description of processing</h2>
<p>The following is a broad description of the way this organisation/data controller processes personal information. To understand how your own personal information is processed you may need to refer to any personal communications you have received, check any privacy notices the organisation has provided or contact the organisation to ask about your personal circumstances.</p>
<h2>Reasons/purposes for processing information</h2>
<p>We process personal information to enable us to provide telecommunications services to and on behalf our customers; maintain our and our customers accounts and records containing their own customer data; promote and advertise our services and services of our customers; support and manage our employees and those of our customers. We also process personal information in the course of selling, hiring or exchanging it.</p>
<h2>Type/classes of information processed</h2>
<p>We process information relating to the above reasons/purposes. This information may include:</p>
<ul>
<li>Personal details</li>
<li>Good and services</li>
<li>Family Details</li>
<li>Lifestyle and Social</li>
<li>Circumstances</li>
<li>Financial Details</li>
</ul>
<h2>Education and employment details</h2>
<p>We also process sensitive classes of information that may include:</p>
<ul>
<li>Physical or mental health details</li>
<li>Racial or ethnic origin Religious or other beliefs Trade union membership</li>
</ul>
<h2>Who the information is processed about</h2>
<p>We process personal information about:</p>
<ul>
<li>Customers</li>
<li>Our customers own customers</li>
<li>Suppliers</li>
<li>Professional experts and consultants</li>
<li>Enquiriers</li>
<li>Employees</li>
</ul>
<h2>Who the information may be shared with</h2>
<p>We sometimes need to share the personal information we process with the individual themselves and also with other organisations. Where we obtain information on behalf of our customers, we share relevant information with our customers. Where this is necessary we are required to comply with all aspects of the Data Protection Act (DPA). What follows is a description of the types of organisations we may need to share some of the personal information we process with for one or more reasons.</p>
<p>Where necessary or required we share information with:</p>
<ul>
<li>Our customers</li>
<li>Business associates and professional advisers</li>
<li>family, associates and representatives of the person whose personal data we are processing</li>
<li>Current, past or prospective employers educators and examining bodies</li>
<li>Financial organisations</li>
<li>Employment and recruitment Agencies</li>
<li>Central government</li>
<li>Credit Reference agencies</li>
<li>Debt collection Agencies</li>
<li>Traders in personal data</li>
<li>Suppliers</li>
</ul>
<h2>Undertaking research</h2>
<p>Personal information is also processed in order to undertake research. For this reason the information processed may include name, contact details, family details, lifestyle and social circumstances, financial details, good and services. The sensitive types of information may include physical or mental health details, racial or ethnic origin and religious or other beliefs. This information is about survey respondents. Where necessary or required this information may be shared with customers and clients, agents, service providers, survey and research organisations.</p>
<h2>Consulting and advisory services</h2>
<p>Information is processed for consultancy and advisory services that are offered. For this reason the information processed may include name, contact details, family details, financial details, and the goods and services provided. This information may be about customers and clients. Where necessary this information is shared with the data subject themselves, business associates and other professional advisers, current, past or prospective employers and service providers.</p>
<h2>CCTV for crime prevention</h2>
<p>CCTV is used for maintaining the security of property and premises and for preventing and investigating crime, it may also be used to monitor staff when carrying out work duties. For these reasons the information processed may include visual images, personal appearance and behaviours. This information may be about staff, customers and clients, offenders and suspected offenders, members of the public and those inside, entering or in the immediate vicinity of the area under surveillance. Where necessary or required this information is shared with the data subjects themselves, employees and agents, services providers, police forces, security organisations and persons making an enquiry.</p>
<h2>Transfers</h2>
<p>It may sometimes be necessary to transfer personal information overseas. When this is needed information may be transferred to countries or territories around the world. Any transfers made will be in full compliance with all aspects of the data protection act.</p>

</div>
@endsection
