@extends('layouts.app')
@section('title','Question')

@section('content')

<div id="questions" class="container">
	<div class="row">
		<div class="col-sm-8">
		<h3>My Questions <span  v-cloak class="badge badge-default">
			@if(Route::current()->getName() == "questions.basic") Basic @endif
			@if(Route::current()->getName() == "questions.index") All @endif
			@if(Route::current()->getName() == "questions.faq") FAQ @endif
			@{{ questions.length}}</span>
		</h3>
		</div>
		<div class="col-sm-4">
			<span class="pull-right"> Filter: 
				<a href="{{ route('questions.faq')}}"><span class="glyphicon glyphicon-question-sign"></span> FAQ's</a>
				<a href="{{ route('questions.basic')}}"><span class="glyphicon glyphicon-info-sign"></span> Basic</a>
				<a href="{{ route('questions.index')}}"><span class="glyphicon glyphicon-ok-sign"></span> All</a>
			</span>
		</div>
	</div>
	<div class="row">
		<div class="well well-faq" v-cloak>
			<div  v-if="!newquestion.edit">
				<button type="button" class="btn btn-sm btn-primary" v-on:click="addQuestion()"><span class="glyphicon glyphicon-plus"></span> New</button>
			</div>
			
			<div v-if="newquestion.edit">
				<div><span class="glyphicon glyphicon-question-sign"></span> New FAQ</div>
				<form class="form" @submit.prevent="savenewQuestion()">
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.question}]">
						<label for="question">Question:</label>
						<textarea rows="2" v-model="newquestion.question" class="form-control"></textarea>
						<span v-if="formerrors && formerrors.question" class="help-block">
							@{{ formerrors.question[0] }}
						</span>
					</div>
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.answer}]">
						<label for="answer">Answer:</label>
						<textarea rows="2"  v-model="newquestion.answer" class="form-control"></textarea>
						<span v-if="formerrors && formerrors.answer" class="help-block">
							@{{ formerrors.answer[0] }}
						</span>
					</div>
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.category}]">
						<label for="category">Category:</label>
						<select v-model="newquestion.category" class="form-control">
							<option value="Business">Business</option>
							<option value="Web">Web</option>
							<option value="Phone">Phone</option>
						</select>
						<span v-if="formerrors && formerrors.category" class="help-block">
							@{{ formerrors.category[0] }}
						</span>
					</div>
					<button type="submit" class="btn btn-sm  btn-primary" ><span class="glyphicon glyphicon-save"></span> Save</button>
					<button v-on:click.stop.prevent="clearQuestion(newquestion)" class="btn btn-sm">
						<span class="glyphicon glyphicon-erase"></span> Clear
					</button>
				</form>
			</div>
		</div>	
	</div>
	<div class="row">
		<div v-for="(question,index) in questions" :key="question.id">
		<div class="well col-sm-12" v-bind:class="[question.basic ? 'well-basic' : 'well-faq' ]">
			<div  v-if="!question.edit">
				<h4 class="col-sm-10">
					<span v-if="question.basic"><span class="glyphicon glyphicon-info-sign"></span> Basic: </span>
					<span v-if="!question.basic"><span class="glyphicon glyphicon-question-sign"></span> FAQ: </span>
					@{{ question.question }}
				</h4>
				<div class="col-sm-2"><span class="badge badge-info">@{{ question.category }}</span></div>
				<div class="col-sm-10">@{{ question.answer }}</div>
				<div class="col-sm-2">
					<button v-if="!question.basic" type="button" class="btn btn-sm btn-danger" v-on:click.stop.prevent="deleteQuestion(index,question.id)"><span class="glyphicon glyphicon-erase"></span> Remove</button>
					<button type="button" class="btn btn-sm  btn-info" v-on:click="editQuestion(question)"><span class="glyphicon glyphicon-edit"></span> Edit</button>
				</div>
			</div>
			<div v-if="question.edit">
				<form class="form" @submit.prevent="saveQuestion(question)">
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.question}]">
						<label for="question">Question:</label>
						<p v-if="question.basic" class="form-control-static">@{{question.question}}</p>
						<textarea v-if="!question.basic" rows="2"  v-model="question.question" name="question" class="form-control"></textarea>
						<span v-if="formerrors && formerrors.question" class="help-block">
							@{{ formerrors.question[0] }}
						</span>
					</div>
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.answer}]">
						<label for="answer">Answer:</label>
						<textarea rows="2" v-model="question.answer" name="answer" class="form-control"></textarea>
						<span v-if="formerrors && formerrors.answer" class="help-block">
							@{{ formerrors.answer[0] }}
						</span>
					</div>
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.category}]">
						<label for="Category">Category:</label>
						<p v-if="question.basic" class="form-control-static">@{{question.category}}</p>
						<select v-if="!question.basic" v-model="question.category" class="form-control">
							<option value="Business"  v-bind:selected="question.category == 'Business'">Business</option>
							<option value="Web" v-bind:selected="question.category == 'Web'">Web</option>
							<option value="Phone" v-bind:selected="question.category == 'Phone'">Phone</option>
						</select>
						<span v-if="formerrors && formerrors.category" class="help-block">
							@{{ formerrors.category[0] }}
						</span>
					</div>
					<button type="submit" class="btn btn-sm  btn-primary" ><span class="glyphicon glyphicon-save"></span> Save</button>
					<button v-on:click.stop.prevent="clearQuestion(question)" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-erase"></span> Clear				</form>
			</div>
		</div>
		</div>
	</div>
</div>		
	
<!--	<pre>@{{ $data | json}}</pre> !-->


@endsection

@section('addscripts')
<script src="{{ asset('js/vue.js') }}"></script>
<script>
toastr.options = {
  "positionClass": "toast-bottom-right",
}




    var vm = new Vue({
          el: '#questions',
          data: {
			newquestion: {
				id: 0,
				question: '',
				answer: '',
				category: 'Business',
				basic: false,
				edit: false
			},  
			questions: {!! $questions !!},
			formerrors: null,
          },
		  methods: {
			deleteQuestion: function(index,question_id) {
				var _this = this;
				axios.delete('{{ url('/questions')}}/'+question_id)
					.then(function (response) {
						_this.questions.splice(index,1);
						toastr.success('Your question was deleted','Success!');
					})
					.catch(function (error)	{
						toastr.error('Oops something went wrong','Error!');
					});
			  },
			clearQuestion: function(question) {
				question.edit = false;
			  },
			  editQuestion: function(question) {
				question.edit = true;
			  },
			  addQuestion: function() {
				this.newquestion.edit = true;
				this.newquestion.id = 0;
				this.newquestion.question = '';
				this.newquestion.answer = '';
				this.newquestion.basic = false;
				this.formerrors = null;
			  },
			  saveQuestion: function(question) {
				axios.put('{{ url('/questions')}}/'+question.id ,question)
					.then(function (response) {
						question.edit = false;
						toastr.success('Your question was updated','Success!');
					})
					.catch(function (error)	{
						this.formerrors = error.response.data;
						errors = error.response.data;
						for(var error in errors) { 
							for(i=0;i<errors[error].length;i++) {
								toastr.error(errors[error][i]);
							}
						}
					});
			  },
			  savenewQuestion: function() {
				this.formerrors = null;
				var _this = this;
				axios.post('{{ url('/questions')}}/',_this.newquestion)
					.then(function (response) {
						_this.newquestion.id = response.data;
						_this.newquestion.edit = false;
						_this.questions.push(Vue.util.extend({},_this.newquestion));
						toastr.success('Your new question was saved','Success!');
						_this.newquestion.id = 0;
						_this.newquestion.question = '';
						_this.newquestion.answer = '';
						_this.newquestion.category = 'Business';
						_this.newquestion.basic = false;
						_this.newquestion.edit = false;
						
					})
					.catch(function (error)	{
						this.formerrors = error.response.data;
						errors = error.response.data;
						for(var error in errors) { 
							for(i=0;i<errors[error].length;i++) {
								toastr.error(errors[error][i]);
							}
						}
					});
			  }
		  }
    });

 
</script>
@endsection
