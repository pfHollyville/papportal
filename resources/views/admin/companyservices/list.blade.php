@extends('admin.layouts.app')

@section('title','Company Services Management')

@section('content')
<a title="Add New Company Service" type="button" class="btn btn-outline-primary" href="{{route('admin.companyservices.create')}}">
	<span class="icon icon-add-to-list"></span> New Service
</a>
<div class="container">
		<div class="dashhead-toolbar">
			<form id="create_companyservices" class="form form-navbar" action="{{ route('admin.companyservices.filter') }}" method="post" role="form" style="display: block;">
					{{ csrf_field() }}
			<div class="dashhead-toolbar-item form-group">		
				<select name="fcompany" id="fcompany" class="form-control">
					<option value="0">Any Company</option>
					@foreach($companies as $company)
					<option value="{{$company->id}}"  
						@if($filter['fcompany'] == $company->id) 
							selected
						@endif 
					>{{$company->getRef()}} - {{$company->companyname}}</option>
					@endforeach
				</select>
			</div>
			<div class="dashhead-toolbar-item form-group">		
				<select name="fservice" id="fservice" class="form-control">
					<option value="0">Any Service</option>
					@foreach($services as $service)
						<option value="{{$service->id}}"  @if($filter['fservice'] == $service->id) selected @endif   >{{$service->name}}</option>
					@endforeach
				</select>
			</div>
			<div class="dashhead-toolbar-item form-group">		
				<select name="fstatus" id="fstatus" class="form-control">
					<option value="All" @if($filter['fstatus'] == 'All') selected @endif>Any status</option>
					<option value="Pending" @if($filter['fstatus'] == 'Pending') selected @endif>Pending</option>
					<option value="Approved" @if($filter['fstatus'] == 'Approved') selected @endif>Approved</option>
					<option value="Rejected" @if($filter['fstatus'] == 'Rejected') selected @endif>Rejected</option>
				</select>
			</div>
			<div class="btn-group dashhead-toolbar-item">
					<button type="submit" name="filtercompanyservice-submit" id="filtercompanyservice-submit" tabindex="4" class="btn btn-outline-primary"> 
					<span class="icon icon-funnel"></span> Filter</button>
			</div>
			</form>		
		</div>
</div>


<div class="col-md-12">

<div class="row">
	<div class="col-sm-8">
	 {{ $companyservices->links() }}
	</div>
</div>
<div class="row">

	<table id="serviceTable" class="table table-responsive" data-sort="table">
		<thead>
			<tr>
				<th class="header">Edit</th>
				<th class="header">Company</th>
				<th class="header">Service</th>
				<th class="header">Status</th>
				<th class="header">Widget ID</th>
				<th class="header">Dataset ID</th>
				<th class="header">Queue ID</th>
				<th class="header-right">Chats</th>
			</tr>
		</thead>
		<tbody>
		@foreach($companyservices as $companyservice)
		<tr>
				<td><a href="{{route('admin.companyservices.edit',['id' =>$companyservice->id])}}"><span class="icon icon-pencil"></span></a></td>
				<td><a href="{{route('admin.companies.show',['id' =>$companyservice['company']['id']])}}">{{$companyservice['company']['companyname']}}</a></td>
				<td><a href="{{route('admin.services.show',['id' =>$companyservice['service']['id']])}}">{{$companyservice['service']['name']}}</a></td>
				<td>{{ucfirst($companyservice->status)}}</td>
				<td>{{$companyservice['widget_id']}}</td>
				<td>{{$companyservice['datasetid']}}</td>
				<td>{{$companyservice['queueid']}}</td>
				<td class="text-right">
					@isset($companyservice['widget_id'])
						{{$companyservice->webchats()->count()}}
					@endisset
				</td>
		</tr>
		@endforeach
		</tbody>
	</table>

</div>
<div class="row">
	<div class="offset-col-8 col-sm-4">
	{{ $companyservices->links() }}
	</div>
</div>
	

</div>
@endsection
@section('addscripts')
<script>
	$(document).ready(function() 
		{ 
			$("#serviceTable").tablesorter(); 
		} 
	); 

</script>
@endsection
