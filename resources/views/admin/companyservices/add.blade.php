@extends('admin.layouts.app')

@section('title','Company Service - Add New Service')

@section('content')
  <div class="col-md-12">
	<h3>Add a service to a company</h3>

@if(count($errors)>0)
	<div class="form-group has-warning">
		<div class="form-control-label">Sorry but there are errors with this service; please correct them and ty to save again</div>
	</div>
@endif

	<form id="create_companyservices" class="form" action="{{ route('admin.companyservices.store') }}" method="post" role="form" style="display: block;">
		    {{ csrf_field() }}
			<div class="form-group @if($errors->has('company_id')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="company_id">Company:</label>
				<div class="col-sm-8 input-group">
				<select name="company_id" id="company_id" tabindex="3" class="form-control" >
					@foreach($companies as $company)
					<option value="{{$company['id']}}" @if (old('company_id') ==$company['id']) ? selected @endif >{{$company['companyname']}}</option>
					@endforeach
				</select>
				</div>
			</div>
			<div class="form-group @if($errors->has('service_id')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="service_id">Service:</label>
				<div class="col-sm-8 input-group">
				<select name="service_id" id="service_id" tabindex="3" class="form-control" >
					<option value="" selected>Select Service</option>
					@foreach($services as $service)
					<option value="{{$service['id']}}" @if (old('service_id') ==$service['id']) ? selected @endif >{{$service['name']}}</option>
					@endforeach
				</select>
				</div>
			</div>
			<div class="form-group @if($errors->has('start_date')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="start_date">Start Date:</label>
				<div class="input-group col-sm-8">
				  <input data-provide="datepicker" name="start_date" id="start_date" class="form-control @if($errors->has('start_date')) form-control-warning @endif" >
				</div>
				@if($errors->has('start_date'))
							@foreach ($errors->get('start_date') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('end_date')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="end_date">End Date:</label>
				<div class="input-group col-sm-8">
				  <input data-provide="datepicker" name="end_date" id="end_date" class="form-control @if($errors->has('end_date')) form-control-warning @endif" >
				</div>
				@if($errors->has('end_date'))
							@foreach ($errors->get('end_date') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('status')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="status">Select:</label>
				<div class="input-group col-sm-8">
					<select name="status" class="form-control">
						<option value="pending"  selected >Pending</option>
						<option value="approved" >Approved</option>
						<option value="rejected" >Rejected</option>
						<option value="cancelled" >Cancelled</option>
					</select>
				</div>
				@if($errors->has('status'))
							@foreach ($errors->get('status') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('maxusage')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="maxusage">Max Usage:</label>
				<div class="input-group col-sm-8">
				  <input type="text" name="maxusage" class="form-control @if($errors->has('maxusage')) form-control-warning @endif" id="maxusage" placeholder="0.00" value="{{ old('maxusage') }}">
				</div>
				@if($errors->has('maxusage'))
							@foreach ($errors->get('maxusage') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('mincommitment')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="mincommitment">Min Commitment:</label>
				<div class="input-group col-sm-8">
				  <input type="text" name="mincommitment" class="form-control @if($errors->has('mincommitment')) form-control-warning @endif" id="mincommitment" placeholder="0.00" value="{{ old('mincommitment') }}">
				</div>
				@if($errors->has('mincommitment'))
							@foreach ($errors->get('mincommitment') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('basecost')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="basecost">Monthly Cost:</label>
				<div class="input-group col-sm-8">
				  <input type="text" name="basecost" class="form-control @if($errors->has('basecost')) form-control-warning @endif" id="basecost" placeholder="0.00" value="{{ old('basecost') }}">
				</div>
				@if($errors->has('basecost'))
							@foreach ($errors->get('basecost') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('pertransactioncost')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="pertransactioncost">Unit cost:</label>
				<div class="input-group col-sm-8">
				  <input type="text" name="pertransactioncost" class="form-control @if($errors->has('pertransactioncost')) form-control-warning @endif" id="pertransactioncost" placeholder="0.00" value="{{ old('pertransactioncost') }}">
				</div>
				@if($errors->has('pertransactioncost'))
							@foreach ($errors->get('pertransactioncost') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('units')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="units">Select:</label>
				<div class="input-group col-sm-8">
					<select name="units" id="units" class="form-control">
						<option value="per minute"  @if (old('units') == 'per minute') selected @endif>Per Minute</option>
						<option value="per month" @if (old('units') == 'per month') selected @endif>Per Month</option>
						<option value="per webchat" @if (old('units') == 'per webchat') selected @endif>Per Webchat</option>
						<option value="per call"  @if (old('units') == 'per call') selected @endif>Per Call</option>
					</select>
				</div>
				@if($errors->has('units'))
							@foreach ($errors->get('units') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('widget_id')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="widget_id">Webchat ID:</label>
				<div class="input-group col-sm-8">
				  <input type="text" name="widget_id" class="form-control @if($errors->has('widget_id')) form-control-warning @endif" id="widget_id" placeholder="webchat reference number" value="{{ old('widget_id') }}">
				</div>
				@if($errors->has('widget_id'))
							@foreach ($errors->get('widget_id') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('datasetid')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="datasetid">Dataset ID:</label>
				<div class="input-group col-sm-8">
				  <input type="text" name="datasetid" class="form-control @if($errors->has('datasetid')) form-control-warning @endif" id="datasetid" placeholder="webchat reference number" value="{{ old('datasetid') }}">
				</div>
				@if($errors->has('datasetid'))
							@foreach ($errors->get('datasetid') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('queueid')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="queueid">Queue ID:</label>
				<div class="input-group col-sm-8">
				  <input type="text" name="queueid" class="form-control @if($errors->has('queueid')) form-control-warning @endif" id="queueid" placeholder="webchat reference number" value="{{ old('queueid') }}">
				</div>
				@if($errors->has('queueid'))
							@foreach ($errors->get('queueid') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>

		<br/>
			<div class="form-group">
				<div class="offset-col-4 col-sm-8 input-group">
					<input type="submit" name="addcompanyservice-submit" id="addcompanyservice-submit" tabindex="4" class="btn btn-primary" value="Save">
				</div>
			</div>
	</form>

  
  </div>
@endsection

@section('addscripts')
<script>
	var $info;
	$(document).ready(function(){
		$( "#start_date" ).datepicker({ 
			format: 'dd/mm/yyyy'
		});
		$( "#end_date" ).datepicker({ 
			format: 'dd/mm/yyyy'
		});
		$( "#service_id" ).change(function() {
			$service = "http://localhost:8002/api/services/"  + this.value ;
			$.ajax({
				url: $service,
			}).done(function(data){
				$info = data;
				$("#maxusage").val(data.maxusage);
				$("#basecost").val(data.basecost);
				$("#mincommitment").val(data.mincommitment);
				$("#pertransactioncost").val(data.extracost);
				$("#units").val(data.units);
			})
		});

		
		
	});
	
	
</script>
@endsection
