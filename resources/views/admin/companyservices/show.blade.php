@extends('admin.layouts.app')

@section('title','Company Service Details')

@section('content')
 <div class="col-md-12">
 
		<dl class="row">
			<dt class="col-sm-4">Company</dt>
			<dd class="col-sm-8">{{$companyservice['company']['companyname']}}</dd>
			<dt class="col-sm-4">Servics</dt>
			<dd class="col-sm-8">{{$companyservice['service']['name']}}</dd>
			<dt class="col-sm-4">Start Date</dt>
			<dd class="col-sm-8">{{$companyservice['start_date']}}</dd>
			<dt class="col-sm-4">End Date</dt>
			<dd class="col-sm-8">{{$companyservice['end_date']}}</dd>
			<dt class="col-sm-4">Status</dt>
			<dd class="col-sm-8">{{ucfirst($companyservice['status'])}}</dd>
			<dt class="col-sm-4">Max Usage</dt>
			<dd class="col-sm-8">{{$companyservice['maxusage']}}</dd>
			<dt class="col-sm-4">Min Commitment</dt>
			<dd class="col-sm-8">{{$companyservice['mincommitment']}}</dd>
			<dt class="col-sm-4">Monthly Cost</dt>
			<dd class="col-sm-8">{{$companyservice['basecost']}}</dd>
			<dt class="col-sm-4">Unit Cost</dt>
			<dd class="col-sm-8">{{$companyservice['pertransactioncost']}}</dd>
			<dt class="col-sm-4">Units</dt>
			<dd class="col-sm-8">{{$companyservice['units']}}</dd>
			<dt class="col-sm-4">Webchat ID</dt>
			<dd class="col-sm-8">{{$companyservice['widget_id']}}</dd>
			<dt class="col-sm-4">Dataset ID</dt>
			<dd class="col-sm-8">{{$companyservice['datasetid']}}</dd>
			<dt class="col-sm-4">Queue ID</dt>
			<dd class="col-sm-8">{{$companyservice['queueid']}}</dd>
			<dt class="col-sm-4">Created</dt>
			<dd class="col-sm-8">{{$companyservice['created_at']}}</dd>
			<dt class="col-sm-4">Updated</dt>
			<dd class="col-sm-8">{{$companyservice['updated_at']}}</dd>
		</dl>
</div>
@endsection
