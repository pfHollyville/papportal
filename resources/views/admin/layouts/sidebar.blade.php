	<nav class="sidebar-nav">
		<div class="sidebar-header">
			<button class="nav-toggler nav-toggler-md sidebar-toggler" type="button" data-toggle="collapse"
			data-target="#nav-toggleable-md">
				<span class="sr-only">Toggle nav</span>
			</button> 
			<a class="sidebar-brand img-responsive" href="{{route('admin.dashboard')}}"></a>
		</div>
		<div class="collapse nav-toggleable-md" id="nav-toggleable-md">
			<ul class="nav nav-pills nav-stacked flex-column sidebar-menu">
				<li class="nav-header">Administration</li>
				<li class="nav-item">
					<a class="nav-link" href="{{route('admin.dashboard')}}"><span class="icon icon-bar-graph"></span> Dashboard</a>
				</li>
            @if (!Auth::guest())
				@if (Auth::user()->isAdmin())
   				<li class="nav-item">
					<a class="nav-link" href="{{ route('admin.users.index')}}"><span class="icon icon-users"></span> Users</a>
				</li>
				@endif
				<li class="nav-item">
					<a class="nav-link" href="{{ route('admin.companies.index')}}"><span class="icon icon-briefcase"></span> Companies</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('admin.companyservices.index')}}"><span class="icon icon-cog"></span> Company Services</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('admin.questions.index')}}"><span class="icon icon-info"></span> Company Questions</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('admin.webchats.index')}}"><span class="icon icon-chat"></span> Web chats</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('admin.queues.index')}}"><span class="icon icon-chat"></span> Queues</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('admin.notes.index')}}"><span class="icon icon-chat"></span> Notes</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('admin.phonecontacts.index')}}"><span class="icon icon-chat"></span> Phone Contact</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('admin.calls.index')}}"><span class="icon icon-chat"></span> Call Records</a>
				</li>
				@if (Auth::user()->isAdmin())
				<li class="nav-item">
					<a class="nav-link" href="{{ route('admin.bills.index')}}""><span class="icon icon-credit-card"></span> Billing</a>
				</li>
				<li class="nav-header">Settings</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('admin.services.index')}}">Services</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('admin.servicecategories.index')}}">Service Categories</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('admin.basicquestionmasters.index')}}">Basic Questions</a>
				</li>
				@endif
			@endif
			</ul>
			<hr class="visible-xs mt-3" />
		</div>
	</nav>