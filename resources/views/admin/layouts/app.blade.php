<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/application.css') }}" rel="stylesheet">
    <link href="{{ asset('css/toolkit-light.css') }}" rel="stylesheet">
    <link href="{{ asset('css/toastr.css') }}" rel="stylesheet">
	
	<style>
	[v-cloak] {
	  display: none;
	}

.modal-mask {
  position: fixed;
  z-index: 9998;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: rgba(0, 0, 0, .5);
  display: table;
  transition: opacity .3s ease;
}

.modal-wrapper {
  display: table-cell;
  vertical-align: middle;
}

.modal-container {
  width: 300px;
  margin: 0px auto;
  padding: 20px 30px;
  background-color: #fff;
  border-radius: 2px;
  box-shadow: 0 2px 8px rgba(0, 0, 0, .33);
  transition: all .3s ease;
  font-family: Helvetica, Arial, sans-serif;
}

.modal-header h3 {
  margin-top: 0;
  color: #42b983;
}

.modal-body {
  margin: 20px 0;
}

.modal-default-button {
  float: right;
}

/*
 * The following styles are auto-applied to elements with
 * transition="modal" when their visibility is toggled
 * by Vue.js.
 *
 * You can easily play with the modal transition by editing
 * these styles.
 */

.modal-enter {
  opacity: 0;
}

.modal-leave-active {
  opacity: 0;
}

.modal-enter .modal-container,
.modal-leave-active .modal-container {
  -webkit-transform: scale(1.1);
  transform: scale(1.1);
}


.glyphicon.fast-right-spinner {
    -webkit-animation: glyphicon-spin-r 1s infinite linear;
    animation: glyphicon-spin-r 1s infinite linear;
}

.glyphicon.normal-right-spinner {
    -webkit-animation: glyphicon-spin-r 2s infinite linear;
    animation: glyphicon-spin-r 2s infinite linear;
}

.glyphicon.slow-right-spinner {
    -webkit-animation: glyphicon-spin-r 3s infinite linear;
    animation: glyphicon-spin-r 3s infinite linear;
}

.glyphicon.fast-left-spinner {
    -webkit-animation: glyphicon-spin-l 1s infinite linear;
    animation: glyphicon-spin-l 1s infinite linear;
}

.glyphicon.normal-left-spinner {
    -webkit-animation: glyphicon-spin-l 2s infinite linear;
    animation: glyphicon-spin-l 2s infinite linear;
}

.glyphicon.slow-left-spinner {
    -webkit-animation: glyphicon-spin-l 3s infinite linear;
    animation: glyphicon-spin-l 3s infinite linear;
}

@-webkit-keyframes glyphicon-spin-r {
    0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }

    100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
    }
}

@keyframes glyphicon-spin-r {
    0% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }

    100% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
    }
}

@-webkit-keyframes glyphicon-spin-l {
    0% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
    }

    100% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
    }
}

@keyframes glyphicon-spin-l {
    0% {
        -webkit-transform: rotate(359deg);
        transform: rotate(359deg);
    }
	
	</style>
</head>
<body>
  <div id="app">

  <div class="container">
    <div class="row">
		
	<div class="col-md-3 sidebar">
		@include('admin.layouts.sidebar')
	</div>


<div class="col-md-9 content">
    <div class="dashhead">
	  <div class="dashhead-titles">
		<h6 class="dashhead-subtitle">Administration</h6>
		<h2 class="dashhead-title">@yield('title')</h2>
	  </div>

		<div class="btn-toolbar dashhead-toolbar">
		@if (Auth::guest())
			<div><a href="{{ route('admin.login') }}"><span class="icon icon-user"></span> Login</a></div>
		@else
			<div>User: <span class="icon icon-user"></span> {{ Auth::user()->first_name}}</div>
			<div>&nbsp;
				<a href="{{ route('admin.logout') }}" onclick="event.preventDefault();                                                     document.getElementById('logout-form').submit();">
					<span class="icon icon-log-out"></span> Logout</a>
			</div>
			<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					{{ csrf_field() }}
			</form>
		@endif
		</div>
	</div>

<hr class="mt-3">

	  <div class="container">
				@if (session('status'))
					<div class="alert alert-{{session('status.class')}}">
						{{ session('status.text') }}
					</div>
				@endif		
				@yield('content')
		</div>
		

</div>
</div>
</div>
</div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/tether.min.js') }}"></script>
    <script src="{{ asset('js/chart.js') }}"></script>
    <script src="{{ asset('js/tablesorter.min.js') }}"></script>
    <script src="{{ asset('js/toolkit.js') }}"></script></body>
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    <!-- Include jQuery (required) and the JS -->
	@yield('addscripts')
</html>
