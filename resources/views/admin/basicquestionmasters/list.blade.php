@extends('admin.layouts.app')

@section('title','Basic Questions - Master List')

@section('content')
 <div class="col-md-12">
 {{ $basicquestionmasters->links() }}

	<div class="table-responsive">
	<table id="basicquestionmasterTable" class="table" data-sort="table">
		<thead>
			<tr>
				<th class="header">Edit</th>
				<th class="header">Question</th>
				<th class="header">Category</th>
				<th class="header">Order</th>
			</tr>
		</thead>
	<tbody>
	@foreach($basicquestionmasters as $basicquestionmaster)
	<tr>
		<td><a href="{{ route('admin.basicquestionmasters.edit',['id'=>$basicquestionmaster->id])}}">Edit</a></td>
		<td>{{$basicquestionmaster->question}}</td>
		<td>{{$basicquestionmaster->category}}</td>
		<td>{{$basicquestionmaster->order}}</td>
	</tr>
	@endforeach
	</tbody>
	</table>
	</div>

	{{ $basicquestionmasters->links() }}
</div>
@endsection
@section('addscripts')
<script>
	$(document).ready(function() 
		{ 
			$("#basicquestionmasterTable").tablesorter(); 
		} 
	); 

</script>
@endsection
