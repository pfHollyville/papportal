@extends('admin.layouts.app')

@section('title','Edit Basic Question')

@section('content')
  <div class="col-md-12">
	<h3>Edit Basic Question</h3>

@if(count($errors)>0)
	<div class="alert alert-warning">Sorry but there are errors with this question; please correct them and ty to save again</div>
@endif

	<form id="create_basicquestionmaster" class="form" action="{{ route('admin.basicquestionmasters.update',['id' => $basicquestionmaster->id]) }}" method="post" role="form" style="display: block;">
		    {{ csrf_field() }}
			{{ method_field('PUT') }}
		<div class="form-group @if($errors->has('question')) has-warning @endif">
			<label class="col-sm-4 text-left" for="question">Question:</label>
			<div class="input-group col-sm-8 form-inline required">
				<input type="text" name="question" id="question" tabindex="1" class="form-control" placeholder="my question" value="{{ $basicquestionmaster->question }}" required>
			</div>
			@if($errors->has('question'))
					@foreach ($errors->get('question') as $message)
						<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
					@endforeach
			@endif
		</div>
		<div class="form-group @if($errors->has('category')) has-warning @endif">
			<label class="col-sm-4 text-left" for="category">Category:</label>
			<div class="input-group col-sm-8 form-inline required">
				<input type="text" name="category" id="category" tabindex="1" class="form-control" placeholder="my category" value="{{ $basicquestionmaster->category }}" required>
			</div>
			@if($errors->has('category'))
					@foreach ($errors->get('category') as $message)
						<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
					@endforeach
			@endif
		</div>
		<div class="form-group @if($errors->has('order')) has-warning @endif">
			<label class="col-sm-4 text-left" for="order">Order:</label>
			<div class="input-group col-sm-8 form-inline required">
				<input type="text" name="order" id="order" tabindex="1" class="form-control" placeholder="my order" value="{{ $basicquestionmaster->order }}" required>
			</div>
			@if($errors->has('order'))
					@foreach ($errors->get('order') as $message)
						<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
					@endforeach
			@endif
		</div>
		<br/>
		<div class="col-sm-3 offset-sm-6">
				<input type="submit" name="editbasiccategorymaster-submit" id="editbasiccategorymaster-submit" tabindex="4" class="btn btn-login" value="Save">
		</div>
	</form>

  
  </div>
@endsection
