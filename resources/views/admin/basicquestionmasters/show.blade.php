@extends('admin.layouts.app')

@section('title','Baisc Questions - Master')

@section('content')
 <div class="col-md-12">
	<div class="table-responsive">
	<table id="companytable" class="table" data-sort="table">
		<thead>
			<tr>
				<th class="header">Edit</th>
				<th class="header">Question</th>
				<th class="header">Category</th>
				<th class="header">Order</th>
			</tr>
		</thead>
	<tbody>
	<tr>
		<td><a href="{{ route('admin.basicquestionmasters.edit',['id'=>$basicquestionmaster->id])}}">Edit</a></td>
		<td>{{$basicquestionmaster->question}}</td>
		<td>{{$basicquestionmaster->category}}</td>
		<td>{{$basicquestionmaster->order}}</td>
	</tr>
	</tbody>
	</table>
	</div>
</div>
@endsection
