@extends('admin.layouts.app')

@section('title','Question')

@section('content')
 <div class="col-md-12">
 <div class="btn-group">
      <a title="Question List" type="button" class="btn btn-outline-primary" href="{{route('admin.questions.index')}}">
        <span class="icon icon-list"></span>
      </a>
      <a title="Edit" type="button" class="btn btn-outline-primary" href="{{route('admin.questions.edit',['id'=>$question->id])}}">
        <span class="icon icon-edit"></span>
      </a>
</div>

 		<dl class="row">
			<dt class="col-sm-4">Company Name</dt>
			<dd class="col-sm-8">{{$question->company->companyname}}</dd>
			<dt class="col-sm-4">Question</dt>
			<dd class="col-sm-8">{{$question->question}}</dd>
			<dt class="col-sm-4">Category</dt>
			<dd class="col-sm-8">{{$question->category}}</dd>
			<dt class="col-sm-4">Order</dt>
			<dd class="col-sm-8">{{$question->order}}</dd>
			<dt class="col-sm-4">Anser</dt>
			<dd class="col-sm-8">{{$question->answer}}</dd>
		</dl>
</div>
@endsection
