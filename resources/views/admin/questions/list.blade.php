@extends('admin.layouts.app')

@section('title','Company Questions')

@section('content')
<div class="btn-group">
	  <a title="Add new question" type="button" class="btn btn-outline-primary" href="{{route('admin.questions.create')}}">
		<span class="icon icon-pencil"></span>
	  </a>
</div>

<div class="container">
		<div class="dashhead-toolbar">
			<form id="filter-questions" class="form form-navbar" action="{{ route('admin.questions.filter') }}" method="get" role="form" style="display: block;">
			<div class="dashhead-toolbar-item form-group">		
				<select name="company" id="company" class="form-control">
					<option value="0">Any Company</option>
					@foreach($allCompanies as $company)
					<option value="{{$company->getRef()}}"  
						@if($filter['fcompany'] == $company->id) 
							selected
						@endif 
					>{{$company->getRef()}} - {{$company->companyname}}</option>
					@endforeach
				</select>
			</div>
			<div class="dashhead-toolbar-item form-group">		
				<select name="qtype" id="qtype" class="form-control">
					<option value="2">All Questions</option>
					<option value="0">FAQ</option>
					<option value="1">Basic</option>
				</select>
			</div>
			<div class="btn-group dashhead-toolbar-item">
					<button type="submit" tabindex="4" class="btn btn-outline-primary"> 
					<span class="icon icon-funnel"></span> Filter</button>
			</div>
			</form>		
		</div>
</div>

 <div class="col-md-12">
 
 
 {{ $questions->links() }}

	<div class="table-responsive">
	<table id="questionTable" class="table" data-sort="table">
		<thead>
			<tr>
				<th class="header">Edit</th>
				<th class="header">Company</th>
				<th class="header">Question</th>
				<th class="header">Category</th>
				<th class="header">Order</th>
				<th class="header">Answer</th>
			</tr>
		</thead>
	<tbody>
	@foreach($questions as $question)
	<tr>
		<td><a href="{{ route('admin.questions.edit',['id'=>$question->id])}}">Edit</a></td>
		<td>{{$question->company->companyname}}</td>
		<td>{{$question->question}}</td>
		<td>{{$question->category}}</td>
		<td>{{$question->order}}</td>
		<td>{{$question->answer}}</td>
	</tr>
	@endforeach
	</tbody>
	</table>
	</div>

	{{ $questions->links() }}
</div>
@endsection
@section('addscripts')
<script>
	$(document).ready(function() 
		{ 
			$("#questionTable").tablesorter(); 
		} 
	); 

</script>
@endsection
