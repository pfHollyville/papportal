@extends('admin.layouts.basic')

@section('title','Company Questions')

@section('content')
<div class="row">
	<div class="col-md-6">
		<h4>Basic Questions</h4>
	</div>
	<div class="col-md-6">
		<h4>FAQs</h4>
	</div>
</div>
<div class="row">
	<div class="col-md-6">
		@foreach($basicquestions as $index => $question)
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" href="#collapse-b-{{$index}}">{{$question->question}}</a>
					</h4>
				</div>
				<div id="collapse-b-{{$index}}" class="panel-collapse collapse">
					<div class="panel-body">
						<div><span>{{$question->category}}</span> - <span>{{$question->order}}</span></div>
						<p>{{$question->answer}}</p>
					</div>
				</div>
			</div>
		</div>
		@endforeach
	
	</div>
	<div class="col-md-6">
		@foreach($faqquestions as $index => $question)
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a data-toggle="collapse" href="#collapse-f-{{$index}}">{{$question->question}}</a>
					</h4>
				</div>
				<div id="collapse-f-{{$index}}" class="panel-collapse collapse">
					<div class="panel-body">
						<div><span>{{$question->category}}</span> - <span>{{$question->order}}</span></div>
						<p>{{$question->answer}}</p>
					</div>
				</div>
			</div>
		</div>
		@endforeach
	</div>
</div>
@endsection
@section('addscripts')
@endsection
