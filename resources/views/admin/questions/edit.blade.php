@extends('admin.layouts.app')

@section('title','Edit Question')

@section('content')
  <div class="col-md-12">

@if(count($errors)>0)
	<div class="alert alert-warning">Sorry but there are errors with this question; please correct them and ty to save again</div>
@endif

	<form id="create_question" class="form" action="{{ route('admin.questions.update',['id' => $question->id]) }}" method="post" role="form" style="display: block;">
		    {{ csrf_field() }}
			{{ method_field('PUT') }}
		<div class="form-group @if($errors->has('company_id')) has-warning @endif">
			<label class="form-control-label col-sm-4" for="company_id">Company:</label>
			<div class="col-sm-8 input-group">
			<select name="company_id" id="company_id" tabindex="3" class="form-control" >
				@foreach($companies as $company)
				<option value="{{$company->id}}" 
					@if ($question->company_id == $company->id)
						selected 
					@endif 
				>{{$company->companyname}}</option>
				@endforeach
			</select>
			</div>
		</div>
		<div class="form-group @if($errors->has('question')) has-warning @endif">
			<label class="col-sm-4 text-left" for="question">Question:</label>
			<div class="input-group col-sm-8 form-inline required">
				<input type="text" name="question" id="question" tabindex="1" class="form-control" placeholder="my question" value="{{ $question->question }}" required>
			</div>
			@if($errors->has('question'))
					@foreach ($errors->get('question') as $message)
						<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
					@endforeach
			@endif
		</div>
		<div class="form-group @if($errors->has('category')) has-warning @endif">
			<label class="col-sm-4 text-left" for="category">Category:</label>
			<div class="input-group col-sm-8 form-inline required">
				<input type="text" name="category" id="category" tabindex="1" class="form-control" placeholder="my category" value="{{ $question->category }}" required>
			</div>
			@if($errors->has('category'))
					@foreach ($errors->get('category') as $message)
						<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
					@endforeach
			@endif
		</div>
		<div class="form-group @if($errors->has('order')) has-warning @endif">
			<label class="col-sm-4 text-left" for="order">Order:</label>
			<div class="input-group col-sm-8 form-inline required">
				<input type="text" name="order" id="order" tabindex="1" class="form-control" placeholder="my order" value="{{ $question->order }}" required>
			</div>
			@if($errors->has('order'))
					@foreach ($errors->get('order') as $message)
						<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
					@endforeach
			@endif
		</div>
		<div class="form-group @if($errors->has('answer')) has-warning @endif">
			<label class="col-sm-4 text-left" for="answer">Answer:</label>
			<div class="input-group col-sm-8 form-inline required">
				<textarea name="answer" id="answer" tabindex="1" class="form-control" placeholder="my answer"  rows="4" required>{{ $question->answer }}</textarea>
			</div>
			@if($errors->has('answer'))
					@foreach ($errors->get('answer') as $message)
						<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
					@endforeach
			@endif
		</div>
		<br/>
		<div class="col-sm-3 offset-sm-6">
				<input type="submit" name="editbasiccategorymaster-submit" id="editbasiccategorymaster-submit" tabindex="4" class="btn btn-login" value="Save">
		</div>
	</form>

  
  </div>
@endsection
