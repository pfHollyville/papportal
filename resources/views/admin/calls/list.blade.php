@extends('admin.layouts.app')

@section('title','Calls')

@section('content')


 <div class="col-md-12">
 
 
 {{ $calls->links() }}

	<div class="table-responsive">
	<table id="callTable" class="table" data-sort="table">
		<thead>
			<tr>
				<th class="header">Call ID</th>
				<th class="header">Queue ID</th>
				<th class="header">Dataset ID</th>
				<th class="header">Agent</th>
				<th class="header">Duration</th>
				<th class="header">Type</th>
				<th class="header">Chargeable</th>
			</tr>
		</thead>
	<tbody>
	@foreach($calls as $call)
	<tr>
		<td>{{$call->callid}}</td>
		<td>{{$call->qid}}</td>
		<td>{{$call->dataset}}</td>
		<td>{{$call->agent}}</td>
		<td>{{$call->duration}}</td>
		<td>{{$call->type}}</td>
		<td><input type="checkbox"  name="ids" value="{{$call->chargeable}}" id="{{$call->id}}" @if( $call->chargeable == 1) checked @endif/></td>
	</tr>
	@endforeach
	</tbody>
	</table>
	</div>

	{{ $calls->links() }}
	
	<button class"btn btn-primary" onclick="saveChargeable()">Update chargeable</button>
</div>
@endsection
@section('addscripts')
<script>
	$(document).ready(function() 
	{ 
		$("#callTable").tablesorter(); 


	}); 
	
		function saveChargeable() {
		
			$chargeable = [];
			$("[name=ids]").each(function(index,cb) { 
				item = {"id" : cb.id , "checked" : cb.checked};
				$chargeable.push(item);
		
			});
			
			data = JSON.stringify($chargeable);
			url = "http://localhost:8002/api/calls/chargeable";
			$.ajax({
				type: "POST",
				url: url,
				data: {"data" : data}
			})
		
		}

	
	
</script>
@endsection
