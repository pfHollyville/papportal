@extends('admin.layouts.app')

@section('title','Webchats')

@section('content')
 <div class="col-md-12">
<div class="row">
	<div class="col-sm-8">
	 {{ $webchats->links() }}
	</div>
</div>
<div class="row">
	<div class="table-responsive">
	<table id="webchatTable" class="table" data-sort="table">
		<thead>
			<tr>
				<th class="header">Chat Date / Time</th>
				<th class="header">Company</th>
				<th class="header">Service</th>
				<th class="header">Widget ID</th>
				<th class="header">Requester Name</th>
				<th class="header">Requester Email</th>
			</tr>
		</thead>
	<tbody>
	@foreach($webchats as $webchat)
	<tr>
		<td>{{$webchat->created_at_date}}</td>
		<td>{{$webchat->companyservice->company->companyname}}</td>
		<td>{{$webchat->companyservice->service->name}}</td>
		<td>{{$webchat->widget_id}}</td>
		<td>{{$webchat->requester_details_name}}</td>
		<td>{{$webchat->requester_details_emails}}</td>
	</tr>
	@endforeach
	</tbody>
	</table>
	</div>
</div>
<div class="row">
	<div class="offset-col-8 col-sm-4">
	{{ $webchats->links() }}
	</div>
</div>
</div>
@endsection

@section('addscripts')
<script>
	$(document).ready(function() 
		{ 
			$("#webchatTable").tablesorter(); 
		} 
	); 

</script>
@endsection
