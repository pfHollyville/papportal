@extends('admin.layouts.app')

@section('title','Company Details')

@section('content')
<div class="col-md-12">

<div class="row">
<div class="btn-group">
      <a title="Company List" type="button" class="btn btn-outline-primary" href="{{route('admin.companies.index')}}">
        <span class="icon icon-list"></span>
      </a>
      <a title="Edit" type="button" class="btn btn-outline-primary" href="{{route('admin.companies.edit',['id'=>$company['id']])}}">
        <span class="icon icon-edit"></span>
      </a>
</div>
</div>

 		<dl class="row">
			<dt class="col-sm-4">Company Name</dt>
			<dd class="col-sm-8">{{$company['companyname']}}</dd>
			<dt class="col-sm-4">Company Number</dt>
			<dd class="col-sm-8">{{$company['companynumber']}}</dd>
			<dt class="col-sm-4">Address Line 1</dt>
			<dd class="col-sm-8">{{$company['address1']}}</dd>
			<dt class="col-sm-4">Address Line 2</dt>
			<dd class="col-sm-8">{{$company['address2']}}</dd>
			<dt class="col-sm-4">City</dt>
			<dd class="col-sm-8">{{$company['city']}}</dd>
			<dt class="col-sm-4">County</dt>
			<dd class="col-sm-8">{{$company['county']}}</dd>
			<dt class="col-sm-4">Postcode</dt>
			<dd class="col-sm-8">{{$company['postcode']}}</dd>
			<dt class="col-sm-4">Country</dt>
			<dd class="col-sm-8">{{$company['country']}}</dd>
			<dt class="col-sm-4">Work Email</dt>
			<dd class="col-sm-8">{{$company['workemail']}}</dd>
			<dt class="col-sm-4">Work phone</dt>
			<dd class="col-sm-8">{{$company['workphone']}}</dd>
			@include('admin.layouts.timestamps',['item'=>$company])
			@include('admin.layouts.userstamps',['item'=>$company])
		</dl>

 
</div>
@endsection
