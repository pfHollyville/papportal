@extends('admin.layouts.app')

@section('title','System Alerts')

@section('content')
<div class="btn-group">
	  <a title="Add new alert" type="button" class="btn btn-outline-primary" href="{{route('admin.alerts.create')}}">
		<span class="icon icon-pencil"></span>
	  </a>
</div>

<div class="container">
		<div class="dashhead-toolbar">
		</div>
</div>

 <div class="col-md-12">
 
 
 {{ $alerts->links() }}

	<div class="table-responsive">
	<table id="alertTable" class="table" data-sort="table">
		<thead>
			<tr>
				<th class="header">View</th>
				<th class="header">Subject</th>
				<th class="header">Body</th>
				<th class="header">Date</th>
				<th class="header">User</th>
			</tr>
		</thead>
	<tbody>
	@foreach($alerts as $alert)
	<tr>
		<td><a href="{{ route('admin.alerts.show',['id'=>$alert->id])}}">View</a></td>
		<td>{{$alert->subject}}</td>
		<td>{{$alert->body}}</td>
		<td>{{$alert->updated_at}}</td>
		<td>{{$alert->createdby->fullName()}}</td>
	</tr>
	@endforeach
	</tbody>
	</table>
	</div>

	{{ $alerts->links() }}
</div>
@endsection
@section('addscripts')
<script>
	$(document).ready(function() 
		{ 
			$("#alertTable").tablesorter(); 
		} 
	); 

</script>
@endsection
