@extends('admin.layouts.app')

@section('title','Edit Alert')

@section('content')
  <div class="col-md-12">

@if(count($errors)>0)
	<div class="alert alert-warning">Sorry but there are errors with this alert; please correct them and ty to save again</div>
@endif

	<form id="create_note" class="form" action="{{ route('admin.notes.update',['id' => $note->id]) }}" method="post" role="form" style="display: block;">
		    {{ csrf_field() }}
			{{ method_field('PUT') }}
		<div class="form-group @if($errors->has('company_id')) has-warning @endif">
			<label class="form-control-label col-sm-4" for="company_id">Company:</label>
			<div class="col-sm-8 input-group">
			<select name="company_id" id="company_id" tabindex="3" class="form-control" >
				@foreach($companies as $company)
				<option value="{{$company->id}}" 
					@if ($note->company_id == $company->id)
						selected 
					@endif 
				>{{$company->companyname}}</option>
				@endforeach
			</select>
			</div>
		</div>
		<br/>
		<div class="col-sm-3 offset-sm-6">
				<input type="submit" name="editbasiccategorymaster-submit" id="editbasiccategorymaster-submit" tabindex="4" class="btn btn-login" value="Save">
		</div>
	</form>

  
  </div>
@endsection
