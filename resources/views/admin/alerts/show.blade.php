@extends('admin.layouts.app')

@section('title','Alert')

@section('content')
 <div class="col-md-12">
 <div class="btn-group">
      <a title="Alerts List" type="button" class="btn btn-outline-primary" href="{{route('admin.alerts.index')}}">
        <span class="icon icon-list"></span>
      </a>
</div>

 		<dl class="row">
			<dt class="col-sm-4">Subject</dt>
			<dd class="col-sm-8">{{$alert->subject}}</dd>
			<dt class="col-sm-4">Body</dt>
			<dd class="col-sm-8">{{$alert->body}}</dd>
			<dt class="col-sm-4">Date</dt>
			<dd class="col-sm-8">{{$alert->updated_at}}</dd>
			<dt class="col-sm-4">User</dt>
			<dd class="col-sm-8">{{$alert->createdby->fullName()}}</dd>
		</dl>
</div>
@endsection
