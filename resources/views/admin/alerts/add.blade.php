@extends('admin.layouts.app')

@section('title','Add New Alert')

@section('content')
  <div class="col-md-12">
	<h3>Create new Alert</h3>

@if(count($errors)>0)
	<div class="alert alert-warning">Sorry but there are errors with this alert; please correct them and ty to save again</div>
@endif
 <div class="btn-group">
      <a title="Alert List" type="button" class="btn btn-outline-primary" href="{{route('admin.alerts.index')}}">
        <span class="icon icon-list"></span>
      </a>
</div>

	<form id="create_alert" class="form" action="{{ route('admin.alerts.store') }}" method="post" role="form" style="display: block;">
		    {{ csrf_field() }}
		<div class="form-group @if($errors->has('subject')) has-warning @endif">
			<label class="form-control-label col-sm-4" for="subject">Subject:</label>
			<div class="input-group col-sm-8">
				<input type="text" name="subject" id="subject" tabindex="1" class="form-control" placeholder="my alert" value="{{ old('subject') }}" required>
			</div>
			@if($errors->has('subject'))
					@foreach ($errors->get('alert') as $subject)
						<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$subject}}</div>
					@endforeach
			@endif
		</div>
		<div class="form-group @if($errors->has('subject')) has-warning @endif">
			<label class="form-control-label col-sm-4" for="subject">Body:</label>
			<div class="input-group col-sm-8">
				<input type="text" name="body" id="body" tabindex="1" class="form-control" placeholder="my alert" value="{{ old('body') }}" required>
			</div>
			@if($errors->has('body'))
					@foreach ($errors->get('alert') as $body)
						<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$body}}</div>
					@endforeach
			@endif
		</div>
		<br/>
		<div class="col-sm-3 offset-sm-6">
			<input type="submit" name="addalert-submit" id="addalert-submit" tabindex="4" class="btn btn-login" value="Save">
		</div>
	</form>

  
  </div>
@endsection
