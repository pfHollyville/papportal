@extends('admin.layouts.app')

@section('title','Bills - view bill')

@section('content')
<div id="bill" class="container">

	<div v-cloak v-if="bill.company != null">	
		<div  v-cloak>Company:  @{{ bill.company.companyname}}</div>
		<div  v-cloak>Invoice:  @{{ bill.invoicenumber}}</div>
		<div  v-cloak>Billing peroid: from @{{ bill.start_date}} to @{{ bill.end_date}}</div>
	</div>

	<table class="table">
		<thead>
			<th>Service Description</th>
			<th class="text-right">Unit Cost</th>
			<th class="text-right">Quantity</th>
			<th class="text-right">Line Total</th>
		</thead>
		<tbody>
			<tr v-cloak v-for="lineitem in bill.xerolineitems">
				<td><p class="form-control-static">@{{lineitem.description}}</p></td>
				<td class="text-right"><p class="form-control-static">@{{lineitem.unitamount}}</p></td>
				<td class="text-right"><p class="form-control-static">@{{lineitem.quantity}}</p></td>
				<td class="text-right"><p class="form-control-static">@{{lineitem.total}}</p></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td  class="text-right">Bill Total</td>
				<td  class="text-right" v-cloak>@{{bill.total}}</td>
			</tr>
		</tbody>
	</table>  
	
</div>
@endsection
@section('addscripts')
<script src="{{ asset('js/vue.js') }}"></script>
<script>


    var vm = new Vue({
          el: '#bill',
          data: {
			bill: {!! $bill !!}
          }
    });

 
</script>
@endsection
