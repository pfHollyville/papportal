@extends('admin.layouts.app')

@section('title','Bills')

@section('content')
<div class="container">
		<div class="toolbar">
			<div class="btn-group dashhead-toolbar-item">
				  <a title="Create Bills" type="button" class="btn btn-outline-primary" href="{{route('admin.bills.createallbills')}}">
					<span class="icon icon-add-to-list"></span> Create Bills
				  </a>
			</div>
			<form id="company_filter" class="form form-navbar" action="{{ route('admin.bills.filter') }}" method="post" role="form" style="display: block;">
					{{ csrf_field() }}
			<div class="dashhead-toolbar-item form-group">		
				<select name="fcompany" id="fcompany" class="form-control">
					<option value="0">Any Company</option>
					@foreach($allCompanies as $company)
					<option value="{{$company->id}}"  
						@if($filter['fcompany'] == $company->id) 
							selected
						@endif 
					>{{$company->getRef()}} - {{$company->companyname}}</option>
					@endforeach
				</select>
			</div>
			<div class="dashhead-toolbar-item form-group">		
			</div>
			<div class="btn-group dashhead-toolbar-item">
					<button type="submit" name="filtercompany-submit" id="filtercompany-submit" tabindex="4" class="btn btn-outline-primary"> 
					<span class="icon icon-funnel"></span> Filter</button>
			</div>
			</form>		
		</div>
</div>



<div class="col-md-12">
<div class="row">
	<div class="col-sm-8">
	 {{ $bills->links() }}
	</div>
</div>
<div class="row">
	<div>
	<table id="billTable" class="table table-responsive" data-sort="table">
		<thead>
			<tr>
				<th class="header">View Bill</th>
				<th class="header">Company Name</th>
				<th class="header">Start Date</th>
				<th class="header">End Date</th>
				<th class="header">Status</th>
				<th class="header">Total</th>
				<th class="header">Line Items</th>
			</tr>
		</thead>
	<tbody>
	@foreach($bills as $bill)
	<tr>
		<td><a href="{{route('admin.bills.show',['id'=>$bill['id']])}}">{{$bill['id']}}</a></td>
		<td>{{$bill->company->companyname}}</td>
		<td>{{$bill->start_date}}</td>
		<td>{{$bill->end_date}}</td>
		<td>{{$bill->status}}</td>
		<td>{{$bill->total}}</td>
		<td>{{$bill->xerolineitems->count()}}</td>
	</tr>
	@endforeach
	</tbody>
	</table>
	</div>
</div>
<div class="row">
	<div class="offset-col-8 col-sm-4">
	{{ $bills->links() }}
	</div>
</div>
</div>




@endsection
@section('addscripts')
@endsection
