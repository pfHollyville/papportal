@extends('admin.layouts.app')

@section('title','Bills - create new bill')

@section('content')
<div id="lineitems" class="container">

@if(count($errors)>0)
	<div class="alert alert-warning">Sorry but there are errors with this bill; please correct them and ty to save again</div>
@endif

	<div>
		<form class="form form-inline" @submit.prevent="getServices()">
			<label for="company">Company:</label>
			<select name="company" v-model="billdetails.company_id">
				<template v-for="company in companies">
					<option v-bind:value="company.id">@{{ company.companyname}}</option>
				</template>	
			</select>
			<label for="start_date">Start:</label>
			<datepicker v-model="billdetails.start_date" ></datepicker>
			<label for="end_date">End:</label>
			<datepicker v-model="billdetails.end_date" ></datepicker>
			<button type="submit" class="btn btn-sm  btn-primary" ><span class="icon icon-funnel"></span> Find Services</button>
		</form>
	</div>

	<div v-cloak v-if="billdetails.company != null">	
		<div  v-cloak>@{{ billdetails.company.companyname}}</div>
		<div  v-cloak>@{{ billdetails.start_date}}</div>
		<div  v-cloak>@{{ billdetails.end_date}}</div>
	</div>

	<div c-cloak v-if="lineitems">
	<form class="form" @submit.prevent="saveBill()">
			<table class="table">
				<thead>
					<th>Service Description</th>
					<th>Account Code</th>
					<th>Unit Amount</th>
					<th>Inclusive Qty</th>
					<th>Quantity</th>
					<th>Discount</th>
					<th>Total</th>
				</thead>
				<tbody>
		<template v-for="lineitem in lineitems">
				<tr>
					<td><p class="form-control-static">@{{lineitem.description}}</p></td>
					<td><p class="form-control-static">@{{lineitem.accountcode}}</p></td>
					<td><p class="form-control-static">@{{lineitem.unitamount}}</p></td>
					<td><p class="form-control-static">@{{lineitem.maxusage}}</p></td>
					<td><input class="form-control" type="number" 
							v-model.number="lineitem.quantity" 
							v-on:blur="updateQuantity(lineitem)"
							v-bind:readonly="lineitem.maxusage == null" 
							>
					</td>
					<td><input class="form-control" type="number" min="0" max="100"
							v-model.number="lineitem.discount"
 							v-on:blur="updateDiscount(lineitem)"
							>
					</td>
					<td><input type="text" class="form-control" v-model.number="lineitem.total" type="number" readonly></td>
				</tr>
		</template>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Bill Total</td>
					<td v-cloak>@{{totalBill}}</td>
				</tr>
				</tbody>
			</table>  
		<div class="row">	
			<div class="col-sm-3 offset-sm-6">
				<button type="submit" class="btn btn-sm  btn-primary" ><span class="glyphicon glyphicon-save"></span> Save</button>
			</div>
		</div>
	</form>
	</div>
	
</div>
@endsection
@section('addscripts')
<script src="{{ asset('js/vue.js') }}"></script>
<script>
toastr.options = {
  "positionClass": "toast-bottom-right",
}


    var vm = new Vue({
          el: '#lineitems',
          data: {
			companies: {!! $companies !!},
			billdetails: {
				company_id: 0,
				start_date: Moment().subtract(1,'months').startOf('month').format('YYYY-MM-DD'),
				end_date: Moment().subtract(1,'months').endOf('month').format('YYYY-MM-DD'),
				company: null
			},
			lineitems: null,
			formerrors: null,
          },
			computed: {
			  totalBill: function(){
				let sumLines = 0;
					for(let i = 0; i < this.lineitems.length; i++){
					  sumLines += parseFloat(this.lineitems[i].total);
					}
					return sumLines.toFixed(2);
				}
			},
		  methods: {
			getServices: function() {
				var _this = this;
				this.billdetails.start_date = Moment(this.billdetails.start_date).format('YYYY-MM-DD');
				this.billdetails.end_date = Moment(this.billdetails.end_date).format('YYYY-MM-DD');
				axios.post('{{ route("admin.bills.getservices") }}',this.billdetails)
					.then(function (response) {
						_this.lineitems = response.data.xerolineitems;
						_this.billdetails = response.data.billdetails;
					})
					.catch(function (error)	{
					});
			},
			updateQuantity: function(li) {
				if(li.quantity > li.maxusage) {
					total = ((li.quantity - li.maxusage) * li.unitamount);
				} else {
					total = 0;
				}
				li.total = total.toFixed(2);
			},
			updateDiscount: function(li) {
				if (li.quantity > li.maxusage) {
					qtyCharged = li.quantity - li.maxusage;
				} else {
					qtyCharged = 0;
				}
				total = ((qtyCharged * li.unitamount) * (100 - li.discount)/100);
				li.total = total.toFixed(2);
			},
			saveBill: function() {
				this.billdetails.start_date = Moment(this.billdetails.start_date).format('YYYY-MM-DD');
				this.billdetails.end_date = Moment(this.billdetails.end_date).format('YYYY-MM-DD');
				axios.post('{{ route("admin.bills.store") }}',{'xerolineitems': this.lineitems,'billdetails': this.billdetails,'total':this.totalBill})
					.then(function (response) {
						toastr.success('Bill saved','Success!');
					})
					.catch(function (error)	{
						toastr.error('Bill not saved','Oops!');
						console.log(error);
					});
			  }		  
		},
		components: {
				'datepicker': Datepicker
		},
    });

 
</script>
@endsection
