@extends('admin.layouts.app')

@section('title','Bills - create new bills')

@section('content')
<div id="bills" class="container">

@if(count($errors)>0)
	<div class="alert alert-warning">Sorry but there are errors with these bill; please correct them and ty to save again</div>
@endif

	<div>
		<form class="form form-inline" @submit.prevent="createBills()">
			<label for="selected_year">Year:</label>
			<select v-model="selected_year">
				<option v-for="year in years" v-bind:value="year"  :selected="year == selected_year">@{{year}}</option>
			</select>
			<label for="selected_month">Month:</label>
			<select v-model="selected_month">
				<option v-for="(month, index) in months" v-bind:value="index + 1"   :selected="index + 1 == selected_month">@{{month}}</option>
			</select>
			
			<button type="submit" class="btn btn-sm  btn-primary" ><span class="icon icon-save"></span> Create Bills</button>
		</form>
	</div>

	<div v-show="loading">
		<span class="glyphicon glyphicon-repeat fast-right-spinner"></span> Creating Bill, please wait......
	</div>
	
	<div id="download" v-show="download">
		<a v-bind:href="downloadUrl" class="btn btn-primary">Download CSV<a>
	</div>
	
</div>
@endsection
@section('addscripts')
<script src="{{ asset('js/vue.js') }}"></script>
<script>
toastr.options = {
  "positionClass": "toast-bottom-right",
}


    var vm = new Vue({
		el: '#bills',
		data: {
			years: [2017,2018,2019,2020],
			months: ["January","February","March","April","May","June","July","August","September","October","November","December"],
			selected_year: parseInt(Moment().subtract(1,'months').startOf('month').format('YYYY')),
			selected_month: parseInt(Moment().subtract(1,'months').startOf('month').format('M')),
			formerrors: null,
			download: false,
			loading: false
		},
		computed: {
			downloadUrl: function() {
				return "{{url('/admin/bills/exportbills')}}/" + this.selected_year + "/" + this.selected_month
			}
		},
		methods: {
			createBills: function() {
				var _this = this;
				this.download = false;
				this.loading = true;
				axios.get("{{url('/admin/bills/saveallbills')}}/" + this.selected_year + "/" + this.selected_month)
					.then(function (response) {
						_this.download = true;
						_this.loading = false;
						if (response.data == 1) {
							toastr.success('Bills already exist','Success!');
						}
						if (response.data == 2) {
							toastr.success('Bills created','Success!');
						}
					})
					.catch(function (error)	{
						toastr.error('Bills not saved','Oops!');
						console.log(error);
						_this.loading = false;
						_this.download = false;
					});
			  }		  
		},
	});

 
</script>
@endsection
