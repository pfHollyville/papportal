@extends('admin.layouts.app')

@section('title','Company Management')

@section('content')

<div class="container">
		<div class="toolbar">
			<div class="btn-group dashhead-toolbar-item">
				  <a title="Add New Company" type="button" class="btn btn-outline-primary" href="{{route('admin.companies.create')}}">
					<span class="icon icon-add-to-list"></span> New Company
				  </a>
			</div>
			<form id="company_filter" class="form form-navbar" action="{{ route('admin.companies.filter') }}" method="post" role="form" style="display: block;">
					{{ csrf_field() }}
			<div class="dashhead-toolbar-item form-group">		
				<select name="fcompany" id="fcompany" class="form-control">
					<option value="0">Any Company</option>
					@foreach($allCompanies as $company)
					<option value="{{$company->id}}"  
						@if($filter['fcompany'] == $company->id) 
							selected
						@endif 
					>{{$company->getRef()}} - {{$company->companyname}}</option>
					@endforeach
				</select>
			</div>
			<div class="dashhead-toolbar-item form-group">		
				<select name="findustry" id="findustry" class="form-control">
					<option value="0">Any Industry</option>
					@foreach($industries as $industry)
						<option value="{{$industry->id}}"  @if($filter['findustry'] == $industry->id) selected @endif   >{{$industry->name}}</option>
					@endforeach
				</select>
			</div>
			<div class="btn-group dashhead-toolbar-item">
					<button type="submit" name="filtercompany-submit" id="filtercompany-submit" tabindex="4" class="btn btn-outline-primary"> 
					<span class="icon icon-funnel"></span> Filter</button>
			</div>
			</form>		
		</div>
</div>



<div class="col-md-12">
<div class="row">
	<div class="col-sm-8">
	 {{ $companies->links() }}
	</div>
</div>
<div class="row">
	<div>
	<table id="companyTable" class="table table-responsive" data-sort="table">
		<thead>
			<tr>
				<th class="header">Edit Client</th>
				<th class="header">Client Name</th>
				<th class="header">Edit Company</th>
				<th class="header">Company Name</th>
				<th class="header">Company Services</th>
				<th class="header">Industry</th>
				<th class="header">Legal Status</th>
				<th class="header">Usage Reason</th>
			</tr>
		</thead>
	<tbody>
	@foreach($companies as $company)
	<tr>
		<td class="text-left"><a title="Edit User" href="{{route('admin.users.edit',['id' => $company['user']['id']])}}"><span class="icon icon-edit"></a></td>
		<td><a href="{{route('admin.users.show',['id'=>$company['user']['id']])}}">{{$company['user']['first_name']}}&nbsp;{{$company['user']['last_name']}}</a></td>
		<td class="text-left"><a title="Edit Company" href="{{route('admin.companies.edit',['id' => $company['id']])}}"><span class="icon icon-edit"></a></td>
		<td><a href="{{route('admin.companies.show',['id'=>$company['id']])}}">{{$company->getRef()}} - {{$company['companyname']}}</a></td>
		<td>{{$company->companyservices()->count()}}</td>
		<td>{{$company['industry']['name']}}</td>
		<td>{{$company['legalstatus']['name']}}</td>
		<td>{{$company['usagereason']['name']}}</td>
	</tr>
	@endforeach
	</tbody>
	</table>
	</div>
</div>
<div class="row">
	<div class="offset-col-8 col-sm-4">
	{{ $companies->links() }}
	</div>
</div>
</div>
@endsection

@section('addscripts')
<script>
	$(document).ready(function() 
		{ 
			$("#companyTable").tablesorter(); 
		} 
	); 

</script>
@endsection
