@extends('admin.layouts.app')

@section('title','Companies - Add New Company')

@section('content')
  <div class="col-md-12">
	<h3>Create new company</h3>

@if(count($errors)>0)
	<div class="alert alert-warning">Sorry but there are errors with this company; please correct them and ty to save again</div>
@endif

	<form id="create_company" class="form" action="{{ route('admin.companies.store') }}" method="post" role="form" style="display: block;">
		    {{ csrf_field() }}
			<div class="form-group @if($errors->has('user_id')) has-warning @endif">
				<label class="form-control-label col-sm-4"  for="user_id">User:</label>
				<div class="input-group col-sm-8 ">
					<select name="user_id" id="user_id" tabindex="3" class="form-control @if($errors->has('user_id')) form-control-warning @endif" >
						@foreach($lookups['users'] as $user)
							<option value="{{$user['id']}}" 
								@if (old('user_id') == $user['id']) ? selected @endif 
							>{{ $user['first_name']}} {{ $user['last_name']}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group @if($errors->has('companyname')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="companyname">Company Name:</label>
				<div class="input-group col-sm-8">
					<input type="text" name="companyname" id="companyname" tabindex="1" class="form-control @if($errors->has('companyname')) form-control-warning @endif" placeholder="Company Name" value="{{ old('company_name') }}" required>
				</div>
				@if($errors->has('companyname'))
						@foreach ($errors->get('companyname') as $message)
							<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
						@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('companynumber')) has-warning @endif">
				<label class="form-control-label col-sm-4"  for="companynumber">Company Number:</label>
				<div class="input-group col-sm-8 form-inline">
					<input type="text" name="companynumber" id="companynumber" tabindex="2" class="form-control @if($errors->has('companynumber')) form-control-warning @endif" placeholder="012345678" value="{{ old('companynumber') }}" >
				</div>
					@if($errors->has('companynumber'))
							@foreach ($errors->get('companynumber') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
					@endif
			</div>
			<div class="form-group @if($errors->has('address1')) has-warning @endif">
				<label class="form-control-label col-sm-4"  for="address1">Address Line 1:</label>
				<div class="input-group col-sm-8 form-inline">
					<input type="text" name="address1" id="address1" tabindex="3" class="form-control @if($errors->has('address1')) form-control-warning @endif" placeholder="house name or number" value="">
				</div>
			</div>
			<div class="form-group @if($errors->has('address2')) has-warning @endif">
				<label class="form-control-label col-sm-4"  for="address2">Address Line 2:</label>
				<div class="input-group col-sm-8 form-inline">
					<input type="text" name="address2" id="address2" tabindex="3" class="form-control @if($errors->has('address1')) form-control-warning @endif" placeholder="street name" value="">
				</div>
			</div>
			<div class="form-group @if($errors->has('city')) has-warning @endif">

			<label class="form-control-label col-sm-4"  for="city">City:</label>
			<div class="input-group col-sm-8 form-inline">
				<input type="text" name="city" id="city" tabindex="3" class="form-control @if($errors->has('address2')) form-control-warning @endif" placeholder="city" value="">
			</div>
			</div>
			<div class="form-group @if($errors->has('county')) has-warning @endif">
			<label class="form-control-label col-sm-4"  for="county">County:</label>
			<div class="input-group col-sm-8 form-inline">
				<input type="text" name="county" id="county" tabindex="3" class="form-control @if($errors->has('address2')) form-control-warning @endif" placeholder="county" value="">
			</div>
			</div>
			<div class="form-group @if($errors->has('postcode')) has-warning @endif">
			<label class="form-control-label col-sm-4"  for="postcode">Postcode:</label>
			<div class="input-group col-sm-8 form-inline">
				<input type="text" name="postcode" id="postcode" tabindex="3" class="form-control @if($errors->has('postcode')) form-control-warning @endif" placeholder="AB1 10XY" value="">
			</div>
			</div>
			<div class="form-group @if($errors->has('country')) has-warning @endif">
			<label class="form-control-label col-sm-4"  for="country">Country:</label>
			<div class="input-group col-sm-8 form-inline">
				<input type="text" name="country" id="country" tabindex="3" class="form-control @if($errors->has('country')) form-control-warning @endif" placeholder="" value="UK">
			</div>
			</div>
			<div class="form-group @if($errors->has('workemail')) has-warning @endif">
			<label class="form-control-label col-sm-4"  for="workemail">Work EMail:</label>
			<div class="input-group col-sm-8 ">
				<input type="text" name="workemail" id="workemail" tabindex="3" class="form-control @if($errors->has('workemail')) form-control-warning @endif" placeholder="emal@work.com" value="{{ old('workemail') }}">
			</div>
			@if($errors->has('workemail'))
					@foreach ($errors->get('workemail') as $message)
						<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
					@endforeach
			@endif
			</div>
			<div class="form-group @if($errors->has('workphone')) has-warning @endif">
			<label class="form-control-label col-sm-4"  for="workphoe">Work Phone:</label>
			<div class="input-group col-sm-8 ">
				<input type="text" name="workphone" id="workphone" tabindex="3" class="form-control @if($errors->has('workphone')) form-control-warning @endif" placeholder="0207123456" value="{{ old('workphone') }}">
			</div>
			</div>
			<div class="form-group @if($errors->has('industry_id')) has-warning @endif">
			<label class="form-control-label col-sm-4"  for="industry_id">Industry:</label>
			<div class="input-group col-sm-8 ">
				<select name="industry_id" id="industry_id" tabindex="3" class="form-control @if($errors->has('industry')) form-control-warning @endif" >
					@foreach($lookups['industries'] as $industry)
						<option value="{{$industry['id']}}" 
							@if (old('industry_id') == $industry['id']) ? selected @endif 
						>{{ $industry['name']}}</option>
					@endforeach
				</select>
			</div>
			</div>
			<div class="form-group @if($errors->has('legalstatus_id')) has-warning @endif">
				<label class="form-control-label col-sm-4"  for="legalstatus_id">Legal Status:</label>
				<div class="input-group col-sm-8 ">
					<select name="legalstatus_id" id="legalstatus_id" tabindex="3" class="form-control @if($errors->has('legalstatus')) form-control-warning @endif" >
						@foreach($lookups['legalstatuses'] as $legalstatus)
							<option value="{{$legalstatus['id']}}" 
								@if (old('legalstatus_id') == $legalstatus['id']) ? selected @endif 
							>{{ $legalstatus['name']}}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group @if($errors->has('usagereason_id')) has-warning @endif">
			<label class="form-control-label col-sm-4"  for="usagereason_id">Usage Reason:</label>
			<div class="input-group col-sm-8 form-inline">
				<select name="usagereason_id" id="usagereason_id" tabindex="3" class="form-control @if($errors->has('usagereason')) form-control-warning @endif" >
					@foreach($lookups['usagereasons'] as $usagereason)
						<option value="{{$usagereason['id']}}" 
							@if (old('usagereason_id') == $usagereason['id']) ? selected @endif 
						>{{ $usagereason['name']}}</option>
					@endforeach
				</select>
			</div>
			</div>
			<div class="form-group @if($errors->has('xero_id')) has-warning @endif">
				<label class="col-sm-4 text-left"  for="xero_id">Xero Contact ID:</label>
				<div class="input-group col-sm-8 form-inline required">
					<input type="text" name="xero_id" id="xero_id" tabindex="3" class="form-control" placeholder="contact guid" value="{{ old('xero_id') }}">
				</div>
				@if($errors->has('xero_id'))
						@foreach ($errors->get('xero_id') as $message)
							<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
						@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('webvisitorspermonth')) has-warning @endif">
				<label class="col-sm-4 text-left"  for="webvisitorspermonth">Web Visitors Per Month:</label>
				<div class="input-group col-sm-8 form-inline required">
					<input type="text" name="webvisitorspermonth" id="webvisitorspermonth" tabindex="3" class="form-control" placeholder="contact guid" value="{{ old('webvisitorspermonth') }}">
				</div>
				@if($errors->has('webvisitorspermonth'))
						@foreach ($errors->get('webvisitorspermonth') as $message)
							<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
						@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('callspermonth')) has-warning @endif">
				<label class="col-sm-4 text-left"  for="callspermonth">Call Volume Per Month:</label>
				<div class="input-group col-sm-8 form-inline required">
					<input type="text" name="callspermonth" id="callspermonth" tabindex="3" class="form-control" placeholder="contact guid" value="{{ old('callspermonth') }}">
				</div>
				@if($errors->has('callspermonth'))
						@foreach ($errors->get('callspermonth') as $message)
							<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
						@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('averagesale')) has-warning @endif">
				<label class="col-sm-4 text-left"  for="averagesale">Average Sale Value:</label>
				<div class="input-group col-sm-8 form-inline required">
					<input type="text" name="averagesale" id="averagesale" tabindex="3" class="form-control" placeholder="contact guid" value="{{ old('averagesale') }}">
				</div>
				@if($errors->has('averagesale'))
						@foreach ($errors->get('averagesale') as $message)
							<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
						@endforeach
				@endif
			</div>
			
		<br/>
		<div class="row">	
			<div class="col-sm-3 offset-sm-6">
				<input type="submit" name="addclient-submit" id="addclient-submit" tabindex="4" class="btn btn-login" value="Save">
			</div>
		</div>
	</form>

  
  </div>
@endsection
