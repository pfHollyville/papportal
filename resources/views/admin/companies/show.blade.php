@extends('admin.layouts.app')

@section('title','Company Details')

@section('content')
<div id="company" class="col-md-12">




	<div class="row">
		<div class="btn-group">
			  <a title="Company List" type="button" class="btn btn-outline-primary" href="{{route('admin.companies.index')}}">
				<span class="icon icon-list"></span>
			  </a>
			  <a title="Edit" type="button" class="btn btn-outline-primary" href="{{route('admin.companies.edit',['id'=>$company['id']])}}">
				<span class="icon icon-edit"></span>
			  </a>
		</div>
	</div>

	  <div class="panel panel-default">
			<div class="panel-heading">
			  <h4 class="panel-title">
				Company Details: {{str_pad($company->id,4,0,STR_PAD_LEFT)}} - {{$company->companyname}}
				<a data-toggle="collapse" href="#company"><span class="icon icon-chevron-down"></span></a>
			  </h4>
			</div>
			<div id="company" class="panel-collapse collapse in">
			  <div class="panel-body">
				<div class="row">
					<div class="col-sm-6">
						<address>
							{{$company->address1}}<br/>
							{{$company->address2}}<br/>
							{{$company->city}}, {{$company->county}}<br/>
							{{$company->postcode}}, {{$company->country}}
						</address>
						<address>
							<span class="icon icon-email"></span> {{$company->workemail}}
							<span class="icon icon-phone"></span> {{$company->workphone}}
						</address>
					</div>
					<div class="col-sm-6">
						<div>Company Number: {{$company->companynumber}}</div>
						<div>Xero ID: {{$company->xero_id}}</div>
						<div>Industry: {{$company->industry->name}}</div>
						<div>Usage Reason: {{$company->usagereason->name}}</div>
						<div>Services
							<ul>
								<li>Web Visitors: {{$company->webvisitorspermonth}}</li>
								<li>Call Volume per month: {{$company->callspermonth}}</li>
								<li>Average Sale: {{$company->averagesale}}</li>
							</ul>
						</div>
					</div>
				</div>
			  </div>
				<div class="panel-footer">
					Client: {{$company->user->first_name}} {{$company->user->last_name}}
				</div>
		   </div>
	  </div>
	  

	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			Company Services <span class="badge badge-info">{{$company->companyservices->count()}}</span>
			<a data-toggle="collapse" href="#companyservices"><span class="icon icon-chevron-down"></span></a>
		  </h4>
		</div>
		<div id="companyservices" class="panel-collapse collapse">
		  <div class="panel-body">
			<ul class="list-unstyled">
			@foreach($company->companyservices as $companyservice)
				<li class="">
					<div class="panel">
						<div class="panel-heading">
							<h4 class="panel-title">
								{{$companyservice->service->name}} 
								@if($companyservice->widget_id)
								<button type="button" class="btn btn-info" onclick="showCode('{{$companyservice->widget_id}}')">Show code</button>
								@endif
								@if($companyservice->webchats->count() > 0)
									<span class="badge badge-default">Webchats {{$companyservice->webchats->count()}}</span>
									<a data-toggle="collapse" href="#companyservices-{{$companyservice->id}}"><span class="icon icon-chevron-down"></span></a>
								@endif	
								@if($companyservice->queues->count() > 0)
									<span class="badge badge-default">Queues {{$companyservice->queues->count()}}</span>
									<a data-toggle="collapse" href="#companyservices-{{$companyservice->id}}"><span class="icon icon-chevron-down"></span></a>
								@endif	
							</h4>
						</div>
						@if($companyservice->webchats->count() > 0)
						<div id="companyservices-{{$companyservice->id}}" class="panel-collapse collapse">
							<div class="panel-body">
									<ul class="list-unstyled">
										@foreach($companyservice->webchats as $webchat)
											<li>Requester: {{$webchat->requester_details_name}}, Date: {{$webchat->created_at_date}}</li>
										@endforeach
									</ul>
							</div>
						 </div>
						@endif
						
						@if($companyservice->queues->count() > 0)
						<div id="companyservices-{{$companyservice->id}}" class="panel-collapse collapse">
							<div class="panel-body">
									<ul class="list-unstyled">
										@foreach($companyservice->queues as $queue)
											<li>
												<div class="well row">	
													<h5 class="col-sm-2">Queue: {{$queue->queueid}} / {{$queue->queuename}}	<span class="label label-default">{{$queue->type}}</span></h5>
													<div class="col-sm-2">Date: {{$queue->date}}</div>
													<div class="col-sm-4">Phone Number(s): {{$queue->phonenumbers}}</div>
													<div class="col-sm-4">Number Presentation: {{$queue->numberpresentation}}</div>
													<div class="col-sm-12">Notes: {{$queue->notes}}</div>
												</div>
											</li>
										@endforeach
									</ul>
							</div>
						 </div>
						@endif
					</div>
				</li>
			@endforeach
			</ul>
		  </div>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			Notes <span class="badge badge-info">@{{ notes.length }}</span>
			<a data-toggle="collapse" href="#notes"><span class="icon icon-chevron-down"></span></a>
			  <button class="btn btn-default" id="show-modal-note" @click="showModalNote = true">Add Note</button>
			  <!-- use the modal component, pass in the prop -->
			  <modal v-if="showModalNote" @close="showModalNote = false" v-bind:note="newnote">
				<!--
				  you can use custom content here to overwrite
				  default content
				-->
				<h3 slot="header">New Note</h3>
			  </modal>
		  </h4>
		</div>
		<div id="notes" class="panel-collapse collapse">
		  <div class="panel-body">
			<ul class="list-unstyled">
				<div v-for="(note,index) in notes" :key="note.id">
					<div class="well row">	
						<h5 class="col-sm-6">Date: @{{ note.updated_at | formatDate }}</h5>
						<div class="col-sm-6">User: @{{ note.updatedby.first_name }} @{{ note.updatedby.last_name }}</div>
						<div class="col-sm-12">Notes: @{{ note.message }}</div>
					</div>
				</div>
			</ul>
		  </div>
		</div>
	</div>

	  <div class="panel panel-default">
		<div class="panel-heading">
		  <h4 class="panel-title">
			Company Questions <span class="badge badge-info">{{$company->questions->count()}}</span>
			<a data-toggle="collapse" href="#questions"><span class="icon icon-chevron-down"></span></a>
		  </h4>
		</div>
		<div id="questions" class="panel-collapse collapse">
		  <div class="panel-body">
			<ul class="list-group">
			@foreach($company->questions as $question)
				<li class="list-group-item">
					<h5 class="list-group-item-heading">Q: {{$question->question}}	<span class="label label-default">{{$question->category}}</span></h5>
					<p class="list-group-item-text">A: {{$question->answer}}</p>
				</li>
			@endforeach
			</ul>
		  </div>
		</div>
	  </div>
	</div>

 
</div>

<div id="webchatCode" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Webchat Code</h4>
      </div>
      <div class="modal-body">
        <p>Insert this code into your web page.</p>
		<pre id="modalCode"></pre>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



@endsection

@section('addscripts')
<script src="{{ asset('js/vue.js') }}"></script>


<script type="text/x-template" id="modal-template-note">
<transition name="modal">
    <div class="modal-mask">
      <div class="modal-wrapper">
        <div class="modal-container">

          <div class="modal-header">
            <slot name="header">
              default header
            </slot>
          </div>

          <div class="modal-body">
            <slot name="body">
				<div v-bind:class="['form-group',{'has-warning': note.formerrors && note.formerrors.message}]">
					<label for="message">Note:</label>
					<textarea rows="2" v-model="note.message" name="message" class="form-control"></textarea>
					<span v-if="note.formerrors && note.formerrors.message" class="help-block">
						@{{ note.formerrors.message[0] }}
					</span>
				</div>
            </slot>
          </div>

          <div class="modal-footer">
            <slot name="footer">
              <button class="btn btn-primary" v-on:click.stop.prevent="saveNote()">
                OK
              </button>
              <button class="btn btn-primary" v-on:click.stop.prevent="cancelNote()">
                Cancel
              </button>
            </slot>
          </div>
        </div>
      </div>
    </div>
 </transition>
</script>

<script>

toastr.options = {
  "positionClass": "toast-bottom-right",
}




Vue.component('modal', {
  template: '#modal-template-note',
  props: ['note'],
  methods: {
	  saveNote: function() {
		var _this = this; 
		axios.post('{{ url('/admin/notes')}}',this.note)
			.then(function (response) {
				_this.$parent.showModalNote = false;
				_this.note.id = response.data;
				_this.note.updated_at = Moment();
				_this.$parent.notes.push(Vue.util.extend({},_this.note));
				_this.note.message = '';
				toastr.success('Your note was added','Success!');
			})
			.catch(function (error)	{
						_this.note.formerrors = error.response.data;
						errors = error.response.data;
						for(var error in errors) { 
							for(i=0;i<errors[error].length;i++) {
								toastr.error(errors[error][i]);
							}
						}
			});
	  },
	  cancelNote: function() {
			this.note.message = '';
			this.note.formerrors = null;
			this.$parent.showModalNote = false;
	  }
  }
});
Vue.filter('formatDate', function(value) {
  if (value) {
    return Moment(String(value)).format('MM/DD/YYYY H:mm');
  }
});

	
    var vm = new Vue({
          el: '#company',
          data: {
			showModalNote: false,
			newnote: {
				id: 0,
				message: '',
				updated_at: '',
				company_id: {!!$company['id']!!},
				formerrors: null,
				updatedby: {
					id: {!! Auth::user()->id !!},
					first_name: '{!! Auth::user()->first_name !!}',
					last_name: '{!! Auth::user()->last_name !!}'
				}
			},
			notes: {!! $company->notes !!}
          },
		  
    });

		
  
 
</script>

<script>

function showCode(webchatid) {
	modalCode = "&lt;!-- begin Handlr code --&gt;\r\n";
	modalCode += "&lt;script type=&quot;text/javascript&quot;&gt;\r\n";
	modalCode += "(function() {\r\n";
	modalCode += "    var se = document.createElement('script'); se.type = &quot;text/javascript&quot;; se.async = true;\r\n";
	modalCode += "    se.src = '//storage.googleapis.com/handlrchat/js/" + webchatid + ".js';\r\n";
	modalCode += "    var done = false;\r\n";
	modalCode += "	  se.onload = se.onreadystatechange = function() {\r\n";
	modalCode += "         if (!done&amp;&amp;(!this.readyState||this.readyState==='loaded'||this.readyState==='complete')) {\r\n";
	modalCode += "				done = true;\r\n";
	modalCode += "         }\r\n";
	modalCode += "    };\r\n";
	modalCode += "	  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(se, s);\r\n";
	modalCode += "})();\r\n";
	modalCode += "&lt;/script&gt;\r\n";
	modalCode += "&lt;!-- end Handlr code --&gt;\r\n";

	$("#modalCode").html(modalCode);
	$("#webchatCode").modal("show");
	
}

</script>


@endsection
