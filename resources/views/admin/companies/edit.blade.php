@extends('admin.layouts.app')

@section('title','Companies - Edit Company')

@section('content')
  <div class="col-md-12">
	<h3>Edit company</h3>

@if(count($errors)>0)
	<div class="alert alert-warning">Sorry but there are errors with this company; please correct them and ty to save again</div>
@endif

	<form id="edit_company" class="form-inline form-horizontal" action="{{ route('admin.companies.update', ['id' => $company['id']]) }}" method="post" role="form" style="display: block;">
		    {{ csrf_field() }}
			{{ method_field('PUT') }}
		<div class="row">
			<label class="col-sm-4 text-left" for="companyname">Company Name:</label>
			<div class="input-group col-sm-8 form-inline required">
				<input type="text" name="companyname" id="companyname" tabindex="1" class="form-control" placeholder="Company Name" value="{{ $company['companyname'] }}" required>
			</div>
			@if($errors->has('companyname'))
					@foreach ($errors->get('companyname') as $message)
						<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
					@endforeach
			@endif
			<label class="col-sm-4 text-left"  for="companynumber">Company Number:</label>
			<div class="input-group col-sm-8 form-inline">
				<input type="text" name="companynumber" id="companynumber" tabindex="2" class="form-control" placeholder="012345678" value="{{ $company['companynumber'] }}
" >
			</div>
			@if($errors->has('companynumber'))
					@foreach ($errors->get('companynumber') as $message)
						<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
					@endforeach
			@endif
			<label class="col-sm-4 text-left"  for="address1">Address Line 1:</label>
			<div class="input-group col-sm-8 form-inline">
				<input type="text" name="address1" id="address1" tabindex="3" class="form-control" placeholder="house name or number" value="{{$company['address1']}}">
			</div>
			<label class="col-sm-4 text-left"  for="address2">Address Line 2:</label>
			<div class="input-group col-sm-8 form-inline">
				<input type="text" name="address2" id="address2" tabindex="3" class="form-control" placeholder="street name" value="{{$company['address2']}}">
			</div>
			<label class="col-sm-4 text-left"  for="city">City:</label>
			<div class="input-group col-sm-8 form-inline">
				<input type="text" name="city" id="city" tabindex="3" class="form-control" placeholder="city" value="{{$company['city']}}">
			</div>
			<label class="col-sm-4 text-left"  for="county">County:</label>
			<div class="input-group col-sm-8 form-inline">
				<input type="text" name="county" id="county" tabindex="3" class="form-control" placeholder="county" value="{{$company['county']}}">
			</div>
			<label class="col-sm-4 text-left"  for="postcode">Postcode:</label>
			<div class="input-group col-sm-8 form-inline">
				<input type="text" name="postcode" id="postcode" tabindex="3" class="form-control" placeholder="AB1 10XY" value="{{$company['postcode']}}">
			</div>
			<label class="col-sm-4 text-left"  for="country">Country:</label>
			<div class="input-group col-sm-8 form-inline">
				<input type="text" name="country" id="country" tabindex="3" class="form-control" placeholder="" value="{{$company['country']}}">
			</div>
			<label class="col-sm-4 text-left"  for="workemail">Work EMail:</label>
			<div class="input-group col-sm-8 form-inline required">
				<input type="text" name="workemail" id="workemail" tabindex="3" class="form-control" placeholder="emal@work.com" value="{{$company['workemail']}}">
			</div>
			@if($errors->has('workemail'))
					@foreach ($errors->get('workemail') as $message)
						<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
					@endforeach
			@endif
			<label class="col-sm-4 text-left"  for="workphoe">Work Phone:</label>
			<div class="input-group col-sm-8 form-inline required">
				<input type="text" name="workphone" id="workphone" tabindex="3" class="form-control" placeholder="0207123456" value="{{$company['workphone']}}">
			</div>
			<label class="col-sm-4 text-left"  for="industry_id">Industry:</label>
			<div class="input-group col-sm-8 form-inline required">
				<select name="industry_id" id="industry_id" tabindex="3" class="form-control" >
					@foreach($lookups['industries'] as $industry)
						<option value="{{$industry['id']}}" 
							@if ($company['industry_id'] == $industry['id']) ? selected @endif 
						>{{ $industry['name']}}</option>
					@endforeach
				</select>
			</div>
			<label class="col-sm-4 text-left"  for="legalstatus_id">Legal Status:</label>
			<div class="input-group col-sm-8 form-inline required">
				<select name="legalstatus_id" id="legalstatus_id" tabindex="3" class="form-control" >
					@foreach($lookups['legalstatuses'] as $legalstatus)
						<option value="{{$legalstatus['id']}}" 
							@if ($company['legalstatus_id'] == $legalstatus['id']) ? selected @endif 
						>{{ $legalstatus['name']}}</option>
					@endforeach
				</select>
			</div>
			<label class="col-sm-4 text-left"  for="usagereason_id">Usage Reason:</label>
			<div class="input-group col-sm-8 form-inline">
				<select name="usagereason_id" id="usagereason_id" tabindex="3" class="form-control" >
					@foreach($lookups['usagereasons'] as $usagereason)
						<option value="{{$usagereason['id']}}" 
							@if ($company['usagereason_id'] == $usagereason['id']) ? selected @endif 
						>{{ $usagereason['name']}}</option>
					@endforeach
				</select>
			</div>
			<label class="col-sm-4 text-left"  for="xero_id">Xero Contact ID:</label>
			<div class="input-group col-sm-8 form-inline required">
				<input type="text" name="xero_id" id="xero_id" tabindex="3" class="form-control" placeholder="contact guid" value="{{$company['xero_id']}}">
			</div>
			@if($errors->has('xero_id'))
					@foreach ($errors->get('xero_id') as $message)
						<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
					@endforeach
			@endif
			<label class="col-sm-4 text-left"  for="webvisitorspermonth">Xero Contact ID:</label>
			<div class="input-group col-sm-8 form-inline required">
				<input type="text" name="webvisitorspermonth" id="webvisitorspermonth" tabindex="3" class="form-control" placeholder="contact guid" value="{{$company['webvisitorspermonth']}}">
			</div>
			@if($errors->has('webvisitorspermonth'))
					@foreach ($errors->get('webvisitorspermonth') as $message)
						<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
					@endforeach
			@endif
			<label class="col-sm-4 text-left"  for="callspermonth">Xero Contact ID:</label>
			<div class="input-group col-sm-8 form-inline required">
				<input type="text" name="callspermonth" id="callspermonth" tabindex="3" class="form-control" placeholder="contact guid" value="{{$company['callspermonth']}}">
			</div>
			@if($errors->has('callspermonth'))
					@foreach ($errors->get('callspermonth') as $message)
						<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
					@endforeach
			@endif
			<label class="col-sm-4 text-left"  for="averagesale">Xero Contact ID:</label>
			<div class="input-group col-sm-8 form-inline required">
				<input type="text" name="averagesale" id="averagesale" tabindex="3" class="form-control" placeholder="contact guid" value="{{$company['averagesale']}}">
			</div>
			@if($errors->has('averagesale'))
					@foreach ($errors->get('averagesale') as $message)
						<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
					@endforeach
			@endif
			
			
		</div>
		<br/>
		<div class="row">	
			<div class="col-sm-3 offset-sm-6">
				<input type="submit" name="addclient-submit" id="addclient-submit" tabindex="4" class="btn btn-login" value="Save">
			</div>
		</div>
	</form>

  
  </div>
@endsection
