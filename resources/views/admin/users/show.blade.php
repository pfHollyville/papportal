@extends('admin.layouts.app')

@section('title','User Details')

@section('content')
 <div class="col-md-12">
	<div class="table-responsive">
	<table id="usertable" class="table" data-sort="table">
		<thead>
			<tr>
				<th class="header">First Name</th>
				<th class="header">Last Name</th>
				<th class="header">EMail</th>
				<th class="header">Role</th>
				<th class="header">Account Status</th>
				<th class="header">Company</th>
				<th class="header">Created</th>
				<th class="header">Update</th>
			</tr>
		</thead>
	<tbody>
	<tr>
		<td>{{$user['first_name']}}</td>
		<td>{{$user['last_name']}}</td>
		<td>{{$user['email']}}</td>
		<td>{{$user['role']}}</td>
		<td>{{$user['account_status']}}</td>
		<td>{{$user['company']['companyname']}}</td>
		<td>{{$user['created_at']}}</td>
		<td>{{$user['updated_at']}}</td>
	</tr>
	</tbody>
	</table>
	</div>
</div>
@endsection
