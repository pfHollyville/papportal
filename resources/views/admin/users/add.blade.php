@extends('admin.layouts.app')

@section('title','User Management - Add New User')

@section('content')
  <div class="col-md-12">
	<h3>Create new user account</h3>

@if(count($errors)>0)
	<div class="form-group has-warning">
		<div class="form-control-label">Sorry but there are errors with this user; please correct them and ty to save again</div>
	</div>
@endif

	<form id="create_users" class="form" action="{{ route('admin.users.store') }}" method="post" role="form" style="display: block;">
		    {{ csrf_field() }}
			<div class="form-group @if($errors->has('first_name')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="first_name">First Name:</label>
				<div class="input-group col-sm-8">
				  <input type="text" name="first_name" class="form-control @if($errors->has('first_name')) form-control-warning @endif" id="first_name" placeholder="first name" value="{{ old('first_name') }}">
				</div>
				@if($errors->has('first_name'))
							@foreach ($errors->get('first_name') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('last_name')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="last_name">Last Name:</label>
				<div class="col-sm-8 input-group">
				  <input type="text" name="last_name"  class="form-control @if($errors->has('last_name')) form-control-warning @endif" id="last_name" placeholder="last name" value="{{ old('last_name') }}">
				</div>
				@if($errors->has('last_name'))
							@foreach ($errors->get('last_name') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('password')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="password">Password:</label>
				<div class="col-sm-8 input-group">
				  <input type="password" name="password"  class="form-control @if($errors->has('password')) form-control-warning @endif" id="password" value="">
				</div>
				@if($errors->has('password'))
							@foreach ($errors->get('password') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
				  <div class="offset-sm-4 col-sm-8"><small class="form-text text-muted">Password needs to be min. 6 characters.</small></div>
			</div>
			<div class="form-group @if($errors->has('confpassword')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="confpassword">Confirm Password:</label>
				<div class="col-sm-8 input-group">
				  <input type="password" name="confpassword"  class="form-control @if($errors->has('confpassword')) form-control-warning @endif" id="confpassword" value="">
				</div>
				@if($errors->has('confpassword'))
							@foreach ($errors->get('confpassword') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			  <div class="offset-sm-4 col-sm-8"><small class="form-text text-muted">Make sure it matches the password above.</small></div>
			</div>
			<div class="form-group @if($errors->has('email')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="email">EMail:</label>
				<div class="col-sm-8 input-group">
				  <input type="text" name="email"  class="form-control @if($errors->has('email')) form-control-warning @endif" id="email" placeholder="EMail" value="{{ old('email') }}">
				</div>
				@if($errors->has('email'))
							@foreach ($errors->get('email') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			  <div class="offset-sm-4 col-sm-8"><small class="form-text text-muted">Please enter a valid email address.</small></div>
			</div>
			<div class="form-group @if($errors->has('role')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="role">Role:</label>
				<div class="col-sm-8 input-group">
				<select name="role" id="role" tabindex="3" class="form-control" >
					<option value="admin" @if (old('role') =='admin') ? selected @endif >Admin</option>
					<option value="staff" @if (old('role') =='staff') ? selected @endif >Staff</option>
					<option value="client" 
						@if (old('role') =='client') ? selected @endif 
						@empty (old('role')) ? selected @endempty 
					>Client</option>
				</select>
				</div>
			</div>
			<div class="form-group @if($errors->has('account_status')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="role">Account Status:</label>
				<div class="col-sm-8 input-group">
				<select name="account_status" id="account_status" tabindex="3" class="form-control"  value="0">
					<option value="inactive" @if (old('account_status') == 'inactive') ? selected @endif >Inactive</option>
					<option value="active" @if (old('account_status') == 'active') ? selected @endif >Active</option>
				</select>
				</div>
			</div>
		<br/>
			<div class="form-group">
					<div class="input-with-icon">
						<input type="submit" name="addclient-submit" id="addclient-submit" tabindex="4" class="form-control" value="Save User">
						<span class="icon icon-save"></span>
					</div>
			</div>
	</form>

  
  </div>
@endsection
