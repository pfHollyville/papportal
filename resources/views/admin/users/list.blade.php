@extends('admin.layouts.app')

@section('title','User Management')

@section('content')
 <div class="col-md-12">
<div class="row">
	<div class="col-sm-8">
	 {{ $users->links() }}
	</div>
	<div class="col-sm-4 pull-right">
		<div class="btn-group">
			  <a class="btn btn-primary" href="{{route('admin.users.create')}}" data-toggle="tooltip" data-placement="right" data-container="body" data-original-title="Add new user">
				<span class="icon icon-add-to-list"></span> New
			  </a>
		</div>
	</div>
</div>
<div class="row">
	<div class="table-responsive">
	<table id="userTable" class="table" data-sort="table">
		<thead>
			<tr>
				<th class="header">Edit</th>
				<th class="header">Name</th>
				<th class="header">EMail</th>
				<th class="header">Role</th>
				<th class="header">Status</th>
				<th class="header">Company</th>
			</tr>
		</thead>
	<tbody>
	@foreach($users as $user)
	<tr>
		<td class="text-left"><a title="Edit" href="{{route('admin.users.edit',['id' => $user['id']])}}"><span class="icon icon-edit"></a></td>
		<td class="text-left"><a href="{{route('admin.users.show',['id' => $user['id']])}}">{{$user['first_name']}}&nbsp;{{$user['last_name']}}</a></td>
		<td class="text-left">{{$user['email']}}</td>
		<td class="text-left">{{$user['role']}}</td>
		<td class="text-left">{{$user['account_status']}}</td>
		<td class="text-left"><a href="{{route('admin.companies.show', ['id' => $user['company']['id']])}}">{{$user['company']['companyname']}}</a></td>
	</tr>
	@endforeach
	</tbody>
	</table>
	</div>
</div>
<div class="row">
	<div class="offset-col-8 col-sm-4">
	{{ $users->links() }}
	</div>
</div>
</div>
@endsection

@section('addscripts')
<script>
	$(document).ready(function() 
		{ 
			$("#userTable").tablesorter(); 
		} 
	); 

</script>
@endsection
