@extends('admin.layouts.app')

@section('title','Users - Edit User')

@section('content')
  <div class="col-md-12">
	<h3>Edit User</h3>

@if(count($errors)>0)
	<div class="alert alert-warning">Sorry but there are errors with this user; please correct them and ty to save again</div>
@endif

	<form id="edit_user" class="form" action="{{ route('admin.users.update', ['id' => $user['id']]) }}" method="post" role="form" style="display: block;">
		    {{ csrf_field() }}
			{{ method_field('PUT') }}
			<div class="form-group @if($errors->has('first_name')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="first_name">First Name:</label>
				<div class="input-group col-sm-8">
					<input type="text" name="first_name" id="first_name" tabindex="1" class="form-control @if($errors->has('first_name')) form-control-warning @endif" placeholder="John" value="{{ $user['first_name'] }}" required>
				</div>
				@if($errors->has('first_name'))
						@foreach ($errors->get('first_name') as $message)
							<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
						@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('last_name')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="last_name">Last Name:</label>
				<div class="input-group col-sm-8">
					<input type="text" name="last_name" id="last_name" tabindex="1" class="form-control @if($errors->has('last_name')) form-control-warning @endif" placeholder="Smith" value="{{ $user['last_name'] }}" required>
				</div>
				@if($errors->has('last_name'))
						@foreach ($errors->get('last_name') as $message)
							<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
						@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('email')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="email">Email:</label>
				<div class="input-group col-sm-8">
					<input type="text" name="email" id="email" tabindex="1" class="form-control @if($errors->has('email')) form-control-warning @endif" placeholder="me@somewhere.com" value="{{ $user['email'] }}" required>
				</div>
				@if($errors->has('email'))
						@foreach ($errors->get('email') as $message)
							<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
						@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('password')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="password">Password:</label>
				<div class="input-group col-sm-8">
					<input type="password" name="password" id="password" tabindex="1" class="form-control @if($errors->has('password')) form-control-warning @endif"  value="">
				</div>
				@if($errors->has('password'))
						@foreach ($errors->get('password') as $message)
							<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
						@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('confpassword')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="confpassword">Confirm Password:</label>
				<div class="input-group col-sm-8">
					<input type="password" name="confpassword" id="confpassword" tabindex="1" class="form-control @if($errors->has('confpassword')) form-control-warning @endif"  value="">
				</div>
				@if($errors->has('confpassword'))
						@foreach ($errors->get('confpassword') as $message)
							<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
						@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('role')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="role">Role:</label>
				<div class="input-group col-sm-8">
					<select name="role" id="role" tabindex="1" class="form-control @if($errors->has('role')) form-control-warning @endif" placeholder="First Name" value="{{ $user['role'] }}" required>
						<option value="client" @if ($user['role'] == 'client') selected @endif>Client</option>
						<option value="staff" @if ($user['role'] == 'staff') selected @endif>Staff</option>
						<option value="admin" @if ($user['role'] == 'admin') selected @endif>Admin</option>
					</select>
				</div>
				@if($errors->has('role'))
						@foreach ($errors->get('role') as $message)
							<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
						@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('account_status')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="account_status">Account Status:</label>
				<div class="input-group col-sm-8">
					<select name="account_status" id="account_status" tabindex="1" class="form-control @if($errors->has('account_status')) form-control-warning @endif" placeholder="First Name" value="{{ $user['account_status'] }}" required>
						<option value="inactive" @if ($user['account_status'] == 'inactive') selected @endif>Inactive</option>
						<option value="active" @if ($user['account_status'] == 'active') selected @endif>Active</option>
					</select>
				</div>
				@if($errors->has('account_status'))
						@foreach ($errors->get('account_status') as $message)
							<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
						@endforeach
				@endif
			</div>
		<br/>
		<div class="row">	
			<div class="col-sm-3 offset-sm-6">
				<input type="submit" name="addclient-submit" id="addclient-submit" tabindex="4" class="btn btn-login" value="Save">
			</div>
		</div>
	</form>

  
  </div>
@endsection
