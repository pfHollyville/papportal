@extends('admin.layouts.app')

@section('title','Phone Contacts')

@section('content')



<div class="col-md-12">
<div class="row">
	<div class="col-sm-8">
	 {{ $contacts->links() }}
	</div>
</div>
<div class="row">
	<div>
	<table id="billTable" class="table table-responsive" data-sort="table">
		<thead>
			<tr>
				<th class="header">Phone Contact ID</th>
				<th class="header">Load Date</th>
				<th class="header">Data Set ID</th>
				<th class="header">Sourcefile</th>
				<th class="header">Outcome Code</th>
				<th class="header">Call Back</th>
				<th class="header">Process Type</th>
				<th class="header">Agent Ref</th>
			</tr>
		</thead>
	<tbody>
	@foreach($contacts as $contact)
	<tr>
		<td>{{$contact->phonecontactid}}</td>
		<td>{{$contact->loaddate}}</td>
		<td>{{$contact->datasetid}}</td>
		<td>{{$contact->sourcefile}}</td>
		<td>{{$contact->outcomecode}}</td>
		<td>{{$contact->callback}}</td>
		<td>{{$contact->processtype}}</td>
		<td>{{$contact->agentref}}</td>
	</tr>
	@endforeach
	</tbody>
	</table>
	</div>
</div>
<div class="row">
	<div class="offset-col-8 col-sm-4">
	{{ $contacts->links() }}
	</div>
</div>
</div>




@endsection
@section('addscripts')
@endsection
