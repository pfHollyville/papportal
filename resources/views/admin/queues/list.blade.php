@extends('admin.layouts.app')
@section('title','Queues')

@section('content')



<div id="queues" class="container">
	<div class="row">
		<div class="toolbar">
			<form class="form" @submit.prevent="filterByCompany()">
			<div class="dashhead-toolbar-item form-group">		
				<select name="fcompany" v-model="fcompany" id="fcompany" class="form-control">
					<option value="0">Any Company</option>
					@foreach($allCompanies as $company)
					<option value="{{$company->id}}"  
					>{{$company->getRef()}} - {{$company->companyname}}</option>
					@endforeach
				</select>	
			</div>
			<div class="dashhead-toolbar-item form-group">		
			</div>
			<div class="btn-group dashhead-toolbar-item">
					<button type="submit" name="filtercompany-submit" id="filtercompany-submit" tabindex="4" class="btn btn-outline-primary"> 
					<span class="icon icon-funnel"></span> Find Queues</button>
			</div>
			</form>		
		</div>
	</div>
	
	<div class="row">
		<div v-if="companyservices" class="well col-sm-12" v-cloak>
			<div  v-if="!newqueue.edit">
				<button type="button" class="btn btn-sm btn-primary" v-on:click="addQueue()"><span class="glyphicon glyphicon-plus"></span> New</button>
			</div>
			
			<div v-if="newqueue.edit">
				<div><span class="glyphicon glyphicon-question-sign"></span> New Queue</div>
				<form class="form" @submit.prevent="savenewQueue()">
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.company_services_id}]">
						<label for="company_services_id">Company Service:</label>
						<select  v-model="newqueue.company_services_id"  class="form-control">
							<option v-for="companyservice in companyservices" v-bind:value="companyservice.id">@{{ companyservice.service.name }}</option>
						</select>	
						<span v-if="formerrors && formerrors.company_services_id" class="help-block">
							@{{ formerrors.company_services_id[0] }}
						</span>
					</div>
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.date}]">
						<label for="date">Date:</label>
						<datepicker v-model="newqueue.date" ></datepicker>
						<span v-if="formerrors && formerrors.date" class="help-block">
							@{{ formerrors.date[0] }}
						</span>
					</div>
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.queuename}]">
						<label for="queuename">Queue Name:</label>
						<input v-model="newqueue.queuename" class="form-control">
						<span v-if="formerrors && formerrors.queuename" class="help-block">
							@{{ formerrors.queuename[0] }}
						</span>
					</div>
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.queueid}]">
						<label for="queueid">Queue ID:</label>
						<input v-model="newqueue.queueid" class="form-control">
						<span v-if="formerrors && formerrors.queueid" class="help-block">
							@{{ formerrors.queueid[0] }}
						</span>
					</div>
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.type}]">
						<div class="control-group">
							<label class="control-label" for="Type">Type</label>
							<div class="controls">
								<input type="radio" id="Inbound" value="Inbound" v-model="newqueue.type" >
								<label for="Inbound">Inbound</label>
								<br>
								<input type="radio" id="Outbound" value="Outbound" v-model="newqueue.type" >
								<label for="Outbound">Outbound</label>
								<br>
								<input type="radio" id="Broadcast" value="Broadcast" v-model="newqueue.type" >
								<label for="Broadcast">Broadcast</label>
								<br>
								<span v-if="formerrors && formerrors.type" class="help-block">
									@{{ formerrors.type[0] }}
								</span>
							</div>
						</div>
					</div>
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.phonenumbers}]">
						<label for="phonenumbers">Phone Numbers:</label>
						<input v-model="newqueue.phonenumbers" class="form-control">
						<span v-if="formerrors && formerrors.phonenumbers" class="help-block">
							@{{ formerrors.phonenumbers[0] }}
						</span>
					</div>
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.numberpresentation}]">
						<label for="numberpresentation">Number Presentation:</label>
						<input v-model="newqueue.numberpresentation" class="form-control">
						<span v-if="formerrors && formerrors.numberpresentation" class="help-block">
							@{{ formerrors.numberpresentation[0] }}
						</span>
					</div>
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.notes}]">
						<label for="notes">Notes:</label>
						<textarea rows="2"  v-model="newqueue.notes" class="form-control"></textarea>
						<span v-if="formerrors && formerrors.notes" class="help-block">
							@{{ formerrors.notes[0] }}
						</span>
					</div>
					<button type="submit" class="btn btn-sm  btn-primary" ><span class="glyphicon glyphicon-save"></span> Save</button>
					<button v-on:click.stop.prevent="clearQueue(newqueue)" class="btn btn-sm"><span class="glyphicon glyphicon-erase"></span> Clear</button>
				</form>
			</div>
		</div>	
	</div>
	<div v-for="(queue,index) in queues" :key="queue.id">
		<div class="row">
		<div class="well col-sm-12" v-cloak >
			<div  v-if="!queue.edit">
				<h4>@{{ queue.queuename }}</h4>
				<div class="col-sm-2"><span class="badge badge-info">@{{ queue.type }}</span></div>
				<div class="col-sm-2">@{{ queue.queueid }}</div>
				<div class="col-sm-2">@{{ queue.date }}</div>
				<div class="col-sm-4">
					<button type="button" class="btn btn-sm btn-danger" v-on:click.stop.prevent="deleteQueue(index,queue.id)"><span class="glyphicon glyphicon-erase"></span> Remove</button>
					<button type="button" class="btn btn-sm  btn-info" v-on:click="editQueue(queue)"><span class="glyphicon glyphicon-edit"></span> Edit</button>
				</div>
				<div class="col-sm-12">@{{ queue.notes }}</div>
			</div>
			<div v-if="queue.edit">
				<form class="form" @submit.prevent="saveQueue(queue)">
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.company_services_id}]">
						<label for="company_services_id">Company Service:</label>
						<select  v-model="queue.company_services_id"  class="form-control">
							<option v-for="companyservice in companyservices" 
									v-bind:value="companyservice.id"
									selected="@{{ companyservice.id == queue.company_services_id ? 'true' : 'false' }}" 
							>@{{ companyservice.service.name }}</option>
						</select>	
						<span v-if="formerrors && formerrors.company_services_id" class="help-block">
							@{{ formerrors.company_services_id[0] }}
						</span>
					</div>
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.date}]">
						<label for="date">Date:</label>
						<datepicker v-model="queue.date" ></datepicker>
						<span v-if="formerrors && formerrors.date" class="help-block">
							@{{ formerrors.date[0] }}
						</span>
					</div>
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.queuename}]">
						<label for="queuename">Queue Name:</label>
						<input v-model="queue.queuename" class="form-control">
						<span v-if="formerrors && formerrors.queuename" class="help-block">
							@{{ formerrors.queuename[0] }}
						</span>
					</div>
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.queueid}]">
						<label for="queueid">Queue ID:</label>
						<input v-model="queue.queueid" class="form-control">
						<span v-if="formerrors && formerrors.queueid" class="help-block">
							@{{ formerrors.queueid[0] }}
						</span>
					</div>
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.type}]">
						<div class="control-group">
							<label class="control-label" for="Type">Type</label>
							<div class="controls">
								<input type="radio" id="Inbound" value="Inbound" v-model="queue.type" :checked="queue.type == 'Inbound'" >
								<label for="Inbound">Inbound</label>
								<br>
								<input type="radio" id="Outbound" value="Outbound" v-model="queue.type"  :checked="queue.type == 'Outbound'">
								<label for="Outbound">Outbound</label>
								<br>
								<input type="radio" id="Broadcast" value="Broadcast" v-model="queue.type" :checked="queue.type == 'Broadcast'" >
								<label for="Broadcast">Broadcast</label>
								<br>
								<span v-if="formerrors && formerrors.type" class="help-block">
									@{{ formerrors.type[0] }}
								</span>
							</div>
						</div>
					</div>
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.phonenumbers}]">
						<label for="phonenumbers">Phone Numbers:</label>
						<input v-model="queue.phonenumbers" class="form-control">
						<span v-if="formerrors && formerrors.phonenumbers" class="help-block">
							@{{ formerrors.phonenumbers[0] }}
						</span>
					</div>
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.numberpresentation}]">
						<label for="numberpresentation">Number Presentation:</label>
						<input v-model="queue.numberpresentation" class="form-control">
						<span v-if="formerrors && formerrors.numberpresentation" class="help-block">
							@{{ formerrors.numberpresentation[0] }}
						</span>
					</div>
					<div v-bind:class="['form-group',{'has-warning': formerrors && formerrors.notes}]">
						<label for="notes">Notes:</label>
						<textarea rows="2"  v-model="queue.notes" class="form-control"></textarea>
						<span v-if="formerrors && formerrors.notes" class="help-block">
							@{{ formerrors.notes[0] }}
						</span>
					</div>
					<button type="submit" class="btn btn-sm  btn-primary" ><span class="glyphicon glyphicon-save"></span> Save</button>
					<button v-on:click.stop.prevent="clearQueue(queue)" class="btn btn-sm"><span class="glyphicon glyphicon-erase"></span> Clear</button>
				</form>
			</div>
		</div>
		</div>
	</div>
</div>		
	
<!--	<pre>@{{ $data | json}}</pre> !-->


@endsection

@section('addscripts')
<script src="{{ asset('js/vue.js') }}"></script>
<script>
toastr.options = {
  "positionClass": "toast-bottom-right",
}




    var vm = new Vue({
          el: '#queues',
          data: {
			newqueue: {
				id: 0,
				queuename: '',
				queueid: '',
				date: Moment().format('YYYY-MM-DD'),
				type: '',
				notes: '',
				phonenumbers: '',
				numberpresentation: '',
				company_services_id: 0,
				edit: false
			},  
			queues: null,
			companyservices: null,
			formerrors: null,
			fcompany: 0,
          },
		  methods: {
			
			deleteQueue: function(index,queue_id) {
				var _this = this;
				axios.delete('{{ url("/admin/queues")}}/'+queue_id)
					.then(function (response) {
						_this.queues.splice(index,1);
						toastr.success('The queue was deleted','Success!');
					})
					.catch(function (error)	{
					});
			  },
			clearQueue: function(queue) {
				queue.edit = false;
			  },
			  editQueue: function(queue) {
				queue.edit = true;
			  },
			  addQueue: function() {
				this.newqueue.edit = true;
				this.newqueue.id = 0;
				this.newqueue.company_services_id = 0;
				this.newqueue.queuename = '';
				this.newqueue.queueid = '';
				this.newqueue.date = Moment(this.newqueue.date).format('YYYY-MM-DD');
				this.newqueue.type = '';
				this.newqueue.phonenumbers = '';
				this.newqueue.numberpresentation = '';
				this.newqueue.notes = '';
				this.formerrors = null;
			  },
			  saveQueue: function(queue) {
				queue.date = Moment(queue.date).format('YYYY-MM-DD');
				axios.put('{{ url("/admin/queues")}}/'+queue.id ,queue)
					.then(function (response) {
						queue.edit = false;
						toastr.success('Your queue was updated','Success!');
					})
					.catch(function (error)	{
						this.formerrors = error.response.data;
						errors = error.response.data;
						for(var error in errors) { 
							for(i=0;i<errors[error].length;i++) {
								toastr.error(errors[error][i]);
							}
						}
					});
			  },
			  savenewQueue: function() {
				this.formerrors = null;
				this.newqueue.date = Moment(this.newqueue.date).format('YYYY-MM-DD');
				var _this = this;
				axios.post('{{ url("/admin/queues")}}',_this.newqueue)
					.then(function (response) {
						_this.newqueue.id = response.data;
						_this.newqueue.edit = false;
						_this.queues.push(Vue.util.extend({},_this.newqueue));
						toastr.success('Your new queue was saved','Success!');
						_this.newqueue.id = 0;
						_this.newqueue.company_services_id = 0;
						_this.newqueue.queuname = '';
						_this.newqueue.queueid = '';
						_this.newqueue.date = Moment().format('YYYY-MM-DD');
						_this.newqueue.type = '';
						_this.newqueue.phonenumbers = '';
						_this.newqueue.numberpresentation = '';
						_this.newqueue.notes = '';
						_this.newqueue.edit = false;
						
					})
					.catch(function (error)	{
						this.formerrors = error.response.data;
						errors = error.response.data;
						for(var error in errors) { 
							for(i=0;i<errors[error].length;i++) {
								toastr.error(errors[error][i]);
							}
						}
					});
			  },
			  filterByCompany: function() {
				this.clearQueue(this.newqueue);
				this.formerrors = null;
				this.companyservices = null;
				this.queues = null;
				var _this = this;
				axios.get('{{ url("/admin/queues/company")}}/'+_this.fcompany)
					.then(function (response) {
						_this.queues = response.data.queues;
						_this.companyservices = response.data.companyservices;
					})
					.catch(function (error)	{
					});
			  }
			  
			  
		  },
		components: {
				'datepicker': Datepicker
		},
    });

 
</script>
@endsection
