@extends('admin.layouts.app')

@section('title','Company Notes')

@section('content')
<div class="btn-group">
	  <a title="Add new note" type="button" class="btn btn-outline-primary" href="{{route('admin.notes.create')}}">
		<span class="icon icon-pencil"></span>
	  </a>
</div>

<div class="container">
		<div class="dashhead-toolbar">
			<form id="filter-notes" class="form form-navbar" action="{{ route('admin.notes.filter') }}" method="get" role="form" style="display: block;">
			<div class="dashhead-toolbar-item form-group">		
				<select name="company" id="company" class="form-control">
					<option value="0">Any Company</option>
					@foreach($allCompanies as $company)
					<option value="{{$company->getRef()}}"  
						@if($filter['fcompany'] == $company->id) 
							selected
						@endif 
					>{{$company->getRef()}} - {{$company->companyname}}</option>
					@endforeach
				</select>
			</div>
			<div class="btn-group dashhead-toolbar-item">
					<button type="submit" tabindex="4" class="btn btn-outline-primary"> 
					<span class="icon icon-funnel"></span> Filter</button>
			</div>
			</form>		
		</div>
</div>

 <div class="col-md-12">
 
 
 {{ $notes->links() }}

	<div class="table-responsive">
	<table id="noteTable" class="table" data-sort="table">
		<thead>
			<tr>
				<th class="header">View</th>
				<th class="header">Company</th>
				<th class="header">Note</th>
				<th class="header">Date</th>
				<th class="header">User</th>
			</tr>
		</thead>
	<tbody>
	@foreach($notes as $note)
	<tr>
		<td><a href="{{ route('admin.notes.show',['id'=>$note->id])}}">View</a></td>
		<td>{{$note->company->companyname}}</td>
		<td>{{$note->message}}</td>
		<td>{{$note->updated_at}}</td>
		<td>{{$note->createdby->fullName()}}</td>
	</tr>
	@endforeach
	</tbody>
	</table>
	</div>

	{{ $notes->links() }}
</div>
@endsection
@section('addscripts')
<script>
	$(document).ready(function() 
		{ 
			$("#noteTable").tablesorter(); 
		} 
	); 

</script>
@endsection
