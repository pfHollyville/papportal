@extends('admin.layouts.app')

@section('title','Note')

@section('content')
 <div class="col-md-12">
 <div class="btn-group">
      <a title="Note List" type="button" class="btn btn-outline-primary" href="{{route('admin.notes.index')}}">
        <span class="icon icon-list"></span>
      </a>
</div>

 		<dl class="row">
			<dt class="col-sm-4">Company Name</dt>
			<dd class="col-sm-8">{{$note->company->companyname}}</dd>
			<dt class="col-sm-4">Note</dt>
			<dd class="col-sm-8">{{$note->message}}</dd>
			<dt class="col-sm-4">Date</dt>
			<dd class="col-sm-8">{{$note->updated_at}}</dd>
			<dt class="col-sm-4">User</dt>
			<dd class="col-sm-8">{{$note->createdby->fullName()}}</dd>
		</dl>
</div>
@endsection
