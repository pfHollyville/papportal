@extends('admin.layouts.app')

@section('title','Add New Note')

@section('content')
  <div class="col-md-12">
	<h3>Create new Note</h3>

@if(count($errors)>0)
	<div class="alert alert-warning">Sorry but there are errors with this note; please correct them and ty to save again</div>
@endif
 <div class="btn-group">
      <a title="Note List" type="button" class="btn btn-outline-primary" href="{{route('admin.notes.index')}}">
        <span class="icon icon-list"></span>
      </a>
</div>

	<form id="create_note" class="form" action="{{ route('admin.notes.store') }}" method="post" role="form" style="display: block;">
		    {{ csrf_field() }}
		<div class="form-group @if($errors->has('company_id')) has-warning @endif">
			<label class="form-control-label col-sm-4" for="company_id">Company:</label>
			<div class="col-sm-8 input-group">
			<select name="company_id" id="company_id" tabindex="3" class="form-control" >
				@foreach($companies as $company)
				<option value="{{$company['id']}}" @if (old('company_id') ==$company['id']) ? selected @endif >{{$company['companyname']}}</option>
				@endforeach
			</select>
			</div>
		</div>
		<div class="form-group @if($errors->has('message')) has-warning @endif">
			<label class="form-control-label col-sm-4" for="message">Note:</label>
			<div class="input-group col-sm-8">
				<input type="text" name="message" id="message" tabindex="1" class="form-control" placeholder="my note" value="{{ old('message') }}" required>
			</div>
			@if($errors->has('message'))
					@foreach ($errors->get('note') as $message)
						<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
					@endforeach
			@endif
		</div>
		<br/>
		<div class="col-sm-3 offset-sm-6">
			<input type="submit" name="addnote-submit" id="addnote-submit" tabindex="4" class="btn btn-login" value="Save">
		</div>
	</form>

  
  </div>
@endsection
