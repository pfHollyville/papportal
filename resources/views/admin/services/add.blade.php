@extends('admin.layouts.app')

@section('title','Service - Add New Service')

@section('content')
  <div class="col-md-12">
	<h3>Create new service</h3>

@if(count($errors)>0)
	<div class="form-group has-warning">
		<div class="form-control-label">Sorry but there are errors with this service; please correct them and ty to save again</div>
	</div>
@endif

	<form id="create_users" class="form" action="{{ route('admin.services.store') }}" method="post" role="form" style="display: block;">
		    {{ csrf_field() }}
			<div class="form-group @if($errors->has('name')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="name">Service Name:</label>
				<div class="input-group col-sm-8">
				  <input type="text" name="name" class="form-control @if($errors->has('name')) form-control-warning @endif" id="name" placeholder="service name" value="{{ old('name') }}">
				</div>
				@if($errors->has('name'))
							@foreach ($errors->get('name') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('description')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="description">Description:</label>
				<div class="input-group col-sm-8">
				  <textarea name="description"  rows="4" class="form-control @if($errors->has('description')) form-control-warning @endif" id="description" placeholder="service description">"{{ old('description') }}</textarea>
				</div>
				@if($errors->has('description'))
							@foreach ($errors->get('description') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('maxusage')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="maxusage">Max usage:</label>
				<div class="input-group col-sm-8">
				  <input type="text" name="maxusage" class="form-control @if($errors->has('maxusage')) form-control-warning @endif" id="maxusage" placeholder="0.00" value="{{ old('maxusage') }}">
				</div>
				@if($errors->has('maxusage'))
							@foreach ($errors->get('maxusage') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('mincommitment')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="mincommitment">Min Commitment:</label>
				<div class="input-group col-sm-8">
				  <input type="text" name="mincommitment" class="form-control @if($errors->has('mincommitment')) form-control-warning @endif" id="mincommitment" placeholder="0.00" value="{{ old('mincommitment') }}">
				</div>
				@if($errors->has('mincommitment'))
							@foreach ($errors->get('mincommitment') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('basecost')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="basecost">Monthly Cost:</label>
				<div class="input-group col-sm-8">
				  <input type="text" name="basecost" class="form-control @if($errors->has('basecost')) form-control-warning @endif" id="basecost" placeholder="0.00" value="{{ old('basecost') }}">
				</div>
				@if($errors->has('basecost'))
							@foreach ($errors->get('basecost') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('extracost')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="extracost">Unit Cost:</label>
				<div class="input-group col-sm-8">
				  <input type="text" name="extracost" class="form-control @if($errors->has('extracost')) form-control-warning @endif" id="extracost" placeholder="0.00" value="{{ old('extracost') }}">
				</div>
				@if($errors->has('extracost'))
							@foreach ($errors->get('extracost') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('units')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="units">Units:</label>
				<div class="input-group col-sm-8">
				  <select name="units" class="form-control @if($errors->has('units')) form-control-warning @endif" id="units">
					<option value="per minute" @if (old('units') == 'per minute') selected  @endif>Per Minute</option>
					<option value="per call" @if (old('units') == 'per call') selected  @endif>Per Call</option>
					<option value="per month" @if (old('units') == 'per month') selected  @endif>Per Month</option>
					<option value="per webchat" @if (old('units') == 'per webchat') selected  @endif>Per Webchat</option>
				  </select>
				</div>
				@if($errors->has('units'))
							@foreach ($errors->get('units') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('active')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="active">Active:</label>
				<div class="input-group col-sm-8">
				  <select name="active" class="form-control @if($errors->has('active')) form-control-warning @endif" id="active">
					<option value="1" @if (old('active') == 1) selected  @endif>Active</option>
					<option value="0" @if (old('active') == 0) selected  @endif>InActive</option>
				  </select>
				</div>
				@if($errors->has('active'))
							@foreach ($errors->get('active') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('image')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="image">Image:</label>
				<div class="input-group col-sm-8">
				  <input type="text" name="image" class="form-control @if($errors->has('image')) form-control-warning @endif" id="image" placeholder="image" value="{{ old('image') }}">
				</div>
				@if($errors->has('image'))
							@foreach ($errors->get('image') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>
			<div class="form-group @if($errors->has('category_id')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="category_id">Category:</label>
				<div class="col-sm-8 input-group">
				<select name="category_id" id="category_id" tabindex="3" class="form-control" >
					@foreach($categories as $category)
					<option value="{{$category['id']}}" @if (old('category_id') ==$category['id']) ? selected @endif >{{$category['name']}}</option>
					@endforeach
				</select>
				</div>
			</div>
			<div class="form-group @if($errors->has('accountcode')) has-warning @endif">
				<label class="form-control-label col-sm-4" for="accountcode">Account Code:</label>
				<div class="input-group col-sm-8">
				  <input type="text" name="accountcode" class="form-control @if($errors->has('accountcode')) form-control-warning @endif" id="accountcode" placeholder="accountcode" value="{{ old('extracost') }}">
				</div>
				@if($errors->has('accountcode'))
							@foreach ($errors->get('accountcode') as $message)
								<div class="offset-sm-4 col-sm-8 form-control-label"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
							@endforeach
				@endif
			</div>

		<br/>
			<div class="form-group">
				<div class="offset-col-4 col-sm-8 input-group">
					<input type="submit" name="addservice-submit" id="addservice-submit" tabindex="4" class="btn btn-primary" value="Save">
				</div>
			</div>
	</form>

  
  </div>
@endsection
