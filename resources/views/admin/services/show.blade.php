@extends('admin.layouts.app')

@section('title','Service Details')

@section('content')
 <div class="col-md-12">
	<div class="table-responsive">
	<table id="serviceTable" class="table" data-sort="table">
		<thead>
			<tr>
				<th class="header">Category</th>
				<th class="header">Service Name</th>
				<th class="header">Description</th>
				<th class="header text-right">Max Usage</th>
				<th class="header text-right">Min Commitment</th>
				<th class="header text-right">Monthly Cost</th>
				<th class="header text-right">Unit Cost</th>
				<th class="header text-right">Units</th>
				<th class="header text-right">Account Code</th>
				<th class="header">Active</th>
				<th class="header">Image</th>
				<th class="header">Created</th>
				<th class="header">Updated</th>
			</tr>
		</thead>
	<tbody>
		<tr>
			<td class="text-left">{{$service['servicecategory']['name']}}</td>
			<td class="text-left">{{$service['name']}}</td>
			<td class="text-left">{{$service['description']}}</td>
			<td class="text-right">{{$service['maxusage']}}</td>
			<td class="text-right">{{$service['mincommitment']}}</td>
			<td class="text-right">{{$service['basecost']}}</td>
			<td class="text-right">{{$service['extracost']}}</td>
			<td class="text-right">{{$service['units']}}</td>
			<td class="text-right">{{$service['accountcode']}}</td>
			<td class="text-left">{{$service['active']}}</td>
			<td class="text-left">{{$service['image']}}</td>
			<td class="text-left">{{$service['created_at']}}</td>
			<td class="text-left">{{$service['updated_at']}}</td>
		</tr>
	</tbody>
	</table>
	</div>
</div>
@endsection
