@extends('admin.layouts.app')

@section('title','Service Management')

@section('content')
 <div class="col-md-12">
	 <div class="offset-col-8 col-md-4">
		<a href="{{route('admin.services.create')}}" class="btn btn-outline-primary"><span class="icon icon-add-to-list"> Add Service</a>
	 </div>
	 {{ $services->links() }}

	<div class="table-responsive">
	<table id="serviceTable" class="table" data-sort="table">
		<thead>
			<tr>
				<th class="header">Edit</th>
				<th class="header">Category</th>
				<th class="header">Service Name</th>
				<th class="header">Description</th>
				<th class="header text-right">Max Usage</th>
				<th class="header text-right">Min Commitment</th>
				<th class="header text-right">Monthly Cost</th>
				<th class="header text-right">Unit Cost</th>
				<th class="header text-right">Units</th>
				<th class="header text-right">AccountCode</th>
				<th class="header">Active</th>
				<th class="header">Image</th>
			</tr>
		</thead>
	<tbody>
	@foreach($services as $service)
	<tr>
		<td class="text-left"><a href="{{ route('admin.services.edit', ['id' => $service['id']])}}"><span class="icon icon-pencil"></span></a></td>
		<td class="text-left">{{$service['servicecategory']['name']}}</td>
		<td class="text-left"><a href="{{ route('admin.services.show', ['id' => $service['id']])}}">{{$service['name']}}</a></td>
		<td class="text-left">{{$service['description']}}</td>
		<td class="text-right">{{$service['maxusage']}}</td>
		<td class="text-right">{{$service['mincommitment']}}</td>
		<td class="text-right">{{$service['basecost']}}</td>
		<td class="text-right">{{$service['extracost']}}</td>
		<td class="text-right">{{$service['units']}}</td>
		<td class="text-right">{{$service['accountcode']}}</td>
		<td class="text-left">{{$service['active']}}</td>
		<td class="text-left">{{$service['image']}}</td>
	</tr>
	@endforeach
	</tbody>
	</table>
	</div>

	{{ $services->links() }}
</div>
@endsection
@section('addscripts')
<script>
	$(document).ready(function() 
		{ 
			$("#serviceTable").tablesorter(); 
		} 
	); 

</script>
@endsection
