@extends('admin.layouts.app')

@section('title','Service Category Management')

@section('content')
 <div class="col-md-12">
 {{ $servicecategories->links() }}

	<div class="table-responsive">
	<table id="servicecategoryTable" class="table" data-sort="table">
		<thead>
			<tr>
				<th class="header">Service Category Name</th>
				<th class="header">Description</th>
				<th class="header">Image</th>
				<th class="header">Queue</th>
			</tr>
		</thead>
	<tbody>
	@foreach($servicecategories as $servicecategory)
	<tr>
		<td class="text-left">{{$servicecategory['name']}}</td>
		<td class="text-left">{{$servicecategory['description']}}</td>
		<td class="text-left">{{$servicecategory['image']}}</td>
		<td class="text-left">{{$servicecategory['queue']}}</td>
	</tr>
	@endforeach
	</tbody>
	</table>
	</div>

	{{ $servicecategories->links() }}
</div>
@endsection
@section('addscripts')
<script>
	$(document).ready(function() 
		{ 
			$("#servicecategoryTable").tablesorter(); 
		} 
	); 

</script>
@endsection
