@extends('admin.layouts.app')

@section('title','Edit servicecategory')

@section('content')
  <div class="col-md-12">
	<h3>Update Service Category</h3>

@if(count($errors)>0)
	<div class="alert alert-warning">Sorry but there are errors with this category; please correct them and ty to save again</div>
@endif

	<form id="create_servicecategory" class="form-inline form-horizontal" action="{{ route('admin.servicecategories.update',['id'=>$servicecategory->id]) }}" method="post" role="form" style="display: block;">
		    {{ csrf_field() }}
			{{ method_field('PUT') }}
		<div class="row">
			<label class="col-sm-4 text-left" for="companyname">Name:</label>
			<div class="input-group col-sm-8 form-inline required">
				<input type="text" name="name" id="name" tabindex="1" class="form-control" placeholder="Name" value="{{ $servicecategory->name }}" required>
			</div>
			@if($errors->has('name'))
					@foreach ($errors->get('name') as $message)
						<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
					@endforeach
			@endif
			<label class="col-sm-4 text-left"  for="description">Description:</label>
			<div class="input-group col-sm-8 form-inline">
				<input type="text" name="description" id="description" tabindex="2" class="form-control" placeholder="describe the service" value="{{ $servicecategory->description }}
" >
			</div>
			@if($errors->has('description'))
					@foreach ($errors->get('description') as $message)
						<div class="offset-sm-4 col-sm-8 alert alert-warning"><span class="icon icon-thumbs-down"></span><strong>Oops! </strong>{{$message}}</div>
					@endforeach
			@endif
			<label class="col-sm-4 text-left"  for="image">Image:</label>
			<div class="input-group col-sm-8 form-inline">
				<input type="text" name="image" id="image" tabindex="3" class="form-control" placeholder="image url" value="{{$servicecategory->image}}">
			</div>
			<label class="col-sm-4 text-left" for="queue">Queue:</label>
			<div class="input-group col-sm-8 form-inline required">
				<select name="queue" id="queue" tabindex="1" class="form-control">
						<option value='No' @if( $servicecategory->queue == 'No') selected @endif>No</option>
						<option value='Optional' @if( $servicecategory->queue == 'Optional') selected @endif>Optional</option>
						<option value='Mandatory' @if( $servicecategory->queue == 'Mandatory') selected @endif>Mandatory</option>
				</select>
			</div>
		</div>
		<br/>
		<div class="row">	
			<div class="col-sm-3 offset-sm-6">
				<input type="submit" name="addservicecategory-submit" id="addservicecategory-submit" tabindex="4" class="btn btn-login" value="Save">
			</div>
		</div>
	</form>

  
  </div>
@endsection
