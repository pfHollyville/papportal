@extends('admin.layouts.app')

@section('title','Service category details')

@section('content')
 <div class="col-md-12">
	<div class="table-responsive">
	<table id="companytable" class="table" data-sort="table">
		<thead>
			<tr>
				<th class="header">Name</th>
				<th class="header">Description</th>
				<th class="header">Image</th>
				<th class="header">Queue</th>
				<th class="header">Created At</th>
				<th class="header">Updated At</th>
			</tr>
		</thead>
	<tbody>
	<tr>
		<td>{{$servicecategory['name']}}</td>
		<td>{{$servicecategory['description']}}</td>
		<td>{{$servicecategory['image']}}</td>
		<td>{{$servicecategory['queue']}}</td>
		<td>{{$servicecategory['created_at']}}</td>
		<td>{{$servicecategory['updated_at']}}</td>
	</tr>
	</tbody>
	</table>
	</div>
</div>
@endsection
