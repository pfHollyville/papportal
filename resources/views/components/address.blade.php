<address>
  <strong>{{$ccompany['companyname']}}</strong><br>
  {{$ccompany['address1']}}&nbsp;{{$ccompany['address2']}}<br>
	{{$ccompany['city']}},&nbsp;{{$ccompany['county']}}<br>
  {{$ccompany['postcode']}}, {{$ccompany['country']}}<br>
  <abbr title="Phone">P:</abbr> {{$ccompany['workphone']}}<br>
  <abbr title="Email">E:</abbr> <a href="mailto:#">{{$ccompany['workemail']}}</a>
</address>
