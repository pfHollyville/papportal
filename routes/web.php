<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
Route::get('/privacy', function () {
    return view('legals');
});

Route::get('/terms', function () {
    return view('tsandcs');
});
*/

Auth::routes();
Route::get('/admin', 'Admin\\DashboardController@index')->name('admin.dashboard');
Route::get('/admin/login', 'Admin\\Auth\\LoginController@showLoginForm')->name('admin.login');
Route::post('/admin/login', 'Admin\\Auth\\LoginController@login')->name('admin.login');
Route::post('/admin/logout', 'Admin\\Auth\\LoginController@logout')->name('admin.logout');
Route::get('/pages/{slug}', 'PageController@show')->name('pages.show');

// Client routes
Route::group(['middleware' => ['auth','active','client']], function()
{
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/questions/basic', 'QuestionController@basic')->name('questions.basic');
	Route::get('/questions/faq', 'QuestionController@faq')->name('questions.faq');
	Route::resource('company', 'CompanyController');
	Route::resource('questions', 'QuestionController');
	Route::resource('services', 'CompanyServicesController');
});

// Admin routes
Route::group(['middleware' => ['auth','active','admin'], 'namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.'], function()
{
	Route::get('users/create', 'UserController@create')->name('users.create');
	Route::post('users', 'UserController@store')->name('users.store');
	Route::get('users/{user}/edit', 'UserController@edit')->name('users.edit');
	Route::put('users/{user}', 'UserController@update')->name('users.update');

	Route::get('companies/create', 'CompanyController@create')->name('companies.create');
	Route::post('companies', 'CompanyController@store')->name('companies.store');
	Route::get('companies/{user}/edit', 'CompanyController@edit')->name('companies.edit');
	Route::put('companies/{user}', 'CompanyController@update')->name('companies.update');

	Route::get('alerts', 'AlertController@index')->name('alerts.index');
	Route::get('alerts/create', 'AlertController@create')->name('alerts.create');
	Route::get('alerts/{id}', 'AlertController@show')->name('alerts.show');
	Route::post('alerts', 'AlertController@store')->name('alerts.store');
	Route::get('alerts/{id}/edit', 'AlertController@edit')->name('alerts.edit');
	Route::put('alerts/{id}', 'AlertController@update')->name('alerts.update');
	
	
	
	
//	Route::resource('users', 'UserController');
//	Route::resource('companies', 'CompanyController');
	Route::resource('servicecategories', 'ServicecategoryController');
	Route::resource('services', 'ServiceController');
//	Route::resource('companyservices', 'CompanyServicesController');
	Route::post('bills/getservices', 'BillController@getServices')->name('bills.getservices');
	Route::post('bills/filter', 'BillController@filter')->name('bills.filter');
	Route::get('bills/createallbills', 'BillController@createAllBills')->name('bills.createallbills');
	Route::get('bills/saveallbills/{year}/{month}', 'BillController@saveAllBills')->name('bills.saveallbills');
	Route::get('bills/exportbills/{year}/{month}', 'BillController@export')->name('bills.export');
	Route::resource('bills', 'BillController');

	Route::resource('queues', 'QueueController');
	Route::get('queues/company/{company_id}', 'QueueController@company')->name('queues.company');

	Route::post('company/filter', 'CompanyController@filter')->name('companies.filter');
	Route::post('companyservices/filter', 'CompanyServicesController@filter')->name('companyservices.filter');
	Route::resource('webchats', 'WebchatController');
	Route::resource('basicquestionmasters', 'BasicquestionmasterController');

	Route::resource('pages', 'PageController');
	Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

	

});



/* Staff  */
Route::group(['middleware' => ['auth','active','staff'], 'namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.'], function()
{
	Route::get('users', 'UserController@index')->name('users.index');
	Route::get('users/{user}', 'UserController@show')->name('users.show');

	Route::get('companies', 'CompanyController@index')->name('companies.index');
	Route::get('companies/{company}', 'CompanyController@show')->name('companies.show');

	Route::get('companyservices/create', 'CompanyServicesController@create')->name('companyservices.create');
	Route::get('companyservices', 'CompanyServicesController@index')->name('companyservices.index');
	Route::get('companyservices/{company}', 'CompanyServicesController@show')->name('companyservices.show');
	Route::post('companyservices', 'CompanyServicesController@store')->name('companyservices.store');
	Route::get('companyservices/{user}/edit', 'CompanyServicesController@edit')->name('companyservices.edit');
	Route::put('companyservices/{user}', 'CompanyServicesController@update')->name('companyservices.update');

	Route::get('questions/filter', 'QuestionController@filter')->name('questions.filter');
	Route::get('questions/basic', 'QuestionController@basic')->name('questions.basic');
	Route::resource('questions', 'QuestionController');

	Route::get('calls','CallController@index')->name('calls.index');
	Route::get('phonecontacts','PhonecontactController@index')->name('phonecontacts.index');

	Route::get('notes/filter', 'NoteController@filter')->name('notes.filter');
	Route::get('notes/create', 'NoteController@create')->name('notes.create');
	Route::get('notes', 'NoteController@index')->name('notes.index');
	Route::get('notes/{company}', 'NoteController@show')->name('notes.show');
	Route::post('notes', 'NoteController@store')->name('notes.store');
//	Route::get('notes/{user}/edit', 'NoteController@edit')->name('notes.edit');
//	Route::put('notes/{user}', 'NoteController@update')->name('notes.update');
	
});

Route::get('/{slug}', 'PageController@show')->name('pages.show');
	


