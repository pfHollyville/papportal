<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('cors')->post('/sechat', 'Api\\SechatController@postMessage');
Route::get('/calls/getcalls', 'Api\\CallController@getCalls');
Route::get('/sendgrid/test', 'Api\\SendgridController@test');
Route::post('/calls/chargeable', 'Api\\CallController@chargeable');
Route::post('/phone', 'Api\\PhoneController@postPhone');
Route::get('/xero/invoices', 'Api\\XeroController@getInvoices');
Route::get('/xero/createinvoice', 'Api\\XeroController@createInvoice');
Route::get('/xero/branding', 'Api\\XeroController@getBranding');
Route::get('xero/saveallinvoices/{year}/{month}', 'Api\\XeroController@saveAllInvoices')->name('bills.saveallinvoices');

Route::get('/services/{id}', 'Api\\ServiceController@show');


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
