<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Carbon\Carbon;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
		'role' => 'client',
		'account_status' => 'active'
    ];
});

$factory->define(App\Company::class, function (Faker\Generator $faker) {

    return [
        'companyname' => $faker->company,
        'companynumber' => 11111,
        'address1' => $faker->randomDigit . " " . $faker->streetName,
        'city' => $faker->city,
        'county' => $faker->county,
        'postcode' => $faker->postcode,
        'country' => 'UK',
		'industry_id' => $faker->numberBetween(1,25),
		'legalstatus_id' => $faker->numberBetween(1,5),
		'usagereason_id' => $faker->numberBetween(1,2),
		'inbound' => $faker->numberBetween(0,1),
		'outbound' => $faker->numberBetween(0,1),
		'web' => $faker->numberBetween(0,1),
		'leads' => $faker->numberBetween(0,1),
		'averagesale' => $faker->randomFloat(2,10,100),
		'averageduration' => $faker->numberBetween(0,24),
		'workphone' => $faker->phoneNumber,
		'workemail' => $faker->safeEmail
    ];
});

$factory->define(App\CompanyServices::class, function (Faker\Generator $faker) {

	$service = $faker->numberBetween(1,9);
	if ($service >3 && $service < 8) {
		$webchat = $faker->uuid;
	} else {
		$webchat = null;
	}
	
    return [
		'company_id' => $faker->numberBetween(1,50),
		'service_id' => $service,
		'widget_id' => $webchat,
		'created_by' => $faker->numberBetween(1,50),
		'status' 	 => $faker->randomElement(['pending','approved','rejected']),
		'start_date' => Carbon::now()->subDays($faker->numberBetween(1,60)),
		'created_at' => Carbon::now()->subDays($faker->numberBetween(30,60)),
		'created_by' => $faker->numberBetween(1,4)
    ];
});

$factory->define(App\Webchat::class, function (Faker\Generator $faker) {

	
    return [
		'chat_id' 		=> $faker->uuid,
		'url'			=> $faker->url,
		'requester_details_name'	=> $faker->name,
		'requester_details_emails'	=> $faker->safeEmail,
		'requester_details_address'	=> $faker->randomDigit . " " . $faker->streetName,
        'requester_details_city' => $faker->city,
        'requester_details_state' => $faker->county,
        'requester_details_zip' => $faker->postcode,
        'requester_details_country' => $faker->country,
        'requester_details_company_name' => $faker->company,
        'requester_details_gender' => $faker->randomElement(['male', 'female']),
        'requester_details_age' => $faker->numberBetween(12,80),
		'created_at' 	=> Carbon::now()->subDays($faker->numberBetween(0,60)),
		'created_at_date' 	=> Carbon::now()->subDays($faker->numberBetween(0,90))->subHours($faker->numberBetween(0,24))->subMinutes($faker->numberBetween(0,60))
    ];
});

$factory->define(App\Question::class, function (Faker\Generator $faker) {
    return [
        'question' => $faker->sentence,
        'answer' => $faker->paragraph,
        'category' => $faker->randomElement(['Business','Web','Business','Business','Phone','Business','Business']),
		'order' => 1,
		'company_id' => $faker->numberBetween(1,50)
    ];
});

$factory->define(App\Queue::class, function (Faker\Generator $faker) {
    return [
		'company_id' => $faker->numberBetween(1,50),
		'date' => Carbon::now()->subDays($faker->numberBetween(30,60)),
        'queuename' => $faker->word,
        'queueid' => $faker->numberBetween(680000,699000),
        'type' => $faker->randomElement(['Inbound','Outbound','Broadcast']),
		'phonenumbers' => implode(",",$faker->randomElements([$faker->phonenumber,$faker->phonenumber,$faker->phonenumber],$faker->numberBetween(1,3))),
		'notes' => $faker->sentence
    ];
});
