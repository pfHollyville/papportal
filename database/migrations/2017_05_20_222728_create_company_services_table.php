<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_services', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('company_id')->unsigned();	
			$table->foreign('company_id')->references('id')->on('companies');
			$table->integer('service_id')->unsigned();	
			$table->foreign('service_id')->references('id')->on('services');
			$table->string('widget_id')->nullable()->default(null);
			$table->foreign('widget_id')->references('widget_id')->on('webchats');
            $table->decimal('basecost',10,2)->nullable()->default(null);
            $table->decimal('pertransactioncost',10,2)->nullable()->default(null);;
			$table->string('status')->default('pending');	
			$table->date('start_date')->nullable()->default(null);	
			$table->date('end_date')->nullable()->default(null);	
            $table->timestamps();
			$table->unsignedInteger('created_by')-> nullable()->default(null);
			$table->unsignedInteger('updated_by')-> nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_services');
    }
}
