<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('basic')->default(false);
            $table->integer('order')->default(0);
            $table->string('question',200);
            $table->string('category',64);
            $table->text('answer')->nullable()->default(null);
            $table->timestamps();
			$table->unsignedInteger('created_by')-> nullable()->default(null);
			$table->unsignedInteger('updated_by')-> nullable()->default(null);
			$table->integer('company_id')->unsigned();	
			$table->foreign('company_id')->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
