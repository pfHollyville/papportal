<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccountcode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('services', function($table) {
			$table->string('accountcode',16)-> nullable()->default(null);
		});
    }
	

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('services', function($table) {
			$table->dropColumn('accountcode');
		});    
	}
}
