<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXerobillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xerobills', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('company_id')->unsigned();	
			$table->foreign('company_id')->references('id')->on('companies');
			$table->integer('year');
			$table->integer('month');
			$table->uuid('invoice_id')->nullable()->default(null);
			$table->string('status',16)->default('draft');
			$table->text('notes')->nullable()->default(null);
			$table->unsignedInteger('created_by')-> nullable()->default(null);
			$table->unsignedInteger('updated_by')-> nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xerobills');
    }
}
