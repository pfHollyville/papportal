<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCdrlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		

        Schema::create('cdrlogs', function (Blueprint $table) {
            $table->increments('id');
			$table->biginteger('callid')->unsigned();	
			$table->integer('qid')->unsigned()->nullable()->default(0);	
			$table->integer('dataset')->unsigned()->nullable()->default(0);	
			$table->integer('urn')->unsigned()->nullable()->default(0);	
			$table->integer('agent')->unsigned()->nullable()->default(0);	
			$table->string('ddi',16)->nullable();
			$table->string('cli',16)->nullable();
            $table->decimal('ringtime',10,5)->nullable()->default(0);
            $table->decimal('duration',10,5)->nullable()->default(0);
			$table->string('result',16)->nullable();
			$table->integer('outcome')->unsigned()->nullable()->default(0);	
			$table->string('type',16)->nullable();
			$table->dateTime('datetime')->nullable();
			$table->dateTime('answer')->nullable();
			$table->dateTime('disconnect')->nullable();
			$table->dateTime('last_update')->nullable();
			$table->string('carrier',16)->nullable();
			$table->string('flags',32)->nullable();
			$table->string('terminate',32)->nullable();
			$table->biginteger('customer')->unsigned()->nullable();	
            $table->decimal('customer_cost',10,2)->nullable();
			$table->boolean('chargeable')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cdrlogs');

    }
}
