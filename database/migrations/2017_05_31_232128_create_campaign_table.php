<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
			$table->date('date')->nullable()->default(null);
			$table->string('queuename',50)->nullable()->default(null);
			$table->string('queueid',10)->nullable()->default(null);
			$table->string('type',20)->nullable()->default(null);
			$table->string('phonenumbers',180)->nullable()->default(null);
			$table->string('numberpresentation',180)->nullable()->default(null);
			$table->text('notes')->nullable()->default(null);
			$table->integer('company_id')->unsigned();	
			$table->foreign('company_id')->references('id')->on('companies');
            $table->timestamps();
			$table->unsignedInteger('created_by')-> nullable()->default(null);
			$table->unsignedInteger('updated_by')-> nullable()->default(null);
        //
		});
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');

    }
}
