<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBilllineitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lineitems', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('companyservices_id')->unsigned();	
			$table->foreign('companyservices_id')->references('id')->on('companyservices');
			$table->integer('bill_id')->unsigned();	
			$table->foreign('bill_id')->references('id')->on('bills');
            $table->string('name',50);
            $table->integer('maxusage')->nullable()->default(null);
            $table->integer('mincommitment')->nullable()->default(null);
            $table->decimal('basecost',10,2)->nullable()->default(null);
            $table->decimal('extracost',10,2)->nullable()->default(null);
            $table->decimal('unitsused',10,2)->nullable()->default(null);
            $table->decimal('basesubtotal',10,2)->nullable()->default(null);
            $table->decimal('extrasubtotal',10,2)->nullable()->default(null);
            $table->decimal('linetotal',10,2)->nullable()->default(null);
			$table->decimal('prorata',10,2)->default(0);
			$table->unsignedInteger('created_by')-> nullable()->default(null);
			$table->unsignedInteger('updated_by')-> nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lineitems');

    }
}
