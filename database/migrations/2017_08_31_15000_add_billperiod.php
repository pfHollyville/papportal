<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBillperiod extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

		Schema::table('bills', function($table) {
            $table->integer('year')->nullable()->default(0);
            $table->integer('month')->nullable()->default(0);
		});

    }
	

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('bills', function($table) {
			$table->dropColumn('year');
			$table->dropColumn('month');
		});    
	}
}
