<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calls', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('qid')->unsigned();	
            $table->decimal('sec_talk',10,2)->nullable()->default(null);
            $table->integer('cid')->nullable()->default(null);
            $table->integer('nc_in')->nullable()->default(null);
            $table->integer('nc_out')->nullable()->default(null);
            $table->date('date')->nullable()->default(null);
			$table->unsignedInteger('created_by')-> nullable()->default(null);
			$table->unsignedInteger('updated_by')-> nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calls');

    }
}
