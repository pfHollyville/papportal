<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('services', function($table) {
			$table->string('units')-> nullable()->default(null);
		});
		Schema::table('company_services', function($table) {
            $table->integer('maxusage')->nullable()->default(null);
            $table->integer('mincommitment')->nullable()->default(null);
			$table->string('units')-> nullable()->default(null);
		});
    }
	

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('services', function($table) {
			$table->dropColumn('units');
		});    
		Schema::table('company_services', function($table) {
			$table->dropColumn('units');
			$table->dropColumn('maxusage');
			$table->dropColumn('mincommitment');
		});    
	}
}
