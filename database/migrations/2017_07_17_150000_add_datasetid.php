<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDatasetid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('company_services', function($table) {
            $table->integer('datasetid')->nullable()->default(null);
            $table->integer('queueid')->nullable()->default(null);
		});
    }
	

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('company_services', function($table) {
			$table->dropColumn('datasetid');
			$table->dropColumn('queueid');
		});    
	}
}
