<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
			$table->string('companyname',50);
			$table->string('companynumber',20)->nullable()->default(null);
			$table->string('address1',50)->nullable()->default(null);
			$table->string('address2',50)->nullable()->default(null);
			$table->string('city',50)->nullable()->default(null);
			$table->string('county',50)->nullable()->default(null);
			$table->string('postcode',50)->nullable()->default(null);
			$table->string('country',50)->nullable()->default(null);
			$table->string('workphone',32)->nullable()->default(null);
			$table->string('workemail',150)->nullable()->default(null);
			$table->integer('industry_id')->unsigned();	
			$table->foreign('industry_id')->references('id')->on('industries');
			$table->integer('legalstatus_id')->unsigned();	
			$table->foreign('legalstatus_id')->references('id')->on('legalstatuses');
			$table->integer('usagereason_id')->unsigned();	
			$table->foreign('usagereason_id')->references('id')->on('usagereasons');
			$table->integer('user_id')->unsigned();	
			$table->foreign('user_id')->references('id')->on('users');
			$table->boolean('inbound')->default(0);
			$table->boolean('outbound')->default(0);
			$table->boolean('web')->default(0);
			$table->boolean('leads')->default(0);
			$table->decimal('averagesale',10,2)->default(0);
			$table->integer('averageduration')->default(0);
            $table->timestamps();
			$table->unsignedInteger('created_by')-> nullable()->default(null);
			$table->unsignedInteger('updated_by')-> nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
