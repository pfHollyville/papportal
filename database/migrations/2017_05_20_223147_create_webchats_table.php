<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebchatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	 
	 
    public function up()
    {
        Schema::create('webchats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('chat_id',50);
            $table->string('widget_id',150);
            $table->string('url',250)->nullable()->default(null);
            $table->string('snapshot_image_url',250)->nullable()->default(null);
            $table->string('type',20)->nullable()->default(null);
            $table->string('requested_by',150)->nullable()->default(null);
            $table->string('requester_details_name',150)->nullable()->default(null);
			$table->text('requester_details_emails')->nullable()->default(null);
			$table->string('requester_details_name_profile_link',255)->nullable()->default(null);
			$table->text('requester_details_phones',20)->nullable()->default(null);			
			$table->string('requester_details_address',50)->nullable()->default(null);			
			$table->string('requester_details_address_2',50)->nullable()->default(null);			
			$table->string('requester_details_city',25)->nullable()->default(null);			
			$table->string('requester_details_state',25)->nullable()->default(null);			
			$table->string('requester_details_zip',10)->nullable()->default(null);			
			$table->string('requester_details_country',60)->nullable()->default(null);			
			$table->string('requester_details_company_name',60)->nullable()->default(null);			
			$table->string('requester_details_company_profile_link',255)->nullable()->default(null);			
			$table->string('requester_details_employees',50)->nullable()->default(null);			
			$table->string('requester_details_revenue',10)->nullable()->default(null);			
			$table->string('requester_details_title',25)->nullable()->default(null);			
			$table->string('requester_details_website',255)->nullable()->default(null);			
			$table->text('requester_details_social_profile_links',255)->nullable()->default(null);			
			$table->string('requester_details_gender',10)->nullable()->default(null);			
			$table->integer('requester_details_age')->nullable()->default(null);			
			$table->double('requester_details_influencer_score')->nullable()->default(null);			
			$table->text('requester_details_notes')->nullable()->default(null);			
			$table->string('requester_details_industry',25)->nullable()->default(null);			
			$table->text('requester_details_avatars',255)->nullable()->default(null);			
			$table->string('requester_details_other_data',255)->nullable()->default(null);			
			$table->text('description')->nullable()->default(null);		
			$table->datetime('created_at_date')->nullable()->default(null);			
			$table->integer('created_at_seconds')->nullable()->default(null);			
			$table->integer('created_at_milliseconds')->nullable()->default(null);			
			$table->string('proactive_chat',6)->nullable()->default(null);			
			$table->string('page_url',255)->nullable()->default(null);			
			$table->string('referrer_url',255)->nullable()->default(null);			
			$table->string('entry_url',255)->nullable()->default(null);			
			$table->string('ip_address',50)->nullable()->default(null);			
			$table->string('user_agent',150)->nullable()->default(null);			
			$table->string('browser',50)->nullable()->default(null);			
			$table->string('os',30)->nullable()->default(null);			
			$table->string('country_code',5)->nullable()->default(null);			
			$table->string('country',25)->nullable()->default(null);			
			$table->string('region',11)->nullable()->default(null);			
			$table->string('city',25)->nullable()->default(null);			
			$table->double('latitude')->nullable()->default(null);			
			$table->double('longitude')->nullable()->default(null);			
			$table->integer('source_id')->nullable()->default(null);			
			$table->integer('chat_waittime')->nullable()->default(null);			
			$table->integer('chat_duration')->nullable()->default(null);			
			$table->string('language_code',30)->nullable()->default(null);			
			$table->text('transcripts')->nullable()->default(null);			
			$table->text('plugins')->nullable()->default(null);			
			$table->text('javascript_variables')->nullable()->default(null);			
			$table->text('operator_variables')->nullable()->default(null);			
			$table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webchats');
    }
}
