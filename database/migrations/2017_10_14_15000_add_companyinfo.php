<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyinfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

		Schema::table('companies', function($table) {
            $table->bigInteger('webvisitorspermonth')->nullable()->default(0);
            $table->bigInteger('callspermonth')->nullable()->default(0);
		});

    }
	

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('companies', function($table) {
			$table->dropColumn('webvisitorspermonth');
			$table->dropColumn('callspermonth');
		});    
	}
}
