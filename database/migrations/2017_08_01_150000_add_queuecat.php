<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQueuecat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('servicecategories', function($table) {
            $table->string('queue',16)->default('No');
		});
    }
	

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('servicecategories', function($table) {
			$table->dropColumn('queue');
		});    
	}
}
