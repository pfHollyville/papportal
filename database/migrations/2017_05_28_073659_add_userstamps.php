<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserstamps extends Migration
{
	private $tables = [];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		
		
		foreach ($this->tables as $tablename) {
			Schema::table($tablename, function($table) {
				$table->unsignedInteger('created_by')-> nullable()->default(null)->after('created_at');
				$table->unsignedInteger('updated_by')-> nullable()->default(null)->after('updated_at');
			});
		}
    }
	

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		foreach ($this->tables as $tablename) {
			Schema::table($tablename, function($table) {
				$table->dropColumn('created_by');
				$table->dropColumn('updated_by');
			});    
		}
	}
}
