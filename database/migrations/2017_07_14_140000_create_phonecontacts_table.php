<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhonecontactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phonecontacts', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('phonecontactid')->unsigned();	
            $table->date('loaddate')->nullable()->default(null);
            $table->string('datasetid',120)->nullable()->default(null);
            $table->string('sourcefile',120)->nullable()->default(null);
            $table->string('outcomecode',120)->nullable()->default(null);
            $table->date('callback',120)->nullable()->default(null);
            $table->string('processtype',120)->nullable()->default(null);
            $table->string('agentref',120)->nullable()->default(null);
            $table->text('notes')->nullable()->default(null);
            $table->text('data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phonecontacts');

    }
}
