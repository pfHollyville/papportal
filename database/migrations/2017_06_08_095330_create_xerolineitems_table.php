<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateXerolineitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xerolineitems', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('company_services_id')->unsigned();	
			$table->integer('bill_id')->unsigned();	
            $table->decimal('total',10,2)->nullable()->default(null);
            $table->string('description',50)->nullable()->default(null);
            $table->decimal('quantity',10,2)->nullable()->default(null);
            $table->decimal('unitamount',10,2)->nullable()->default(null);
            $table->decimal('discount',10,2)->nullable()->default(null);
            $table->string('accountcode',16)->nullable()->default(null);
            $table->string('taxtype',50)->nullable()->default(null);
            $table->decimal('taxamount',10,2)->nullable()->default(0);
			$table->unsignedInteger('created_by')-> nullable()->default(null);
			$table->unsignedInteger('updated_by')-> nullable()->default(null);
            $table->timestamps();
			$table->foreign('company_services_id')->references('id')->on('company_services');
			$table->foreign('bill_id')->references('id')->on('bills');
        });


	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xerolineitems');

    }
}
