<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',50);
            $table->text('description')->nullable()->default(null);
            $table->integer('maxusage')->nullable()->default(null);
            $table->integer('mincommitment')->nullable()->default(null);
            $table->decimal('basecost',10,2)->nullable()->default(null);
            $table->decimal('extracost',10,2)->nullable()->default(null);
            $table->boolean('active')->default(true);
            $table->string('image',200)->nullable()->default(null);
            $table->timestamps();
			$table->unsignedInteger('created_by')-> nullable()->default(null);
			$table->unsignedInteger('updated_by')-> nullable()->default(null);
			$table->integer('category_id')->unsigned();	
			$table->foreign('category_id')->references('id')->on('servicecategories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
