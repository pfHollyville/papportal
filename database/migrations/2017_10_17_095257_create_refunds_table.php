<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRefundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('refunds', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('company_id')->unsigned();	
			$table->foreign('company_id')->references('id')->on('companies');
			$table->decimal('amount',10,2)->default(0);
			$table->date('refund_date');
			$table->string('status',16)->default('unapproved');
			$table->string('reason',255);
			$table->text('notes')->nullable()->default(null);
			$table->unsignedInteger('created_by')-> nullable()->default(null);
			$table->unsignedInteger('updated_by')-> nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('refunds');
    }
}
