<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::statement("
        CREATE VIEW vw_billing AS
        (
			SELECT
			bills.id as 'bill_id',
			CONCAT(companies.companyname,' - ',LPAD(companies.id,4,'0')) as 'ContactName',
			companies.workemail as 'EmailAddress',
			companies.address1 as 'POAddressLine1',
			companies.address2 as 'POAddressLine2',
			'' as 'POAddressLine3',
			'' as 'POAddressLine4',
			companies.city as 'POCity',
			companies.county as 'PORegion',
			companies.postcode as 'POPostalCode',
			companies.country as 'POCountry',
			CONCAT('HP',LPAD(bills.id,6,'0')) as 'InvoiceNumber',
			'' as 'Reference',
			DATE_FORMAT(bills.created_at,'%d/%m/%Y') as 'InvoiceDate',
			DATE_FORMAT(bills.created_at,'%d/%m/%Y') as 'DueDate',
			'' as 'InventoryItemCode',
			xerolineitems.description as 'Description',
			xerolineitems.quantity as 'Quantity',
			xerolineitems.unitamount as 'UnitAmount',
			xerolineitems.discount as 'Discount',
			xerolineitems.accountcode as 'AccountCode',
			'None' as 'TaxType',
			0 as 'TaxAmount',
			'' as 'TrackingName1',
			'' as 'TrackingName2',
			'' as 'TrackingName3',
			'GBP' as 'Currency',
			'Handlr' as 'BrandingTheme'
			FROM xerolineitems
			join bills on xerolineitems.bill_id = bills.id
			join companies on companies.id = bills.company_id
			join users on companies.user_id = users.id
        )
		");
	
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vw_billing');
    }
}
