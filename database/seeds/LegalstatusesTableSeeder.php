<?php

use Illuminate\Database\Seeder;

class LegalstatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('legalstatuses')->insert([
            'name' => 'Limited Company',
        ]);
        DB::table('legalstatuses')->insert([
            'name' => 'Not for profit',
        ]);
        DB::table('legalstatuses')->insert([
            'name' => 'Partnership',
        ]);
        DB::table('legalstatuses')->insert([
            'name' => 'Person',
        ]);
        DB::table('legalstatuses')->insert([
            'name' => 'Sole trader',
        ]);
    }
}
