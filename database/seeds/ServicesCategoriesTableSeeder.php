<?php

use Illuminate\Database\Seeder;

class ServicesCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('servicecategories')->insert([
            'name' => 'Inbound Calls',
            'description' => 'Inbound Calls',
            'image' => ''
        ]);
        DB::table('servicecategories')->insert([
            'name' => 'Web Chats',
            'description' => 'Web Chats',
            'image' => ''
        ]);

    }
}
