<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
            'user_id' =>5, //joe
			'companyname' => 'Joes company',
            'companynumber' => '111',
            'address1' => 'some property',
            'address2' => 'some street',
            'city' => 'some city',
            'county' => 'Some county',
            'postcode' => 'AA1 1AA',
            'country' => 'UK',
            'industry_id' => 2,
            'legalstatus_id' => 2,
            'usagereason_id' => 2,
            'workphone' => '1234455',
            'workemail' => 'joework@joecompany.com',
			'created_by' => 1
        ]);
		

		factory(App\User::class, 50)->create()->each(function ($u) {
			$u->company()->save(factory(App\Company::class)->make());
		});		
		
        //
    }
}
