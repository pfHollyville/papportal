<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
			'id' => 1,
            'first_name' => 'Paul',
            'last_name' => 'Fleming',
            'email' => 'paul.fleming@hollyville.co.uk',
			'role' => 'admin',
            'password' => bcrypt('Admin123'),
        ]);
        DB::table('users')->insert([
			'id' => 2,
            'first_name' => 'Aaron',
            'last_name' => 'Ray',
            'email' => 'aaron@papros.co.uk',
			'role' => 'admin',
            'password' => bcrypt('Admin123'),
        ]);
        DB::table('users')->insert([
			'id' => 3,
            'first_name' => 'Fred',
            'last_name' => 'Bloggs',
            'email' => 'fred@bloggs.co.uk',
			'role' => 'staff',
            'password' => bcrypt('Staff123'),
        ]);
        DB::table('users')->insert([
			'id' => 4,
            'first_name' => 'Mary',
            'last_name' => 'Jane',
            'email' => 'mary@company.co.uk',
			'role' => 'client',
            'password' => bcrypt('Client123'),
        ]);
        DB::table('users')->insert([
			'id' => 5,
            'first_name' => 'Joe',
            'last_name' => 'Bloggs',
            'email' => 'joe@company.co.uk',
			'role' => 'client',
            'password' => bcrypt('Client456'),
        ]);
    }
}
