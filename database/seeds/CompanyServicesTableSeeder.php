<?php

use Illuminate\Database\Seeder;

class CompanyServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		factory(App\CompanyServices::class, 200)->create()->each(function ($c) {
			if ($c->widget_id != null) {
				for ($i = 1; $i < rand(100,200); $i++) {
					$c->webchats()->save(factory(App\Webchat::class)->make());
				}
			}
		});
	}

}
