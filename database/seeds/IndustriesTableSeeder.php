<?php

use Illuminate\Database\Seeder;

class IndustriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('industries')->insert([
            'name' => 'Aeronautics and Defence',
        ]);
        DB::table('industries')->insert([
            'name' => 'Alternative Investment Instruments',
        ]);
        DB::table('industries')->insert([
            'name' => 'Automotive and Parts',
        ]);
        DB::table('industries')->insert([
            'name' => 'Banking',
        ]);
        DB::table('industries')->insert([
            'name' => 'Beverages',
        ]);
        DB::table('industries')->insert([
            'name' => 'Building and Materials',
        ]);
        DB::table('industries')->insert([
            'name' => 'Commercial Transportation',
        ]);
        DB::table('industries')->insert([
            'name' => 'Domestic Goods',
        ]);
        DB::table('industries')->insert([
            'name' => 'Electricity Generation and Distribution',
        ]);
        DB::table('industries')->insert([
            'name' => 'Electronic & Electrical Equipment',
        ]);
        DB::table('industries')->insert([
            'name' => 'Engineering Products',
        ]);
        DB::table('industries')->insert([
            'name' => 'Financials',
        ]);
        DB::table('industries')->insert([
            'name' => 'Food Products',
        ]);
        DB::table('industries')->insert([
            'name' => 'Forestry & Paper',
        ]);
        DB::table('industries')->insert([
            'name' => 'Fossil Fuels and Distribution',
        ]);
        DB::table('industries')->insert([
            'name' => 'Health Care and Related Services',
        ]);
        DB::table('industries')->insert([
            'name' => 'Household Utilities',
        ]);
        DB::table('industries')->insert([
            'name' => 'Industrial Chemicals',
        ]);
        DB::table('industries')->insert([
            'name' => 'Industrials',
        ]);
        DB::table('industries')->insert([
            'name' => 'Insurance',
        ]);
        DB::table('industries')->insert([
            'name' => 'Investment Companies',
        ]);
        DB::table('industries')->insert([
            'name' => 'IT Hardware',
        ]);
        DB::table('industries')->insert([
            'name' => 'IT Services',
        ]);
        DB::table('industries')->insert([
            'name' => 'Leisure Products',
        ]);
        DB::table('industries')->insert([
            'name' => 'Media',
        ]);
    }
	

	
}
