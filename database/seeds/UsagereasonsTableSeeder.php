<?php

use Illuminate\Database\Seeder;

class UsagereasonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usagereasons')->insert([
            'name' => 'Dedicated',
        ]);
        DB::table('usagereasons')->insert([
            'name' => 'Overflow',
        ]);
    }
}
