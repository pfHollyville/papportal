<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->call(UsersTableSeeder::class);
		$this->call(ServicesCategoriesTableSeeder::class);
		$this->call(ServicesTableSeeder::class);
		$this->call(IndustriesTableSeeder::class);
		$this->call(LegalstatusesTableSeeder::class);
		$this->call(UsagereasonsTableSeeder::class);
		$this->call(CompaniesTableSeeder::class);
		$this->call(CompanyServicesTableSeeder::class);
		$this->call(BasicquestionmastersTableSeeder::class);
		$this->call(QuestionsTableSeeder::class);
		$this->call(QueusTableSeeder::class);
	}
	
}
