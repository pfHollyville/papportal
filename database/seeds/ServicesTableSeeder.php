<?php

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('services')->insert([
            'name' => 'Business calling service',
			'description'=>'Basic telephone calling service monthly service charge.',
			'mincommitment'=>3,
			'basecost'=>18,
			'active'=>1,
			'category_id'=>1
		
        ]);
        DB::table('services')->insert([
            'name' => 'Premier calling service',
			'description'=>'Full calling service monthly service charge.',
			'mincommitment'=>3,
			'basecost'=>25,
			'active'=>1,
			'category_id'=>1
		
        ]);
        DB::table('services')->insert([
            'name' => 'Inbound / Outbound per minute',
			'description'=>'Cost per minute connected to a call when used in conjunction with a Business or Premier calling package.',
			'basecost'=>1,
			'active'=>1,
			'category_id'=>1
		
        ]);
        DB::table('services')->insert([
            'name' => 'PAP Web Lite',
			'description'=>'PAYG live web chats.',
			'mincommitment'=>0,
			'basecost'=>3,
			'extracost'=>0,
			'active'=>1,
			'category_id'=>2
		
        ]);
        DB::table('services')->insert([
            'name' => 'PAP 30',
			'description'=>'Up to 30 web chats per month.',
			'maxusage' =>30,
			'mincommitment'=>3,
			'basecost'=>90,
			'extracost'=>3,
			'active'=>1,
			'category_id'=>2
		
        ]);
        DB::table('services')->insert([
            'name' => 'PAP 50',
			'description'=>'Up to 50 web chats per month.',
			'maxusage' =>50,
			'mincommitment'=>3,
			'basecost'=>129,
			'extracost'=>3,
			'active'=>1,
			'category_id'=>2
		
        ]);
        DB::table('services')->insert([
            'name' => 'PAP 100',
			'description'=>'Up to 100 web chats per month.',
			'maxusage' =>100,
			'mincommitment'=>3,
			'basecost'=>179,
			'extracost'=>2,
			'active'=>1,
			'category_id'=>2
		
        ]);
        DB::table('services')->insert([
            'name' => 'White Label',
			'description'=>'Removal of powered by PA Pros from web chats.',
			'basecost'=>3,
			'active'=>1,
			'category_id'=>2
		
        ]);
        DB::table('services')->insert([
            'name' => 'Add your logo',
			'description'=>'Add your own logo to the web chat widget.',
			'basecost'=>3,
			'active'=>1,
			'category_id'=>2
		
        ]);

/*
Business calling service	Basic telephone calling service monthly service charge.		3	18		1		1	
Premier calling service	Full calling service monthly service charge.		3	25		1		1	
Inbound / Outbound per minute	Cost per minute connected to a call when used in c...			1		1		1	
PAP Web Lite	PAYG live web chats		0	3		2		1	
PAP 30	Up to 30 web chats per month.	30	3	90	3	2		1	
PAP 50	Up to 50 web chats per month.	50	3	129	3	2		1	
PAP 100	Up to 100 web chats per month.	100	3	179	2	2		1	
White Label	Removal of powered by PA Pros from web chats.			5		2		1	
Add your logo	Add your own logo to the web chat widget			3		2		1	
		*/
    }
}
