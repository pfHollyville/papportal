<?php

use Illuminate\Database\Seeder;

class BasicquestionmastersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('basicquestionmasters')->insert([
			'question' => 'Tell us what your company does',
            'category' => 'Business',
            'order' => 1
        ]);
        DB::table('basicquestionmasters')->insert([
			'question' => 'Describe your individual products/services',
            'category' => 'Business',
            'order' => 2
        ]);
        DB::table('basicquestionmasters')->insert([
			'question' => 'What are your business hours?',
            'category' => 'Business',
            'order' => 3
        ]);
        DB::table('basicquestionmasters')->insert([
			'question' => 'What area(s) do you cover?',
            'category' => 'Business',
            'order' => 4
        ]);
        DB::table('basicquestionmasters')->insert([
			'question' => 'What are your contact details? including phone/email/website etc',
            'category' => 'Business',
            'order' => 5
        ]);
        DB::table('basicquestionmasters')->insert([
			'question' => 'What is your average sale/lead value? (textbox decimal only)',
            'category' => 'Business',
            'order' => 6
        ]);
        DB::table('basicquestionmasters')->insert([
			'question' => 'What (if any) promotions do you currently have?',
            'category' => 'Business',
            'order' => 7
        ]);
        DB::table('basicquestionmasters')->insert([
			'question' => 'What are your expectations of PA Pros?',
            'category' => 'Business',
            'order' => 8
        ]);
        DB::table('basicquestionmasters')->insert([
			'question' => 'Do you have any special requirements with our phone service? E.g specific information you require beyond basic contact details and a message being taken?',
            'category' => 'Phone',
            'order' => 10
        ]);
        DB::table('basicquestionmasters')->insert([
			'question' => 'What website(s) will your chat box be hosted on?',
            'category' => 'Web',
            'order' => 11
        ]);
    }
}
