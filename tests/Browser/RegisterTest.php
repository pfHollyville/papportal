<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RegisterTest extends DuskTestCase
{
	use DatabaseTransactions;
    /**
     * A Dusk test example.
     *
     * @return void
     */
	
	public function testClientLoginInvalid()
	{
        sleep(1);

		$this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->type('email', 'joe@company.co.uk')
                    ->type('password', 'wrong')
                    ->press('Login')
                    ->assertSee('These credentials do not match our records.')
                    ->assertPathIs('/login');
        });
	
		
	}
	
    public function testClientLoginValid()
    {
		
        sleep(1);

		$this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->assertSee('Login')
                    ->type('email', 'joe@company.co.uk')
                    ->type('password', 'Client456')
                    ->press('Login')
                    ->assertSee('You are logged in!')
                    ->assertPathIs('/home')
					->visit('/admin/users')
					->assertPathIs('/')
                    ->assertSee('Sorry, you dont have Staff permission to do that!')
					->clickLink('Logout');
        });
    }

	
    public function testStaffLoginValid()
    {
		
        sleep(1);

		$this->browse(function (Browser $browser) {
            $browser->visit('/login')
                    ->type('email', 'fred@bloggs.co.uk')
                    ->type('password', 'Staff123')
                    ->press('Login')
                    ->assertPathIs('/admin')
					->clickLink('Logout');
        });
    }

	
}
