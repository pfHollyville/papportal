<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AdminRouteTest extends DuskTestCase
{
	use DatabaseTransactions;
    /**
     * A Dusk test example.
     *
     * @return void
     */
	
	
    public function testAdminLoginValid()
    {
        sleep(1);
		$this->browse(function (Browser $browser) {
            $browser->visit('/admin/login')
                    ->assertSee('Login')
                    ->type('email', 'paul.fleming@hollyville.co.uk')
                    ->type('password', 'Admin123')
                    ->press('Login')
                    ->assertPathIs('/admin')
                    ->assertSee('Admin Dashboard');
        });
    }

    public function testAdminRouteUsers()
    {
        sleep(1);
		$this->browse(function (Browser $browser) {
            $browser->visit('/admin/users')
					->assertPathIs('/admin/users')
                    ->assertSee('User Management');
        });
    }
	
    public function testAdminRouteCompanies()
    {
        sleep(1);
		$this->browse(function (Browser $browser) {
            $browser->visit('/admin/companies')
					->assertPathIs('/admin/companies')
                    ->assertSee('Company Management');
		});
    }
	
    public function testAdminRouteCompanyServices()
    {
        sleep(1);
		$this->browse(function (Browser $browser) {
            $browser->visit('/admin/companyservices')
					->assertPathIs('/admin/companyservices')
                    ->assertSee('Company Services Management');
        });
    }
    public function testAdminRouteQuestions()
    {
        sleep(1);
		$this->browse(function (Browser $browser) {
            $browser->visit('/admin/questions')
					->assertPathIs('/admin/questions')
                    ->assertSee('Company Questions');
        });
    }

	
    public function testAdminRoutePhoneContacts()
    {
        sleep(1);
		$this->browse(function (Browser $browser) {
            $browser->visit('/admin/phonecontacts')
					->assertPathIs('/admin/phonecontacts')
                    ->assertSee('Phone Contacts');
        });
    }
	
	
    public function testAdminRouteWebchats()
    {
        sleep(1);
		$this->browse(function (Browser $browser) {
            $browser->visit('/admin/webchats')
					->assertPathIs('/admin/webchats')
                    ->assertSee('Webchats');
		});
    }

    public function testAdminRouteBills()
    {
        sleep(1);
		$this->browse(function (Browser $browser) {
            $browser->visit('/admin/bills')
					->assertPathIs('/admin/bills')
                    ->assertSee('Bills');
        });
    }

    public function testAdminRouteCalls()
    {
        sleep(1);
		$this->browse(function (Browser $browser) {
            $browser->visit('/admin/calls')
					->assertPathIs('/admin/calls')
                    ->assertSee('Calls');
        });
    }

    public function testAdminRouteNotes()
    {
        sleep(1);
		$this->browse(function (Browser $browser) {
            $browser->visit('/admin/notes')
					->assertPathIs('/admin/notes')
                    ->assertSee('Company Notes');
        });
    }
	
	
    public function testAdminRouteServices()
    {
        sleep(1);
		$this->browse(function (Browser $browser) {
            $browser->visit('/admin/services')
					->assertPathIs('/admin/services')
                    ->assertSee('Service Management')
					->clickLink('Logout');
        });
    }

    
}