<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ClientSetupTest extends DuskTestCase
{
	use DatabaseTransactions;
	/**
     * @group terms
     */
	public function testClientRegisterTerms()
	{
        sleep(1);
		
		$this->browse(function (Browser $browser) {
            $browser->visit('/register')
					->clickLink('terms and conditions')
                    ->assertPathIs('/register')
					->visit('/terms')
					->assertSee('INTERPRETATION')
                    ->assertPathIs('/terms');					
					
        });

        sleep(1);

		$this->browse(function (Browser $browser) {
            $browser->visit('/register')
					->clickLink('here')
                    ->assertPathIs('/register')
					->visit('/privacy')
					->assertSee('Description of processing')
                    ->assertPathIs('/privacy');					

        });

		
	}

	 
	/**
     * @group register
     */
	public function testClientCreatePasswordInvalid()
	{
        sleep(1);

		$this->browse(function (Browser $browser) {
            $browser->visit('/register')
                    ->type('first_name', 'first')
                    ->type('last_name', 'last')
                    ->type('email', 'test99@test.co.uk')
                    ->type('password', 'wrong')
                    ->type('password_confirmation', 'wrong')
					->check('terms')
                    ->press('Register')
                    ->assertSee('Password needs to have at least 6 characters')
                    ->assertPathIs('/register');
        });
		
	}
	
	/**
     * @group register
     */
	public function testClientCreatePasswordNotMatched()
	{
        sleep(1);

		$this->browse(function (Browser $browser) {
            $browser->visit('/register')
                    ->type('first_name', 'first')
                    ->type('last_name', 'last')
                    ->type('email', 'test99@test.co.uk')
                    ->type('password', '123456')
                    ->type('password_confirmation', 'wrong')
					->check('terms')
                    ->press('Register')
                    ->assertSee('The password confirmation does not match.')
                    ->assertPathIs('/register');
        });
		
	}

	/**
     * @group register
     */
	public function testClientCreateEmailNotUnique()
	{
        sleep(1);

		$this->browse(function (Browser $browser) {
            $browser->visit('/register')
                    ->type('first_name', 'first')
                    ->type('last_name', 'last')
                    ->type('email', 'joe@company.co.uk')
                    ->type('password', '123456')
                    ->type('password_confirmation', '123456')
					->check('terms')
                    ->press('Register')
                    ->assertSee('The email has already been taken.')
                    ->assertPathIs('/register');
        });
		
	}
	
	/**
     * @group register
     */
	public function testClientCreateSuccess()
	{
        sleep(1);

		$this->browse(function (Browser $browser) {
            $browser->visit('/register')
                    ->type('first_name', 'test')
                    ->type('last_name', 'test')
                    ->type('email', 'test99@example.co.uk')
                    ->type('password', '123456')
                    ->type('password_confirmation', '123456')
					->check('terms')
                    ->press('Register')
                    ->assertSee('Welcome test')
                    ->assertPathIs('/company/create')
					->clickLink('Logout');;
        });
		
	}
	
	
}
